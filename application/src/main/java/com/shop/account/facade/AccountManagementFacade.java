package com.shop.account.facade;

import com.shop.account.dto.AccountResponse;
import java.util.List;
import java.util.UUID;

public interface AccountManagementFacade {

    AccountResponse getAccount(UUID accountId);

    List<AccountResponse> getAccounts();

    void deleteAccount(UUID accountId);

}
