package com.shop.account.facade;

import com.shop.account.dto.AcceptPasswordRequest;
import com.shop.account.dto.ChangeEmailRequest;
import com.shop.account.dto.ChangePasswordRequest;
import com.shop.account.dto.ResetPasswordRequest;
import java.util.UUID;

public interface AccountFacade {

    void changeEmail(UUID accountId, ChangeEmailRequest request);

    void changePassword(UUID accountId, ChangePasswordRequest request);

    void resetPassword(ResetPasswordRequest request);

    void acceptPassword(AcceptPasswordRequest request);

}
