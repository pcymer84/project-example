package com.shop.account.facade;

import com.shop.account.model.Role;
import java.util.UUID;

public interface AccountRoleFacade {

    void assigneeRole(UUID accountId, Role role);

    void removeRole(UUID accountId, Role role);

}
