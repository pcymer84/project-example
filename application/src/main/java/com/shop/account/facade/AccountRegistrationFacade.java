package com.shop.account.facade;

import com.shop.account.dto.AccountResponse;
import com.shop.account.dto.FacebookRegistrationRequest;
import com.shop.account.dto.RegistrationRequest;

public interface AccountRegistrationFacade {

    AccountResponse register(RegistrationRequest request);

    AccountResponse register(FacebookRegistrationRequest request);

}
