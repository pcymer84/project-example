package com.shop.account.facade;

import com.shop.account.dto.AuthenticateRequest;
import com.shop.account.dto.FacebookAuthenticateRequest;

public interface AccountAuthFacade {

    String authenticate(AuthenticateRequest request);

    String authenticate(FacebookAuthenticateRequest request);

}
