package com.shop.account.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FacebookAuthenticateRequest {

    private String facebookAccessToken;

}
