package com.shop.account.dto;

import com.shop.core.validator.annotations.Password;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@ToString(exclude = "password")
@NoArgsConstructor
@AllArgsConstructor
public class AcceptPasswordRequest {

    @NotNull
    private UUID code;
    @NotNull
    @Password
    private String password;

}
