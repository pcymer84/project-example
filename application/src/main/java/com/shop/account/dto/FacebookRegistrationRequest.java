package com.shop.account.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class FacebookRegistrationRequest {

    @NotNull
    private String facebookAccessToken;

}
