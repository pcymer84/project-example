package com.shop.account.dto;

import com.google.common.collect.Sets;
import com.shop.account.model.Role;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
public class AccountResponse {

    private UUID id;
    private String email;
    private String facebookId;
    private Set<Role> roles = Sets.newHashSet();

}
