package com.shop.account.dto;

import com.shop.core.validator.annotations.Password;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@ToString(exclude = "password")
@NoArgsConstructor
public class RegistrationRequest {

    @NotNull
    @Email
    private String email;
    @NotNull
    @Password
    private String password;

}
