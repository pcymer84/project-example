package com.shop.account.dto;

import com.shop.core.validator.annotations.Password;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.validation.constraints.NotNull;

@Data
@ToString(exclude = {"oldPassword", "newPassword"})
@NoArgsConstructor
public class ChangePasswordRequest {

    @NotNull
    private String oldPassword;
    @NotNull
    @Password
    private String newPassword;

}
