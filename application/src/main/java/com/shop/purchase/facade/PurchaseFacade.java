package com.shop.purchase.facade;

import com.shop.purchase.dto.DeliveryRequest;
import com.shop.purchase.dto.DeliveryResponse;
import com.shop.purchase.dto.InvoiceRequest;
import com.shop.purchase.dto.InvoiceResponse;
import com.shop.purchase.dto.PurchaseProductResponse;
import com.shop.purchase.dto.PurchaseRequest;
import com.shop.purchase.dto.PurchaseResponse;
import java.util.UUID;

public interface PurchaseFacade {

    PurchaseResponse getPurchase(UUID buyerId, UUID purchaseId);

    InvoiceResponse getPurchaseInvoice(UUID buyerId, UUID purchaseId);

    DeliveryResponse getPurchaseDelivery(UUID buyerId, UUID purchaseId);

    PurchaseResponse submitPurchase(UUID buyerId, PurchaseRequest request);

    InvoiceResponse changePurchaseInvoice(UUID buyerId, UUID purchaseId, InvoiceRequest request);

    DeliveryResponse changePurchaseDelivery(UUID buyerId, UUID purchaseId, DeliveryRequest request);

    PurchaseProductResponse addProductToPurchase(UUID buyerId, UUID purchaseId, UUID productId, Integer amount);

    void removeProductFromPurchase(UUID buyerId, UUID purchaseId, UUID productId);

    PurchaseResponse cancelPurchase(UUID buyerId, UUID purchaseId);

    PurchaseResponse realizePurchase(UUID buyerId, UUID purchaseId);

    PurchaseResponse sendPurchase(UUID buyerId, UUID purchaseId);

    PurchaseResponse finishPurchase(UUID buyerId, UUID purchaseId);

}
