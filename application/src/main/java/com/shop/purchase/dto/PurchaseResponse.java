package com.shop.purchase.dto;

import com.shop.purchase.model.PurchaseStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseResponse {

    @NotNull
    private UUID id;
    @NotNull
    private UUID buyerId;
    @NotNull
    private PurchaseStatus status;
    private Date createdAt;
    private Date realizedAt;
    private Date sendAt;
    private Date finishedAt;
    private DeliveryResponse delivery;
    private InvoiceResponse invoice;
    private Set<PurchaseProductResponse> products;

}
