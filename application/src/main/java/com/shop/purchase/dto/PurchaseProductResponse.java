package com.shop.purchase.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseProductResponse {

    private UUID productId;
    private UUID sellerId;
    private Integer amount;
    private String name;
    private Double price;
    private String description;
    private String imageUrl;

}
