package com.shop.purchase.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseRequest {

    @NotNull
    @NotEmpty
    private List<PurchaseProductRequest> products;
    @NotNull
    private DeliveryRequest delivery;
    private InvoiceRequest invoice;

}
