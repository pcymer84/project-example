package com.shop.product.dto;

import com.shop.core.validator.annotations.Currency;
import com.shop.core.validator.annotations.URL;
import lombok.Data;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class ProductRequest {

    @NotNull
    @Currency
    private Double price;
    @NotNull
    private String name;
    private String description;
    @URL
    private String imageUrl;
    @NotNull
    @Min(1)
    private Integer stockNumber;

}
