package com.shop.product.dto;

import lombok.Data;
import java.util.UUID;

@Data
public class ProductResponse {

    private UUID id;
    private UUID sellerId;
    private Integer stockNumber;
    private Double price;
    private String name;
    private String description;
    private String imageUrl;

}
