package com.shop.product.facade;

import java.util.UUID;

public interface ProductStockFacade {

    void addProductsToStock(UUID productId, Integer amount);

    void removeProductsFromStock(UUID productId, Integer amount);

}
