package com.shop.product.facade;

import com.shop.product.dto.ProductRequest;
import com.shop.product.dto.ProductResponse;
import java.util.List;
import java.util.UUID;

public interface ProductFacade {

    List<ProductResponse> searchProducts(String query);

    ProductResponse getProduct(UUID productId);

    ProductResponse registerProduct(UUID sellerId, ProductRequest productRegistration);

    ProductResponse addProductsToStock(UUID sellerId, UUID productId, Integer amount);

    ProductResponse removeProductsFromStock(UUID sellerId, UUID productId, Integer amount);

    void deleteProduct(UUID sellerId, UUID productId);

}
