package com.shop.rating.facade;

import com.shop.rating.dto.ProductRatingResponse;
import java.util.UUID;

public interface ProductRatingFacade {

    ProductRatingResponse getProductRating(UUID productId);

}
