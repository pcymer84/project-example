package com.shop.rating.facade;

import com.shop.rating.dto.ProductReviewRequest;
import com.shop.rating.dto.ProductReviewResponse;
import java.util.List;
import java.util.UUID;

public interface ProductReviewFacade {

    List<ProductReviewResponse> getProductReviews(UUID productId);

    ProductReviewResponse createProductReview(UUID reviewerId, UUID productId, ProductReviewRequest request);

    ProductReviewResponse updateProductReview(UUID reviewerId, UUID productId, UUID reviewId, ProductReviewRequest request);

    ProductReviewResponse censorshipProductReview(UUID productId, UUID reviewId);

}
