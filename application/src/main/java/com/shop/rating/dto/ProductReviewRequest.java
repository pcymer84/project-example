package com.shop.rating.dto;

import lombok.Getter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
public class ProductReviewRequest {

    @Min(1)
    @Max(5)
    @NotNull
    private Integer score;
    @Size(max = 255)
    private String comment;

}
