package com.shop.rating.dto;

import lombok.Data;
import java.util.List;
import java.util.UUID;

@Data
public class ProductRatingResponse {

    private UUID productId;
    private UUID sellerId;
    private Double averageScore;
    private List<ProductReviewResponse> reviews;

}
