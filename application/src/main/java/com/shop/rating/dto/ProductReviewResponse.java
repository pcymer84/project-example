package com.shop.rating.dto;

import lombok.Data;
import java.util.Date;
import java.util.UUID;

@Data
public class ProductReviewResponse {

    private UUID id;
    private UUID reviewerId;
    private Integer score;
    private String comment;
    private Date lastModified;

}
