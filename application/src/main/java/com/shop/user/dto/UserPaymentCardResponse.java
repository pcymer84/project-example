package com.shop.user.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@NoArgsConstructor
public class UserPaymentCardResponse {

    private UUID id;
    private String cardNumber;
    private String firstName;
    private String lastName;
    private Integer createdMonth;
    private Integer createdYear;
    private Integer expiredMonth;
    private Integer expiredYear;

}
