package com.shop.user.dto;

import com.shop.user.model.enums.Gender;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import java.util.UUID;

@Data
@NoArgsConstructor
public class UserProfileResponse {

    private UUID id;
    private String phone;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private Gender gender;

}
