package com.shop.user.dto;

import com.shop.core.validator.annotations.Phone;
import com.shop.user.model.enums.Gender;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class UserProfileRequest {

    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private Gender gender;
    @Phone
    private String phone;

}
