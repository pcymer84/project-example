package com.shop.user.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@NoArgsConstructor
public class UserAddressResponse {

    private UUID id;
    private String postcode;
    private String city;
    private String street;
    private String description;

}
