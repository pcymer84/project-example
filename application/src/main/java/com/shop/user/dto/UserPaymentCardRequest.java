package com.shop.user.dto;

import com.shop.core.validator.annotations.Month;
import com.shop.core.validator.annotations.Year;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class UserPaymentCardRequest {

    @NotNull
    private String cardNumber;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    @Month
    private Integer createdMonth;
    @NotNull
    @Year
    private Integer createdYear;
    @NotNull
    @Month
    private Integer expiredMonth;
    @NotNull
    @Year
    private Integer expiredYear;

}
