package com.shop.user.facade;

import com.shop.user.dto.UserProfileRequest;
import com.shop.user.dto.UserProfileResponse;
import java.util.UUID;

public interface UserProfileFacade {

    UserProfileResponse getUserProfile(UUID userId);
    
    UserProfileResponse updateUserProfile(UUID userId, UserProfileRequest request);

}
