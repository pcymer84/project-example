package com.shop.user.facade;

import com.shop.user.dto.UserAddressRequest;
import com.shop.user.dto.UserAddressResponse;
import java.util.List;
import java.util.UUID;

public interface UserAddressFacade {

    List<UserAddressResponse> getUserAddresses(UUID userId);

    UserAddressResponse createUserAddress(UUID userId, UserAddressRequest request);

    UserAddressResponse updateUserAddress(UUID userId, UUID addressId, UserAddressRequest request);

    void deleteUserAddress(UUID userId, UUID addressId);

}
