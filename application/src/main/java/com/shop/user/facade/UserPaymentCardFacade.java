package com.shop.user.facade;

import com.shop.user.dto.UserPaymentCardRequest;
import com.shop.user.dto.UserPaymentCardResponse;
import java.util.List;
import java.util.UUID;

public interface UserPaymentCardFacade {

    List<UserPaymentCardResponse> getUserPaymentCards(UUID userId);

    UserPaymentCardResponse createUserPaymentCard(UUID userId, UserPaymentCardRequest request);

    UserPaymentCardResponse updateUserPaymentCard(UUID userId, UUID paymentCardId, UserPaymentCardRequest request);

    void deleteUserPaymentCard(UUID userId, UUID paymentCardId);

}
