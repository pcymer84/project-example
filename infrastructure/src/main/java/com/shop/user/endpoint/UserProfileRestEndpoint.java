package com.shop.user.endpoint;

import com.shop.account.model.AccountDetails;
import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.user.dto.UserProfileRequest;
import com.shop.user.facade.UserProfileFacade;
import com.shop.user.dto.UserProfileResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class UserProfileRestEndpoint {

    private final UserProfileFacade userProfileFacade;

    @GetMapping("/users/profile")
    public UserProfileResponse getUserProfile(@AuthenticationPrincipal AccountDetails account) {
        return userProfileFacade.getUserProfile(account.getId());
    }

    @PutMapping("/users/profile")
    public UserProfileResponse updateUserProfile(@AuthenticationPrincipal AccountDetails account,
                                                 @Valid @RequestBody UserProfileRequest request) {
        return userProfileFacade.updateUserProfile(account.getId(), request);
    }

}
