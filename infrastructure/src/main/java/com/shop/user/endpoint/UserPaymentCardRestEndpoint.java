package com.shop.user.endpoint;

import com.shop.account.model.AccountDetails;
import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.user.dto.UserPaymentCardRequest;
import com.shop.user.dto.UserPaymentCardResponse;
import com.shop.user.facade.UserPaymentCardFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class UserPaymentCardRestEndpoint {

    private final UserPaymentCardFacade userPaymentCardFacade;

    @GetMapping("/users/paymentCards")
    public List<UserPaymentCardResponse> getUserPaymentCards(@AuthenticationPrincipal AccountDetails account) {
        return userPaymentCardFacade.getUserPaymentCards(account.getId());
    }

    @PostMapping("/users/paymentCards")
    public UserPaymentCardResponse createUserPaymentCard(@AuthenticationPrincipal AccountDetails account,
                                                     @Valid @RequestBody UserPaymentCardRequest request) {
        return userPaymentCardFacade.createUserPaymentCard(account.getId(), request);
    }

    @PutMapping("/users/paymentCards/{paymentCardId}")
    public UserPaymentCardResponse updateUserPaymentCard(@AuthenticationPrincipal AccountDetails account,
                                                     @PathVariable("paymentCardId") UUID paymentCardId,
                                                     @Valid @RequestBody UserPaymentCardRequest request) {
        return userPaymentCardFacade.updateUserPaymentCard(account.getId(), paymentCardId, request);
    }

    @DeleteMapping("/users/paymentCards/{paymentCardId}")
    public void deleteUserPaymentCard(@AuthenticationPrincipal AccountDetails account,
                                  @PathVariable("paymentCardId") UUID paymentCardId) {
        userPaymentCardFacade.deleteUserPaymentCard(account.getId(), paymentCardId);
    }

}
