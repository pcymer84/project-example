package com.shop.user.endpoint;

import com.shop.account.model.AccountDetails;
import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.user.dto.UserAddressRequest;
import com.shop.user.dto.UserAddressResponse;
import com.shop.user.facade.UserAddressFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class UserAddressRestEndpoint {

    private final UserAddressFacade userAddressFacade;

    @GetMapping(path = "/users/addresses")
    public List<UserAddressResponse> getUserAddresses(@AuthenticationPrincipal AccountDetails account) {
        return userAddressFacade.getUserAddresses(account.getId());
    }

    @PostMapping(path = "/users/addresses")
    public UserAddressResponse createUserAddress(@AuthenticationPrincipal AccountDetails account,
                                                 @Valid @RequestBody UserAddressRequest request) {
        return userAddressFacade.createUserAddress(account.getId(), request);
    }

    @PutMapping(path = "/users/addresses/{addressId}")
    public UserAddressResponse updateUserAddress(@AuthenticationPrincipal AccountDetails account,
                                                 @PathVariable("addressId") UUID addressId,
                                                 @Valid @RequestBody UserAddressRequest request) {
        return userAddressFacade.updateUserAddress(account.getId(), addressId, request);
    }

    @DeleteMapping(path = "/users/addresses/{addressId}")
    public void deleteUserAddress(@AuthenticationPrincipal AccountDetails account,
                                  @PathVariable("addressId") UUID addressId) {
        userAddressFacade.deleteUserAddress(account.getId(), addressId);
    }

}
