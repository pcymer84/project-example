package com.shop.user.repository;

import com.shop.core.annotations.Repository;
import com.shop.user.ex.UserNotFoundException;
import com.shop.user.model.User;
import lombok.AllArgsConstructor;
import java.util.UUID;

@AllArgsConstructor
@Repository
public class CustomUserRepositoryImpl implements CustomUserRepository {
    
    private final CrudUserRepository crudUserRepository;

    @Override
    public User getById(UUID userId) {
        return crudUserRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(String.format("user with id '%s' not found", userId)));
    }

}
