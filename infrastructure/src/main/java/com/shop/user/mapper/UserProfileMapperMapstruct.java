package com.shop.user.mapper;

import com.shop.user.dto.UserProfileRequest;
import com.shop.user.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper
public interface UserProfileMapperMapstruct extends UserProfileMapper {

    void updateUserProfile(UserProfileRequest request, @MappingTarget User user);

}
