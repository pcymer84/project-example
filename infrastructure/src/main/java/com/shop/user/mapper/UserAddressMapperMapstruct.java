package com.shop.user.mapper;

import com.shop.user.dto.UserAddressRequest;
import com.shop.user.model.UserAddress;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper
public interface UserAddressMapperMapstruct extends UserAddressMapper {

    void updateUserAddress(UserAddressRequest request, @MappingTarget UserAddress userAddress);

}
