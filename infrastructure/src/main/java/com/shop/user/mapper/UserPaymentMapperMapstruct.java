package com.shop.user.mapper;

import com.shop.user.dto.UserPaymentCardRequest;
import com.shop.user.model.UserPaymentCard;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper
public interface UserPaymentMapperMapstruct extends UserPaymentMapper {

    void updateUserPaymentCard(UserPaymentCardRequest request, @MappingTarget UserPaymentCard userPaymentCard);

}
