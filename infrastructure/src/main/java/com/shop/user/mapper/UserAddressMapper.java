package com.shop.user.mapper;

import com.shop.user.dto.UserAddressRequest;
import com.shop.user.dto.UserAddressResponse;
import com.shop.user.model.UserAddress;

public interface UserAddressMapper {

    void updateUserAddress(UserAddressRequest request, UserAddress userAddress);

    UserAddress mapToUserAddress(UserAddressRequest request);

    UserAddressResponse mapToUserAddressResponse(UserAddress userAddress);

}
