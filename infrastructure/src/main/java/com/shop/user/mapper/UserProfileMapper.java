package com.shop.user.mapper;

import com.shop.user.dto.UserProfileRequest;
import com.shop.user.dto.UserProfileResponse;
import com.shop.user.model.User;

public interface UserProfileMapper {

    void updateUserProfile(UserProfileRequest request, User user);

    UserProfileResponse mapToUserProfileResponse(User user);

}
