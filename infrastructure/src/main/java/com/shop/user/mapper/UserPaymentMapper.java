package com.shop.user.mapper;

import com.shop.user.dto.UserPaymentCardRequest;
import com.shop.user.dto.UserPaymentCardResponse;
import com.shop.user.model.UserPaymentCard;

public interface UserPaymentMapper {

    UserPaymentCard mapToUserPaymentCard(UserPaymentCardRequest request);

    UserPaymentCardResponse mapToUserPaymentCardResponse(UserPaymentCard userPaymentCard);

    void updateUserPaymentCard(UserPaymentCardRequest request, UserPaymentCard userPaymentCard);

}
