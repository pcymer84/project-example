package com.shop.user.model;

import com.shop.account.model.Account;
import org.springframework.stereotype.Component;

@Component
public class UserFactoryImpl implements UserFactory {

    @Override
    public User createUserProfileForAccount(Account account) {
        return User.builder().id(account.getId()).build();
    }

}
