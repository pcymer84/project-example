package com.shop.user.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.UUID;

@Getter
@AllArgsConstructor
public enum UserPaymentCardInitData {

    USER_1_PAYMENT_1(
            UUID.fromString("9b50734e-e182-452a-9970-9c335347ec93"),
            "cardNumber",
            "firstName",
            "lastName",
            5, 2018,
            6, 2022
    );

    private UUID id;
    private String cardNumber;
    private String firstName;
    private String lastName;
    private Integer createdMonth;
    private Integer createdYear;
    private Integer expiredMonth;
    private Integer expiredYear;

}
