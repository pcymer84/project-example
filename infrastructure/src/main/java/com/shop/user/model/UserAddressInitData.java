package com.shop.user.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.UUID;

@Getter
@AllArgsConstructor
public enum UserAddressInitData {

    USER_1_ADDRESS_1(
            UUID.fromString("457657c5-4c7b-4d64-93ca-d3aa99488912"),
            "postcode",
            "city",
            "street",
            "description"
    ),
    USER_2_ADDRESS_1(
            UUID.fromString("1b4eaf91-7658-44d2-927d-3e874bcc564a"),
            "postcode",
            "city",
            "street",
            "description"
    );

    private UUID id;
    private String postcode;
    private String city;
    private String street;
    private String description;

}
