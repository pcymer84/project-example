package com.shop.user.model;

import com.google.common.collect.Sets;
import com.shop.core.config.SpringProfile;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Component
public class UserInitDataFactory {

    public User createUser(UserInitData user) {
        return User.builder()
                .id(user.getId())
                .phone(user.getPhone())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .birthDate(user.getBirthDate())
                .gender(user.getGender())
                .with(builder -> withAddress(builder, user.getAddress()))
                .with(builder -> withPaymentCard(builder, user.getPaymentCard()))
                .build();
    }

    private User.UserBuilder withAddress(User.UserBuilder builder, UserAddressInitData address) {
        if (address != null) {
            builder.addresses(Sets.newHashSet(createAddress(address)));
        }
        return builder;
    }

    private User.UserBuilder withPaymentCard(User.UserBuilder builder, UserPaymentCardInitData paymentCard) {
        if (paymentCard != null) {
            builder.paymentCards(Sets.newHashSet(createPaymentCard(paymentCard)));
        }
        return builder;
    }

    private UserAddress createAddress(UserAddressInitData userAddress) {
        return UserAddress.builder()
                .id(userAddress.getId())
                .postcode(userAddress.getPostcode())
                .city(userAddress.getCity())
                .street(userAddress.getStreet())
                .description(userAddress.getDescription())
                .build();
    }

    private UserPaymentCard createPaymentCard(UserPaymentCardInitData paymentCard) {
        return UserPaymentCard.builder()
                .id(paymentCard.getId())
                .cardNumber(paymentCard.getCardNumber())
                .firstName(paymentCard.getFirstName())
                .lastName(paymentCard.getLastName())
                .createdMonth(paymentCard.getCreatedMonth())
                .createdYear(paymentCard.getCreatedYear())
                .expiredMonth(paymentCard.getExpiredMonth())
                .expiredYear(paymentCard.getExpiredYear())
                .build();
    }

}
