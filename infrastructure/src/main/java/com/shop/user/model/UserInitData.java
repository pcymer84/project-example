package com.shop.user.model;

import com.google.common.collect.Sets;
import com.shop.account.model.Role;
import com.shop.user.model.enums.Gender;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum UserInitData {

    USER_1(
            UUID.fromString("ecce1a91-6fce-413d-ab34-a16a0ba6a1b5"),
            "Qwerty1!",
            "$2a$10$VV/Lucw8IsJx9Wk.8nonTeUU/Zqe8BndOmf4BjamrN176qnCBVNLe",
            "facebookId",
            UUID.fromString("a41e10d9-86d0-43b9-8696-bd15fc232b69"),
            "dummy1@test.com",
            "123456789",
            "firstName",
            "lastName",
            LocalDate.of(1988, 10, 5),
            Gender.MALE,
            UserAddressInitData.USER_1_ADDRESS_1,
            UserPaymentCardInitData.USER_1_PAYMENT_1,
            Sets.newHashSet(Role.ADMIN)
    ),
    USER_2(
            UUID.fromString("89fb83b3-776b-473c-849a-45ea8b75cef2"),
            "Qwerty1!",
            "$2a$10$VV/Lucw8IsJx9Wk.8nonTeUU/Zqe8BndOmf4BjamrN176qnCBVNLe",
            null,
            null,
            "dummy2@test.com",
            null,
            null,
            null,
            LocalDate.of(1988, 10, 5),
            Gender.FEMALE,
            null,
            null,
            Sets.newHashSet(Role.SELLER)
    ),
    USER_3(
            UUID.fromString("6675912c-ab96-407a-832b-afb333af5ee1"),
            "Qwerty1!",
            "$2a$10$VV/Lucw8IsJx9Wk.8nonTeUU/Zqe8BndOmf4BjamrN176qnCBVNLe",
            null,
            null,
            "dummy3@test.com",
            null,
            null,
            null,
            LocalDate.of(1986, 5, 1),
            Gender.FEMALE,
            null,
            null,
            Sets.newHashSet()
    ),
    UNSAVED_USER(
            null,
            "Qwerty1!",
            "$2a$10$VV/Lucw8IsJx9Wk.8nonTeUU/Zqe8BndOmf4BjamrN176qnCBVNLe",
            "notUsedFacebookId",
            UUID.fromString("f1e6bb96-7d2c-4010-8209-d785c75e7249"),
            "dummyX@test.com",
            "123456789",
            "firstName",
            "lastName",
            LocalDate.of(1986, 4, 11),
            Gender.FEMALE,
            null,
            null,
            Sets.newHashSet()
    );

    private UUID id;
    private String password;
    private String hashedPassword;
    private String facebookId;
    private UUID resetPasswordCode;
    private String email;
    private String phone;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private Gender gender;
    private UserAddressInitData address;
    private UserPaymentCardInitData paymentCard;
    private Set<Role> role;

}
