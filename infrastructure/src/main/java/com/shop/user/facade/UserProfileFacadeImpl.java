package com.shop.user.facade;

import com.shop.core.annotations.ApplicationService;
import com.shop.core.logger.Log;
import com.shop.user.dto.UserProfileRequest;
import com.shop.user.dto.UserProfileResponse;
import com.shop.user.mapper.UserProfileMapper;
import com.shop.user.model.User;
import com.shop.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@Log
@AllArgsConstructor
@ApplicationService
public class UserProfileFacadeImpl implements UserProfileFacade {

    private final UserRepository userRepository;
    private final UserProfileMapper userProfileMapper;

    @Override
    public UserProfileResponse getUserProfile(UUID userId) {
        User user = userRepository.getById(userId);
        return userProfileMapper.mapToUserProfileResponse(user);
    }

    @Override
    public UserProfileResponse updateUserProfile(UUID userId, UserProfileRequest request) {
        User user = userRepository.getById(userId);
        userProfileMapper.updateUserProfile(request, user);
        userRepository.save(user);
        return userProfileMapper.mapToUserProfileResponse(user);
    }

}
