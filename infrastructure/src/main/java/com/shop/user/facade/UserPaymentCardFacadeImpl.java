package com.shop.user.facade;

import com.shop.core.annotations.ApplicationService;
import com.shop.core.logger.Log;
import com.shop.user.dto.UserPaymentCardRequest;
import com.shop.user.dto.UserPaymentCardResponse;
import com.shop.user.mapper.UserPaymentMapper;
import com.shop.user.model.User;
import com.shop.user.model.UserPaymentCard;
import com.shop.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Log
@AllArgsConstructor
@ApplicationService
public class UserPaymentCardFacadeImpl implements UserPaymentCardFacade {

    private final UserRepository userRepository;
    private final UserPaymentMapper userPaymentMapper;

    @Override
    public List<UserPaymentCardResponse> getUserPaymentCards(UUID userId) {
        return userRepository.getById(userId)
                .getPaymentCards()
                .stream()
                .map(userPaymentMapper::mapToUserPaymentCardResponse)
                .collect(Collectors.toList());
    }

    @Override
    public UserPaymentCardResponse createUserPaymentCard(UUID userId, UserPaymentCardRequest request) {
        User user = userRepository.getById(userId);
        UserPaymentCard paymentCard = userPaymentMapper.mapToUserPaymentCard(request);
        user.addPaymentCard(paymentCard);
        userRepository.save(user);
        return userPaymentMapper.mapToUserPaymentCardResponse(paymentCard);
    }

    @Override
    public UserPaymentCardResponse updateUserPaymentCard(UUID userId, UUID paymentCardId, UserPaymentCardRequest request) {
        User user = userRepository.getById(userId);
        UserPaymentCard paymentCard = user.getPaymentCard(paymentCardId);
        userPaymentMapper.updateUserPaymentCard(request, paymentCard);
        userRepository.save(user);
        return userPaymentMapper.mapToUserPaymentCardResponse(paymentCard);
    }

    @Override
    public void deleteUserPaymentCard(UUID userId, UUID paymentCardId) {
        User user = userRepository.getById(userId);
        user.removePaymentCard(paymentCardId);
        userRepository.save(user);
    }

}
