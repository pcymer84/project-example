package com.shop.user.facade;

import com.shop.core.annotations.ApplicationService;
import com.shop.core.logger.Log;
import com.shop.user.dto.UserAddressRequest;
import com.shop.user.dto.UserAddressResponse;
import com.shop.user.mapper.UserAddressMapper;
import com.shop.user.model.User;
import com.shop.user.model.UserAddress;
import com.shop.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Log
@AllArgsConstructor
@ApplicationService
public class UserAddressFacadeImpl implements UserAddressFacade {

    private final UserRepository userRepository;
    private final UserAddressMapper userAddressMapper;

    @Override
    public List<UserAddressResponse> getUserAddresses(UUID userId) {
        return userRepository.getById(userId)
                .getAddresses()
                .stream()
                .map(userAddressMapper::mapToUserAddressResponse)
                .collect(Collectors.toList());
    }

    @Override
    public UserAddressResponse createUserAddress(UUID userId, UserAddressRequest request) {
        User user = userRepository.getById(userId);
        UserAddress userAddress = userAddressMapper.mapToUserAddress(request);
        user.addAddress(userAddress);
        userRepository.save(user);
        return userAddressMapper.mapToUserAddressResponse(userAddress);
    }

    @Override
    public UserAddressResponse updateUserAddress(UUID userId, UUID addressId, UserAddressRequest request) {
        User user = userRepository.getById(userId);
        UserAddress userAddress = user.getAddress(addressId);
        userAddressMapper.updateUserAddress(request, userAddress);
        userRepository.save(user);
        return userAddressMapper.mapToUserAddressResponse(userAddress);
    }

    @Override
    public void deleteUserAddress(UUID userId, UUID addressId) {
        User user = userRepository.getById(userId);
        user.removeAddress(addressId);
        userRepository.save(user);
    }

}
