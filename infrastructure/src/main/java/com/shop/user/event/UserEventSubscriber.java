package com.shop.user.event;

import com.google.common.eventbus.Subscribe;
import com.shop.account.event.RegistrationEvent;
import com.shop.core.eventBus.EventSubscriber;
import com.shop.core.logger.Log;
import com.shop.user.model.User;
import com.shop.user.model.UserFactory;
import com.shop.user.repository.UserRepository;
import lombok.AllArgsConstructor;

@Log
@EventSubscriber
@AllArgsConstructor
public class UserEventSubscriber {

    private final UserFactory userFactory;
    private final UserRepository userRepository;

    @Subscribe
    public void handleRegistrationEvent(RegistrationEvent event) {
        User user = userFactory.createUserProfileForAccount(event.getAccount());
        user.setFirstName(event.getFirstName());
        user.setLastName(event.getLastName());
        userRepository.save(user);
    }

}
