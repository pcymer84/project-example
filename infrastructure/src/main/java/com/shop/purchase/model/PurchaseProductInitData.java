package com.shop.purchase.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.UUID;
import static com.shop.product.model.ProductInitData.*;

@AllArgsConstructor
@Getter
public enum PurchaseProductInitData {

    PURCHASE_PRODUCT_1(
            UUID.fromString("691eca05-5f04-400a-bac1-7d3633b5e5d0"),
            1,
            PRODUCT_1.getId(),
            PRODUCT_1.getSellerId(),
            PRODUCT_1.getName(),
            PRODUCT_1.getPrice(),
            PRODUCT_1.getDescription(),
            PRODUCT_1.getImageUrl()
    ),
    PURCHASE_PRODUCT_2(
            UUID.fromString("b2d5303e-fd75-4d91-8c63-3bf8974994a6"),
            1,
            PRODUCT_2.getId(),
            PRODUCT_2.getSellerId(),
            PRODUCT_2.getName(),
            PRODUCT_2.getPrice(),
            PRODUCT_2.getDescription(),
            PRODUCT_2.getImageUrl()
    ),
    PURCHASE_PRODUCT_3(
            UUID.fromString("22f4d8f4-f36b-4ea4-ab23-bb710c8e4060"),
            1,
            PRODUCT_3.getId(),
            PRODUCT_3.getSellerId(),
            PRODUCT_3.getName(),
            PRODUCT_3.getPrice(),
            PRODUCT_3.getDescription(),
            PRODUCT_3.getImageUrl()
    ),
    PURCHASE_PRODUCT_4(
            UUID.fromString("27334bae-75a6-4fdd-9276-e19b08141675"),
            1,
            PRODUCT_4.getId(),
            PRODUCT_4.getSellerId(),
            PRODUCT_4.getName(),
            PRODUCT_4.getPrice(),
            PRODUCT_4.getDescription(),
            PRODUCT_4.getImageUrl()
    );

    private UUID id;
    private Integer amount;
    private UUID productId;
    private UUID sellerId;
    private String name;
    private Double price;
    private String description;
    private String imageUrl;

}
