package com.shop.purchase.model;

import com.shop.core.config.SpringProfile;
import com.shop.purchase.dto.DeliveryRequest;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Component
public class DeliveryInitDataFactory {

    public Delivery createDelivery(DeliveryInitData testDelivery) {
        return Delivery.builder()
                .postcode(testDelivery.getPostcode())
                .city(testDelivery.getCity())
                .street(testDelivery.getStreet())
                .description(testDelivery.getDescription())
                .build();
    }

    public DeliveryRequest createDeliveryRequest(DeliveryInitData testDelivery) {
        return DeliveryRequest.builder()
                .postcode(testDelivery.getPostcode())
                .city(testDelivery.getCity())
                .street(testDelivery.getStreet())
                .description(testDelivery.getDescription())
                .build();
    }

}
