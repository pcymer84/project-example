package com.shop.purchase.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DeliveryInitData {

    DELIVERY_1(
        "postcode",
        "city",
        "street",
        "description"
    ),
    DELIVERY_2(
            "postcode",
            "city",
            "street",
            "description"
    ),
    UPDATE_DELIVERY(
            "update postcode",
            "update city",
            "update street",
            "update description"
    );

    private String postcode;
    private String city;
    private String street;
    private String description;

}
