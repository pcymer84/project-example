package com.shop.purchase.model;

import com.shop.core.logger.Log;
import com.shop.product.model.Product;
import com.shop.product.repository.ProductRepository;
import com.shop.purchase.dto.PurchaseProductRequest;
import com.shop.purchase.mapper.ProductDetailsMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Log
@AllArgsConstructor
@Component
public class PurchaseProductFactory {

    private final ProductRepository productRepository;
    private final ProductDetailsMapper productDetailsMapper;

    public Set<PurchaseProduct> createPurchaseProducts(List<PurchaseProductRequest> purchaseProducts) {
        return purchaseProducts.stream().map(this::createPurchaseProduct).collect(Collectors.toSet());
    }

    public PurchaseProduct createPurchaseProduct(PurchaseProductRequest purchaseProduct) {
        return createPurchaseProduct(purchaseProduct.getProductId(), purchaseProduct.getAmount());
    }

    public PurchaseProduct createPurchaseProduct(UUID productId, Integer amount) {
        Product product = productRepository.getById(productId);
        product.checkAvailability();
        return PurchaseProduct.builder()
                .product(productDetailsMapper.mapToProductDetails(product))
                .amount(amount)
                .build();
    }

}
