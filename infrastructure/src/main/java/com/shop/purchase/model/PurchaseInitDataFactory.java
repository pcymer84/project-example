package com.shop.purchase.model;

import com.shop.core.config.SpringProfile;
import com.shop.purchase.dto.PurchaseProductRequest;
import com.shop.purchase.dto.PurchaseRequest;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Component
@AllArgsConstructor
public class PurchaseInitDataFactory {

    private final DeliveryInitDataFactory deliveryFactory;
    private final InvoiceInitDataFactory invoiceFactory;
    private final PurchaseProductInitDataFactory purchaseProductFactory;

    public Purchase createPurchase(PurchaseInitData testPurchase) {
        return Purchase.builder()
                .id(testPurchase.getId())
                .buyerId(testPurchase.getBuyerId())
                .sellerId(testPurchase.getSellerId())
                .invoice(invoiceFactory.createInvoice(testPurchase.getInvoice()))
                .delivery(deliveryFactory.createDelivery(testPurchase.getDelivery()))
                .status(testPurchase.getStatus())
                .createdAt(testPurchase.getCreatedAt())
                .realizedAt(testPurchase.getProcessedAt())
                .sendAt(testPurchase.getSendAt())
                .finishedAt(testPurchase.getFinishedAt())
                .purchaseProducts(createPurchaseProducts(testPurchase))
                .build();
    }

    public PurchaseRequest createPurchaseRequest(PurchaseInitData testPurchase) {
        return PurchaseRequest.builder()
                .products(createPurchaseProductsRequest(testPurchase))
                .invoice(invoiceFactory.createInvoiceRequest(testPurchase.getInvoice()))
                .delivery(deliveryFactory.createDeliveryRequest(testPurchase.getDelivery()))
                .build();
    }

    private Set<PurchaseProduct> createPurchaseProducts(PurchaseInitData testPurchase) {
        return testPurchase.getPurchaseProducts()
                .stream()
                .map(purchaseProductFactory::createPurchaseProduct)
                .collect(Collectors.toSet());
    }

    private List<PurchaseProductRequest> createPurchaseProductsRequest(PurchaseInitData testPurchase) {
        return testPurchase.getPurchaseProducts()
                .stream()
                .map(purchaseProduct -> new PurchaseProductRequest(purchaseProduct.getProductId(), purchaseProduct.getAmount()))
                .collect(Collectors.toList());
    }

}
