package com.shop.purchase.model;

import com.shop.core.logger.Log;
import com.shop.purchase.mapper.DeliveryMapper;
import com.shop.purchase.mapper.InvoiceMapper;
import com.shop.purchase.dto.PurchaseRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Log
@AllArgsConstructor
@Component
public class PurchaseFactory {

    private final PurchaseProductFactory purchaseProductFactory;
    private final DeliveryMapper deliveryMapper;
    private final InvoiceMapper invoiceMapper;

    public Purchase createPurchaseToSubmit(UUID buyerId, PurchaseRequest request) {
        Set<PurchaseProduct> purchaseProducts = purchaseProductFactory.createPurchaseProducts(request.getProducts());
        return Purchase.builder()
                .with(builder -> withSellerId(builder, purchaseProducts))
                .buyerId(buyerId)
                .purchaseProducts(purchaseProducts)
                .delivery(deliveryMapper.mapToDelivery(request.getDelivery()))
                .invoice(invoiceMapper.mapToInvoice(request.getInvoice()))
                .status(PurchaseStatus.OPEN)
                .createdAt(new Date())
                .build();
    }

    private Purchase.PurchaseBuilder withSellerId(Purchase.PurchaseBuilder builder, Set<PurchaseProduct> purchaseProducts) {
        purchaseProducts.stream().findAny().ifPresent(
                purchaseProduct -> builder.sellerId(purchaseProduct.getProduct().getSellerId())
        );
        return builder;
    }

}
