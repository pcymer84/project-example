package com.shop.purchase.model;

import com.shop.core.config.SpringProfile;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Component
public class PurchaseProductInitDataFactory {

    public PurchaseProduct createPurchaseProduct(PurchaseProductInitData purchaseProductInitData) {
        return PurchaseProduct.builder()
                .id(purchaseProductInitData.getId())
                .product(createProductDetails(purchaseProductInitData))
                .amount(purchaseProductInitData.getAmount())
                .build();
    }

    public ProductDetails createProductDetails(PurchaseProductInitData purchaseProductInitData) {
        return ProductDetails.builder()
                .productId(purchaseProductInitData.getProductId())
                .sellerId(purchaseProductInitData.getSellerId())
                .name(purchaseProductInitData.getName())
                .price(purchaseProductInitData.getPrice())
                .description(purchaseProductInitData.getDescription())
                .imageUrl(purchaseProductInitData.getImageUrl())
                .build();
    }

}
