package com.shop.purchase.model;

import com.shop.core.config.SpringProfile;
import com.shop.purchase.dto.InvoiceRequest;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Component
public class InvoiceInitDataFactory {

    public Invoice createInvoice(InvoiceInitData testInvoice) {
        return Invoice.builder()
                .name(testInvoice.getName())
                .nip(testInvoice.getNip())
                .address(testInvoice.getAddress())
                .date(testInvoice.getDate())
                .build();
    }

    public InvoiceRequest createInvoiceRequest(InvoiceInitData testInvoice) {
        return InvoiceRequest.builder()
                .name(testInvoice.getName())
                .nip(testInvoice.getNip())
                .address(testInvoice.getAddress())
                .date(testInvoice.getDate())
                .build();
    }

}
