package com.shop.purchase.model;

import com.google.common.collect.Lists;
import com.shop.user.model.UserInitData;
import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@AllArgsConstructor
public enum PurchaseInitData {

    PURCHASE_1(
            UUID.fromString("4df4093b-4964-4120-bd95-4c59279c1c2a"),
            UserInitData.USER_1.getId(),
            UserInitData.USER_2.getId(),
            InvoiceInitData.INVOICE_1,
            DeliveryInitData.DELIVERY_1,
            PurchaseStatus.OPEN,
            new Date(),
            null,
            null,
            null,
            Lists.newArrayList(PurchaseProductInitData.PURCHASE_PRODUCT_2, PurchaseProductInitData.PURCHASE_PRODUCT_3)
    ),
    PURCHASE_2(
            UUID.fromString("82672f71-d1e5-4036-9656-d182da85f514"),
            UserInitData.USER_2.getId(),
            UserInitData.USER_1.getId(),
            InvoiceInitData.INVOICE_2,
            DeliveryInitData.DELIVERY_2,
            PurchaseStatus.REALIZED,
            new Date(),
            new Date(),
            null,
            null,
            Lists.newArrayList(PurchaseProductInitData.PURCHASE_PRODUCT_1)
    ),
    PURCHASE_3(
            UUID.fromString("88d664e4-d0a2-406a-add7-65f31d950e14"),
            UserInitData.USER_1.getId(),
            UserInitData.USER_2.getId(),
            InvoiceInitData.INVOICE_1,
            DeliveryInitData.DELIVERY_1,
            PurchaseStatus.FINISHED,
            new Date(),
            new Date(),
            new Date(),
            new Date(),
            Lists.newArrayList(PurchaseProductInitData.PURCHASE_PRODUCT_4)
    ),
    UNSAVED_PURCHASE(
            UUID.fromString("c3b7c399-a086-45e1-9113-18c2dbf82743"),
            UserInitData.USER_1.getId(),
            UserInitData.USER_2.getId(),
            InvoiceInitData.INVOICE_1,
            DeliveryInitData.DELIVERY_1,
            null,
            null,
            null,
            null,
            null,
            Lists.newArrayList(PurchaseProductInitData.PURCHASE_PRODUCT_1)
    );

    private UUID id;
    private UUID buyerId;
    private UUID sellerId;
    private InvoiceInitData invoice;
    private DeliveryInitData delivery;
    private PurchaseStatus status;
    private Date createdAt;
    private Date processedAt;
    private Date sendAt;
    private Date finishedAt;
    private List<PurchaseProductInitData> purchaseProducts;

    public PurchaseProductInitData findPurchaseProduct(UUID productId) {
        return purchaseProducts.stream()
                .filter(purchaseProduct -> purchaseProduct.getProductId() == productId)
                .findFirst()
                .orElseThrow(() -> new RuntimeException("can't find purchase product"));
    }

    public PurchaseProductInitData getFirstPurchaseProduct() {
        return purchaseProducts.stream().findFirst().orElseThrow(() -> new RuntimeException("can't find purchase product"));
    }

}
