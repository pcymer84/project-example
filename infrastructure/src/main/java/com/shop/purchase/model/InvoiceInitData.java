package com.shop.purchase.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.Date;

@AllArgsConstructor
@Getter
public enum InvoiceInitData {

    INVOICE_1(
            "name",
            "nip",
            "address",
            new Date()
    ),
    INVOICE_2(
            "name",
            "nip",
            "address",
            new Date()
    ),
    UPDATE_INVOICE(
            "update name",
            "update nip",
            "update address",
            new Date()
    );

    private String name;
    private String nip;
    private String address;
    private Date date;

}
