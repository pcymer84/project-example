package com.shop.purchase.repository;

import com.shop.core.annotations.Repository;
import com.shop.purchase.ex.PurchaseNotFoundException;
import com.shop.purchase.model.Purchase;
import lombok.AllArgsConstructor;
import java.util.UUID;

@AllArgsConstructor
@Repository
public class CustomPurchaseRepositoryImpl implements CustomPurchaseRepository {

    private final CrudPurchaseRepository purchaseRepository;

    @Override
    public Purchase getById(UUID purchaseOrderId) {
        return purchaseRepository.findById(purchaseOrderId).orElseThrow(() -> new PurchaseNotFoundException(String.format("purchase with id '%s' not found", purchaseOrderId)));
    }

    @Override
    public Purchase getByIdAndBuyerId(UUID id, UUID buyerId) {
        return purchaseRepository.findByIdAndBuyerId(id, buyerId).orElseThrow(() -> new PurchaseNotFoundException(String.format("purchase with id '%s' for buyer '%s' not found", id, buyerId)));
    }

    @Override
    public Purchase getByIdAndSellerId(UUID id, UUID sellerId) {
        return purchaseRepository.findByIdAndSellerId(id, sellerId).orElseThrow(() -> new PurchaseNotFoundException(String.format("purchase with id '%s' for seller '%s' not found", id, sellerId)));
    }

}
