package com.shop.purchase.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
public class CancelPurchaseEvent {

    private UUID purchaseId;
    private List<PurchaseProductDetails> purchaseProducts;

}
