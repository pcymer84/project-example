package com.shop.purchase.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.UUID;

@Data
@AllArgsConstructor
public class PurchaseProductDetails {

    private UUID productId;
    private Integer amount;

}
