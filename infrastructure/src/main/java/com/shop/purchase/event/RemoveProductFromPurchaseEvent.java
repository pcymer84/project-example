package com.shop.purchase.event;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RemoveProductFromPurchaseEvent {

    private PurchaseProductDetails details;

}
