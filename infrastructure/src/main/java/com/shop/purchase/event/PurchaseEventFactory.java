package com.shop.purchase.event;

import com.shop.purchase.model.Purchase;
import com.shop.purchase.model.PurchaseProduct;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PurchaseEventFactory {

    public AddProductToPurchaseEvent createAddProductToPurchaseEvent(PurchaseProduct purchaseProduct) {
        return new AddProductToPurchaseEvent(createFromPurchaseProduct(purchaseProduct));
    }

    public RemoveProductFromPurchaseEvent createRemoveProductFromPurchaseEvent(PurchaseProduct purchaseProduct) {
        return new RemoveProductFromPurchaseEvent(createFromPurchaseProduct(purchaseProduct));
    }

    public SubmitPurchaseEvent createSubmitPurchaseEvent(Purchase purchase) {
        return new SubmitPurchaseEvent(purchase.getId(), createFromPurchase(purchase));
    }

    public CancelPurchaseEvent createCancelPurchaseEvent(Purchase purchase) {
        return new CancelPurchaseEvent(purchase.getId(), createFromPurchase(purchase));
    }

    private List<PurchaseProductDetails> createFromPurchase(Purchase purchase) {
        return purchase.getPurchaseProducts().stream()
                .map(this::createFromPurchaseProduct)
                .collect(Collectors.toList());
    }

    private PurchaseProductDetails createFromPurchaseProduct(PurchaseProduct purchaseProduct) {
        return new PurchaseProductDetails(
                purchaseProduct.getProduct().getProductId(),
                purchaseProduct.getAmount());
    }

}
