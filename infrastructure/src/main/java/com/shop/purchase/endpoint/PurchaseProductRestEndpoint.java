package com.shop.purchase.endpoint;

import com.shop.account.model.AccountDetails;
import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.purchase.dto.PurchaseProductResponse;
import com.shop.purchase.facade.PurchaseFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class PurchaseProductRestEndpoint {

    private final PurchaseFacade purchaseFacade;

    @PutMapping("/purchases/{purchaseId}/products/{productId}/amount/{amount}")
    public PurchaseProductResponse addProductToPurchase(@AuthenticationPrincipal AccountDetails account,
                                                        @PathVariable("purchaseId") UUID purchaseId,
                                                        @PathVariable("productId") UUID productId,
                                                        @PathVariable("amount") Integer amount) {
        return purchaseFacade.addProductToPurchase(account.getId(), purchaseId, productId, amount);
    }

    @DeleteMapping("/purchases/{purchaseId}/products/{productId}")
    public void removeProductFromPurchase(@AuthenticationPrincipal AccountDetails account,
                                                @PathVariable("purchaseId") UUID purchaseId,
                                                @PathVariable("productId") UUID productId) {
        purchaseFacade.removeProductFromPurchase(account.getId(), purchaseId, productId);
    }

}
