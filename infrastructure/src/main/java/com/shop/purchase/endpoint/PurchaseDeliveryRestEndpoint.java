package com.shop.purchase.endpoint;

import com.shop.account.model.AccountDetails;
import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.purchase.dto.DeliveryRequest;
import com.shop.purchase.dto.DeliveryResponse;
import com.shop.purchase.facade.PurchaseFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.UUID;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class PurchaseDeliveryRestEndpoint {

    private final PurchaseFacade purchaseFacade;

    @GetMapping("/purchases/{purchaseId}/delivery")
    public DeliveryResponse getPurchaseDelivery(@AuthenticationPrincipal AccountDetails account,
                                                @PathVariable("purchaseId") UUID purchaseId) {
        return purchaseFacade.getPurchaseDelivery(account.getId(), purchaseId);
    }

    @PutMapping("/purchases/{purchaseId}/delivery")
    public DeliveryResponse changePurchaseDelivery(@AuthenticationPrincipal AccountDetails account,
                                           @PathVariable("purchaseId") UUID purchaseId,
                                           @Valid @RequestBody DeliveryRequest request) {
        return purchaseFacade.changePurchaseDelivery(account.getId(), purchaseId, request);
    }

}
