package com.shop.purchase.endpoint;

import com.shop.account.model.AccountDetails;
import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.purchase.dto.PurchaseRequest;
import com.shop.purchase.dto.PurchaseResponse;
import com.shop.purchase.facade.PurchaseFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.UUID;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class PurchaseRestEndpoint {

    private final PurchaseFacade purchaseFacade;

    @GetMapping("/purchases/{purchaseId}")
    public PurchaseResponse getPurchase(@AuthenticationPrincipal AccountDetails account,
                                        @PathVariable("purchaseId") UUID purchaseId) {
        return purchaseFacade.getPurchase(account.getId(), purchaseId);
    }

    @PostMapping(path = "/purchases")
    public PurchaseResponse submitPurchase(@AuthenticationPrincipal AccountDetails account,
                                           @Valid @RequestBody PurchaseRequest request) {
        return purchaseFacade.submitPurchase(account.getId(), request);
    }

    @PostMapping("/purchases/{purchaseId}/cancel")
    public PurchaseResponse cancelPurchase(@AuthenticationPrincipal AccountDetails account,
                                           @PathVariable("purchaseId") UUID purchaseId) {
        return purchaseFacade.cancelPurchase(account.getId(), purchaseId);
    }

    @PostMapping(path = "/purchases/{purchaseId}/realize")
    public PurchaseResponse realizePurchase(@AuthenticationPrincipal AccountDetails account,
                                           @PathVariable("purchaseId") UUID purchaseId) {
        return purchaseFacade.realizePurchase(account.getId(), purchaseId);
    }

    @PostMapping("/purchases/{purchaseId}/send")
    public PurchaseResponse sendPurchase(@AuthenticationPrincipal AccountDetails account,
                                           @PathVariable("purchaseId") UUID purchaseId) {
        return purchaseFacade.sendPurchase(account.getId(), purchaseId);
    }

    @PostMapping("/purchases/{purchaseId}/finish")
    public PurchaseResponse finishPurchase(@AuthenticationPrincipal AccountDetails account,
                                           @PathVariable("purchaseId") UUID purchaseId) {
        return purchaseFacade.finishPurchase(account.getId(), purchaseId);
    }

}
