package com.shop.purchase.endpoint;

import com.shop.account.model.AccountDetails;
import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.purchase.dto.InvoiceRequest;
import com.shop.purchase.dto.InvoiceResponse;
import com.shop.purchase.facade.PurchaseFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.UUID;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class PurchaseInvoiceRestEndpoint {

    private final PurchaseFacade purchaseFacade;

    @GetMapping("/purchases/{purchaseId}/invoice")
    public InvoiceResponse getPurchaseInvoice(@AuthenticationPrincipal AccountDetails account,
                                              @PathVariable("purchaseId") UUID purchaseId) {
        return purchaseFacade.getPurchaseInvoice(account.getId(), purchaseId);
    }

    @PutMapping(path = "/purchases/{purchaseId}/invoice")
    public InvoiceResponse changePurchaseInvoice(@AuthenticationPrincipal AccountDetails account,
                                         @PathVariable("purchaseId") UUID purchaseId,
                                         @Valid @RequestBody InvoiceRequest request) {
        return purchaseFacade.changePurchaseInvoice(account.getId(), purchaseId, request);
    }

}
