package com.shop.purchase.mapper;

import com.shop.purchase.dto.PurchaseProductResponse;
import com.shop.purchase.dto.PurchaseResponse;
import com.shop.purchase.model.Purchase;
import com.shop.purchase.model.PurchaseProduct;

public interface PurchaseMapper {

    PurchaseResponse mapToPurchaseOrderResponse(Purchase purchase);

    PurchaseProductResponse mapToPurchaseProductResponse(PurchaseProduct purchaseProduct);

}
