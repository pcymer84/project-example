package com.shop.purchase.mapper;

import com.shop.product.model.Product;
import com.shop.purchase.model.ProductDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface ProductDetailsMapperMapstruct extends ProductDetailsMapper {

    @Mappings({@Mapping(source = "id", target = "productId")})
    ProductDetails mapToProductDetails(Product product);

}
