package com.shop.purchase.mapper;

import com.shop.purchase.dto.PurchaseResponse;
import com.shop.purchase.dto.PurchaseProductResponse;
import com.shop.purchase.model.Purchase;
import com.shop.purchase.model.PurchaseProduct;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface PurchaseMapperMapstruct extends PurchaseMapper {

    @Mappings({@Mapping(source = "purchaseProducts", target = "products")})
    PurchaseResponse mapToPurchaseOrderResponse(Purchase purchase);

    @Mappings({
            @Mapping(source = "product.sellerId", target = "sellerId"),
            @Mapping(source = "product.name", target = "name"),
            @Mapping(source = "product.price", target = "price"),
            @Mapping(source = "product.productId", target = "productId"),
            @Mapping(source = "product.description", target = "description"),
            @Mapping(source = "product.imageUrl", target = "imageUrl")
    })
    PurchaseProductResponse mapToPurchaseProductResponse(PurchaseProduct purchaseProduct);

}
