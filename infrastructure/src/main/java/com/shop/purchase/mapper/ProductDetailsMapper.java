package com.shop.purchase.mapper;

import com.shop.product.model.Product;
import com.shop.purchase.model.ProductDetails;

public interface ProductDetailsMapper {

    ProductDetails mapToProductDetails(Product product);

}
