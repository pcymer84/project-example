package com.shop.purchase.mapper;

import com.shop.purchase.dto.InvoiceRequest;
import com.shop.purchase.dto.InvoiceResponse;
import com.shop.purchase.model.Invoice;

public interface InvoiceMapper {

    Invoice mapToInvoice(InvoiceRequest invoiceRequest);

    InvoiceResponse mapToInvoiceResponse(Invoice Invoice);

}
