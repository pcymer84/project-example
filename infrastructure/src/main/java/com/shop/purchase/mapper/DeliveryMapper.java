package com.shop.purchase.mapper;

import com.shop.purchase.dto.DeliveryRequest;
import com.shop.purchase.dto.DeliveryResponse;
import com.shop.purchase.model.Delivery;

public interface DeliveryMapper {

    Delivery mapToDelivery(DeliveryRequest deliveryRequest);

    DeliveryResponse mapToDeliveryResponse(Delivery delivery);

}
