package com.shop.purchase.facade;

import com.shop.core.annotations.ApplicationService;
import com.shop.core.eventBus.EventPublisher;
import com.shop.core.logger.Log;
import com.shop.purchase.dto.DeliveryResponse;
import com.shop.purchase.dto.InvoiceResponse;
import com.shop.purchase.dto.PurchaseProductResponse;
import com.shop.purchase.event.PurchaseEventFactory;
import com.shop.purchase.model.PurchaseFactory;
import com.shop.purchase.model.PurchaseProductFactory;
import com.shop.purchase.mapper.DeliveryMapper;
import com.shop.purchase.mapper.InvoiceMapper;
import com.shop.purchase.mapper.PurchaseMapper;
import com.shop.purchase.dto.DeliveryRequest;
import com.shop.purchase.dto.InvoiceRequest;
import com.shop.purchase.dto.PurchaseRequest;
import com.shop.purchase.dto.PurchaseResponse;
import com.shop.purchase.model.Delivery;
import com.shop.purchase.model.Invoice;
import com.shop.purchase.model.Purchase;
import com.shop.purchase.model.PurchaseProduct;
import com.shop.purchase.repository.PurchaseRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@Log
@AllArgsConstructor
@ApplicationService
public class PurchaseFacadeImpl implements PurchaseFacade {

    private final PurchaseFactory purchaseFactory;
    private final PurchaseRepository purchaseRepository;
    private final PurchaseProductFactory purchaseProductFactory;
    private final PurchaseMapper purchaseMapper;
    private final DeliveryMapper deliveryMapper;
    private final InvoiceMapper invoiceMapper;
    private final EventPublisher eventPublisher;
    private final PurchaseEventFactory purchaseEventFactory;

    @Override
    public PurchaseResponse getPurchase(UUID buyerId, UUID purchaseId) {
        Purchase purchase = purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId);
        return purchaseMapper.mapToPurchaseOrderResponse(purchase);
    }

    @Override
    public InvoiceResponse getPurchaseInvoice(UUID buyerId, UUID purchaseId) {
        Purchase purchase = purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId);
        return invoiceMapper.mapToInvoiceResponse(purchase.getInvoice());
    }

    @Override
    public DeliveryResponse getPurchaseDelivery(UUID buyerId, UUID purchaseId) {
        Purchase purchase = purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId);
        return deliveryMapper.mapToDeliveryResponse(purchase.getDelivery());
    }

    @Override
    public PurchaseResponse submitPurchase(UUID buyerId, PurchaseRequest purchaseRequest) {
        Purchase purchase = purchaseFactory.createPurchaseToSubmit(buyerId, purchaseRequest);
        purchaseRepository.save(purchase);
        eventPublisher.publishEvent(purchaseEventFactory.createSubmitPurchaseEvent(purchase));
        return purchaseMapper.mapToPurchaseOrderResponse(purchase);
    }

    @Override
    public InvoiceResponse changePurchaseInvoice(UUID buyerId, UUID purchaseId, InvoiceRequest invoiceRequest) {
        Purchase purchase = purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId);
        Invoice invoice = invoiceMapper.mapToInvoice(invoiceRequest);
        purchase.changeInvoice(invoice);
        purchaseRepository.save(purchase);
        return invoiceMapper.mapToInvoiceResponse(purchase.getInvoice());
    }

    @Override
    public DeliveryResponse changePurchaseDelivery(UUID buyerId, UUID purchaseId, DeliveryRequest deliveryRequest) {
        Purchase purchase = purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId);
        Delivery delivery = deliveryMapper.mapToDelivery(deliveryRequest);
        purchase.changeDelivery(delivery);
        purchaseRepository.save(purchase);
        return deliveryMapper.mapToDeliveryResponse(purchase.getDelivery());
    }

    @Override
    public PurchaseProductResponse addProductToPurchase(UUID buyerId, UUID purchaseId, UUID productId, Integer amount) {
        Purchase purchase = purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId);
        PurchaseProduct purchaseProduct = purchaseProductFactory.createPurchaseProduct(productId, amount);
        purchase.addProduct(purchaseProduct);
        purchaseRepository.save(purchase);
        eventPublisher.publishEvent(purchaseEventFactory.createAddProductToPurchaseEvent(purchaseProduct));
        return purchaseMapper.mapToPurchaseProductResponse(purchaseProduct);
    }

    @Override
    public void removeProductFromPurchase(UUID buyerId, UUID purchaseId, UUID productId) {
        Purchase purchase = purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId);
        PurchaseProduct purchaseProduct = purchase.getPurchaseProduct(productId);
        purchase.removeProduct(purchaseProduct);
        purchaseRepository.save(purchase);
        eventPublisher.publishEvent(purchaseEventFactory.createRemoveProductFromPurchaseEvent(purchaseProduct));
    }

    @Override
    public PurchaseResponse cancelPurchase(UUID buyerId, UUID purchaseId) {
        Purchase purchase = purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId);
        purchase.cancelPurchase();
        purchaseRepository.save(purchase);
        eventPublisher.publishEvent(purchaseEventFactory.createCancelPurchaseEvent(purchase));
        return purchaseMapper.mapToPurchaseOrderResponse(purchase);
    }

    @Override
    public PurchaseResponse realizePurchase(UUID sellerId, UUID purchaseId) {
        Purchase purchase = purchaseRepository.getByIdAndSellerId(purchaseId, sellerId);
        purchase.realizePurchase();
        purchaseRepository.save(purchase);
        return purchaseMapper.mapToPurchaseOrderResponse(purchase);
    }

    @Override
    public PurchaseResponse sendPurchase(UUID sellerId, UUID purchaseId) {
        Purchase purchase = purchaseRepository.getByIdAndSellerId(purchaseId, sellerId);
        purchase.sendPurchase();
        purchaseRepository.save(purchase);
        return purchaseMapper.mapToPurchaseOrderResponse(purchase);
    }

    @Override
    public PurchaseResponse finishPurchase(UUID sellerId, UUID purchaseId) {
        Purchase purchase = purchaseRepository.getByIdAndSellerId(purchaseId, sellerId);
        purchase.finishPurchase();
        purchaseRepository.save(purchase);
        return purchaseMapper.mapToPurchaseOrderResponse(purchase);
    }

}
