package com.shop.product.endpoint;

import com.shop.account.model.AccountDetails;
import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.product.annotations.PermissionToProductManagement;
import com.shop.product.annotations.PermissionToProductStockManagement;
import com.shop.product.dto.ProductRequest;
import com.shop.product.dto.ProductResponse;
import com.shop.product.facade.ProductFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.UUID;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class ProductManagementRestEndpoint {

    private final ProductFacade productFacade;

    @PermissionToProductManagement
    @PostMapping("/products")
    public ProductResponse registerProduct(@AuthenticationPrincipal AccountDetails account,
                                           @Valid @RequestBody ProductRequest productRequest) {
        return productFacade.registerProduct(account.getId(), productRequest);
    }

    @PermissionToProductManagement
    @DeleteMapping("/products/{productId}")
    public void deleteProduct(@AuthenticationPrincipal AccountDetails account,
                              @PathVariable UUID productId) {
        productFacade.deleteProduct(account.getId(), productId);
    }

    @PermissionToProductStockManagement
    @PutMapping("/products/{productId}/stock/add/{amount}")
    public ProductResponse addProductsToStock(@AuthenticationPrincipal AccountDetails account,
                                                    @PathVariable UUID productId,
                                                    @PathVariable Integer amount) {
        return productFacade.addProductsToStock(account.getId(), productId, amount);
    }

    @PermissionToProductStockManagement
    @PutMapping("/products/{productId}/stock/remove/{amount}")
    public ProductResponse removeProductsFromStock(@AuthenticationPrincipal AccountDetails account,
                                                    @PathVariable UUID productId,
                                                    @PathVariable Integer amount) {
        return productFacade.removeProductsFromStock(account.getId(), productId, amount);
    }

}
