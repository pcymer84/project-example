package com.shop.product.endpoint;

import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.product.dto.ProductResponse;
import com.shop.product.facade.ProductFacade;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.UUID;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class ProductRestEndpoint {

    private final ProductFacade productFacade;

    @GetMapping("/products/search")
    public List<ProductResponse> searchProducts(@RequestParam String query) {
        return productFacade.searchProducts(query);
    }

    @GetMapping("/products/{productId}")
    public ProductResponse getProduct(@PathVariable UUID productId) {
        return productFacade.getProduct(productId);
    }

}
