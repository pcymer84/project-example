package com.shop.product.model;

import org.springframework.stereotype.Component;
import java.util.Date;

@Component
public class ProductTransferFactoryImpl implements ProductTransferFactory {

    @Override
    public ProductTransfer createAddProductTransfer(Integer amount) {
        return createProductTransfer(Operation.ADD, amount);
    }

    @Override
    public ProductTransfer createRemoveProductTransfer(Integer amount) {
        return createProductTransfer(Operation.REMOVE, amount);
    }

    private ProductTransfer createProductTransfer(Operation operation, Integer amount) {
        return ProductTransfer.builder()
                .operation(operation)
                .amount(amount)
                .transferDate(new Date())
                .build();
    }

}
