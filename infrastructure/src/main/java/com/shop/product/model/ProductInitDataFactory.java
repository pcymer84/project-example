package com.shop.product.model;

import com.google.common.collect.Sets;
import com.shop.core.config.SpringProfile;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Component
@AllArgsConstructor
public class ProductInitDataFactory {

    private final ProductTransferFactory productTransferFactory;

    public Product createProduct(ProductInitData product) {
        return Product.builder()
                .id(product.getId())
                .sellerId(product.getSellerId())
                .name(product.getName())
                .price(product.getPrice())
                .description(product.getDescription())
                .imageUrl(product.getImageUrl())
                .productTransfers(Sets.newHashSet(productTransferFactory.createAddProductTransfer(product.getStockNumber())))
                .build();
    }

}
