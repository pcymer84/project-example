package com.shop.product.model;

import com.shop.user.model.UserInitData;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.UUID;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ProductInitData {

    PRODUCT_1(
            UUID.fromString("181aaed7-a237-4185-bcd0-28b01fd4497b"),
            UserInitData.USER_1.getId(),
            10,
            "keyboard",
            299.0,
            "gaming keyboard",
            "http://test.com"
    ),
    PRODUCT_2(
            UUID.fromString("0eb69e0c-8523-4782-b5f9-f939e675732a"),
            UserInitData.USER_2.getId(),
            10,
            "monitor",
            999.0,
            "full hd monitor",
            "http://qwe.pl"
    ),
    PRODUCT_3(
            UUID.fromString("de117c97-d95c-47e3-8bad-4deae12ed8ab"),
            UserInitData.USER_2.getId(),
            10,
            "mouse",
            199.0,
            "gaming description",
            "http://asd.pl"
    ),
    PRODUCT_4(
            UUID.fromString("8c28bdbb-1d84-4cd9-8e3b-84991f057040"),
            UserInitData.USER_2.getId(),
            10,
            "headset",
            399.0,
            "dj headset",
            "http://zcc.pl"
    ),
    PRODUCT_5(
            UUID.fromString("3acc52d5-2962-4297-a6df-bf1ca63507f7"),
            UserInitData.USER_2.getId(),
            10,
            "mouse",
            49.0,
            "cheap mouse used in office",
            "http://test.com"
    ),
    UNSAVED_PRODUCT(
            UUID.fromString("f9781254-02d1-4fcb-a93a-304b6092e00b"),
            UserInitData.USER_2.getId(),
            10,
            "bicycle",
            799.0,
            "bicycle used for mountain ridding",
            "http://test.com"
    );

    private UUID id;
    private UUID sellerId;
    private Integer stockNumber;
    private String name;
    private Double price;
    private String description;
    private String imageUrl;

}
