package com.shop.product.factory;

import com.shop.core.ex.InvalidSearchSyntaxException;
import com.shop.product.model.Product;
import org.springframework.data.jpa.domain.Specification;

public interface ProductSpecificationFactory {

    Specification<Product> createSpecification(String query) throws InvalidSearchSyntaxException;

}
