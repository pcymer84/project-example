package com.shop.product.factory;

import com.shop.core.search.rsql.RsqlSpecificationVisitor;
import com.shop.core.ex.InvalidSearchSyntaxException;
import com.shop.product.model.Product;
import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.RSQLParserException;
import lombok.AllArgsConstructor;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RsqlProductSpecificationFactory implements ProductSpecificationFactory {

    private final RSQLParser rsqlParser;
    private final RsqlSpecificationVisitor<Product> visitor;

    @Override
    public Specification<Product> createSpecification(String query) {
        try {
            return rsqlParser.parse(query).accept(visitor);
        } catch (InvalidDataAccessApiUsageException | RSQLParserException e) {
            throw new InvalidSearchSyntaxException("invalid search syntax", e);
        }
    }

}
