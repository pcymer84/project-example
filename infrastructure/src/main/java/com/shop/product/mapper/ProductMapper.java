package com.shop.product.mapper;

import com.shop.product.dto.ProductRequest;
import com.shop.product.dto.ProductResponse;
import com.shop.product.model.Product;
import java.util.UUID;

public interface ProductMapper {

    Product mapToProduct(UUID sellerId, ProductRequest request);

    ProductResponse mapToProductResponse(Product product);

}
