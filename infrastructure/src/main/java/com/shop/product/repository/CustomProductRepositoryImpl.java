package com.shop.product.repository;

import com.shop.core.annotations.Repository;
import com.shop.product.ex.ProductNotFoundException;
import com.shop.product.model.Product;
import lombok.AllArgsConstructor;
import java.util.UUID;

@AllArgsConstructor
@Repository
public class CustomProductRepositoryImpl implements CustomProductRepository {

    private final CrudProductRepository productRepository;

    @Override
    public Product getById(UUID productId) {
        return productRepository.findById(productId).orElseThrow(() -> new ProductNotFoundException(String.format("product with id '%s' not found", productId)));
    }

    @Override
    public Product getByIdAndSellerId(UUID productId, UUID sellerId) {
        return productRepository.findByIdAndSellerId(productId, sellerId).orElseThrow(() -> new ProductNotFoundException(String.format("product with id '%s' for seller '%s' not found", productId, sellerId)));
    }

}
