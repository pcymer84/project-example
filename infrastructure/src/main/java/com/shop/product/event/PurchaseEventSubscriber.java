package com.shop.product.event;

import com.google.common.eventbus.Subscribe;
import com.shop.core.eventBus.EventSubscriber;
import com.shop.core.logger.Log;
import com.shop.product.facade.ProductStockFacade;
import com.shop.purchase.event.AddProductToPurchaseEvent;
import com.shop.purchase.event.CancelPurchaseEvent;
import com.shop.purchase.event.PurchaseProductDetails;
import com.shop.purchase.event.SubmitPurchaseEvent;
import com.shop.purchase.event.RemoveProductFromPurchaseEvent;
import lombok.AllArgsConstructor;

@Log
@EventSubscriber
@AllArgsConstructor
public class PurchaseEventSubscriber {

    private final ProductStockFacade productStockFacade;

    @Subscribe
    public void handleSubmitPurchaseEvent(SubmitPurchaseEvent event) {
        event.getPurchaseProducts().forEach(details -> productStockFacade.removeProductsFromStock(details.getProductId(), details.getAmount()));
    }

    @Subscribe
    public void handleCancelPurchaseEvent(CancelPurchaseEvent event) {
        event.getPurchaseProducts().forEach(details -> productStockFacade.addProductsToStock(details.getProductId(), details.getAmount()));
    }

    @Subscribe
    public void handleAddProductToPurchaseEvent(AddProductToPurchaseEvent event) {
        PurchaseProductDetails details = event.getDetails();
        productStockFacade.removeProductsFromStock(details.getProductId(), details.getAmount());
    }

    @Subscribe
    public void handleRemoveProductFromPurchaseEvent(RemoveProductFromPurchaseEvent event) {
        PurchaseProductDetails details = event.getDetails();
        productStockFacade.addProductsToStock(details.getProductId(), details.getAmount());
    }

}
