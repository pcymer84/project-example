package com.shop.product.facade;

import com.shop.core.annotations.ApplicationService;
import com.shop.core.logger.Log;
import com.shop.product.dto.ProductRequest;
import com.shop.product.dto.ProductResponse;
import com.shop.product.mapper.ProductMapper;
import com.shop.product.model.Product;
import com.shop.product.model.ProductTransfer;
import com.shop.product.model.ProductTransferFactory;
import com.shop.product.repository.ProductRepository;
import com.shop.product.factory.ProductSpecificationFactory;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Log
@AllArgsConstructor
@ApplicationService
public class ProductFacadeImpl implements ProductFacade {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final ProductTransferFactory productTransferFactory;
    private final ProductSpecificationFactory productSpecificationFactory;

    @Override
    public List<ProductResponse> searchProducts(String query) {
        return productRepository.findAll(productSpecificationFactory.createSpecification(query))
                .stream()
                .map(productMapper::mapToProductResponse)
                .collect(Collectors.toList());
    }

    @Override
    public ProductResponse getProduct(UUID productId) {
        return productMapper.mapToProductResponse(productRepository.getById(productId));
    }

    @Override
    public ProductResponse registerProduct(UUID sellerId, ProductRequest request) {
        Product product = productMapper.mapToProduct(sellerId, request);
        request.getStockNumber();
        ProductTransfer addProductsTransfer = productTransferFactory.createAddProductTransfer(request.getStockNumber());
        product.addProductTransfer(addProductsTransfer);
        productRepository.save(product);
        return productMapper.mapToProductResponse(product);
    }

    @Override
    public ProductResponse addProductsToStock(UUID sellerId, UUID productId, Integer amount) {
        Product product = productRepository.getByIdAndSellerId(productId, sellerId);
        ProductTransfer addProductsTransfer = productTransferFactory.createAddProductTransfer(amount);
        product.addProductTransfer(addProductsTransfer);
        productRepository.save(product);
        return productMapper.mapToProductResponse(product);
    }

    @Override
    public ProductResponse removeProductsFromStock(UUID sellerId, UUID productId, Integer amount) {
        Product product = productRepository.getByIdAndSellerId(productId, sellerId);
        ProductTransfer removeProductsTransfer = productTransferFactory.createRemoveProductTransfer(amount);
        product.addProductTransfer(removeProductsTransfer);
        productRepository.save(product);
        return productMapper.mapToProductResponse(product);
    }

    @Override
    public void deleteProduct(UUID sellerId, UUID productId) {
        Product product = productRepository.getByIdAndSellerId(productId, sellerId);
        productRepository.delete(product);
    }

}
