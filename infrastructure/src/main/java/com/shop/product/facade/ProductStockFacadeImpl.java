package com.shop.product.facade;

import com.shop.core.annotations.ApplicationService;
import com.shop.core.logger.Log;
import com.shop.product.model.Product;
import com.shop.product.model.ProductTransfer;
import com.shop.product.model.ProductTransferFactory;
import com.shop.product.repository.ProductRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@Log
@AllArgsConstructor
@ApplicationService
public class ProductStockFacadeImpl implements ProductStockFacade {

    private final ProductRepository productRepository;
    private final ProductTransferFactory productTransferFactory;

    @Override
    public void addProductsToStock(UUID productId, Integer amount) {
        Product product = productRepository.getById(productId);
        ProductTransfer addProductsTransfer = productTransferFactory.createAddProductTransfer(amount);
        product.addProductTransfer(addProductsTransfer);
        productRepository.save(product);
    }

    @Override
    public void removeProductsFromStock(UUID productId, Integer amount) {
        Product product = productRepository.getById(productId);
        ProductTransfer removeProductsTransfer = productTransferFactory.createRemoveProductTransfer(amount);
        product.addProductTransfer(removeProductsTransfer);
        productRepository.save(product);
    }

}
