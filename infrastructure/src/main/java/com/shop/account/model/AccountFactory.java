package com.shop.account.model;

import com.shop.account.dto.RegistrationRequest;
import com.shop.account.policy.EmailPolicy;
import com.shop.account.policy.FacebookPolicy;
import com.shop.account.policy.PasswordPolicy;
import com.shop.core.logger.Log;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Log
@Component
@AllArgsConstructor
public class AccountFactory {

    private final EmailPolicy emailPolicy;
    private final PasswordPolicy passwordPolicy;
    private final FacebookPolicy facebookPolicy;

    public Account createForRegistration(RegistrationRequest request) {
        emailPolicy.checkEmailUsed(request.getEmail());
        passwordPolicy.checkPasswordStrength(request.getPassword());
        return Account.builder()
                .email(request.getEmail())
                .password(passwordPolicy.encodePassword(request.getPassword()))
                .build();
    }

    public Account createFromFacebookUser(FacebookUser facebookUser) {
        emailPolicy.checkEmailUsed(facebookUser.getEmail());
        facebookPolicy.checkFacebookIdUsed(facebookUser.getFacebookId());
        return Account.builder()
                .email(facebookUser.getEmail())
                .facebookId(facebookUser.getFacebookId())
                .build();
    }

}
