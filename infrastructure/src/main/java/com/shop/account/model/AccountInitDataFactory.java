package com.shop.account.model;

import com.shop.core.config.SpringProfile;
import com.shop.user.model.UserInitData;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Component
public class AccountInitDataFactory {

    public Account createAccount(UserInitData user) {
        return Account.builder()
                .id(user.getId())
                .password(user.getHashedPassword())
                .email(user.getEmail())
                .facebookId(user.getFacebookId())
                .resetPasswordCode(user.getResetPasswordCode())
                .roles(user.getRole())
                .build();
    }

}
