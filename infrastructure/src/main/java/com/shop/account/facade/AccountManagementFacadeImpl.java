package com.shop.account.facade;

import com.shop.account.dto.AccountResponse;
import com.shop.account.mapper.AccountMapper;
import com.shop.account.repository.AccountRepository;
import com.shop.core.annotations.ApplicationService;
import com.shop.core.logger.Log;
import com.shop.product.repository.ProductRepository;
import com.shop.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Log
@AllArgsConstructor
@ApplicationService
public class AccountManagementFacadeImpl implements AccountManagementFacade {

    private final AccountRepository accountRepository;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final AccountMapper accountMapper;

    @Override
    public AccountResponse getAccount(UUID accountId) {
        return accountMapper.mapToAccountResponse(accountRepository.getById(accountId));
    }

    @Override
    public List<AccountResponse> getAccounts() {
        return StreamSupport.stream(accountRepository.findAll().spliterator(), false)
                .map(accountMapper::mapToAccountResponse)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteAccount(UUID accountId) {
        accountRepository.deleteById(accountId);
        userRepository.deleteById(accountId);
        productRepository.deleteBySellerId(accountId);
    }

}
