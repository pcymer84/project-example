package com.shop.account.facade;

import com.shop.account.dto.AccountResponse;
import com.shop.account.event.RegistrationEvent;
import com.shop.account.model.AccountFactory;
import com.shop.account.mapper.AccountMapper;
import com.shop.account.model.Account;
import com.shop.account.repository.AccountRepository;
import com.shop.core.annotations.ApplicationService;
import com.shop.account.service.FacebookService;
import com.shop.account.model.FacebookUser;
import com.shop.core.eventBus.EventPublisher;
import com.shop.core.logger.Log;
import com.shop.account.dto.RegistrationRequest;
import com.shop.account.dto.FacebookRegistrationRequest;
import lombok.AllArgsConstructor;

@Log
@AllArgsConstructor
@ApplicationService
public class AccountRegistrationFacadeImpl implements AccountRegistrationFacade {

    private final AccountFactory accountFactory;
    private final AccountRepository accountRepository;
    private final FacebookService facebookService;
    private final AccountMapper accountMapper;
    private final EventPublisher eventPublisher;

    @Override
    public AccountResponse register(RegistrationRequest request) {
        Account account = accountFactory.createForRegistration(request);
        accountRepository.save(account);
        eventPublisher.publishEvent(RegistrationEvent.createFromAccount(account));
        return accountMapper.mapToAccountResponse(account);
    }

    @Override
    public AccountResponse register(FacebookRegistrationRequest request) {
        FacebookUser facebookUser = facebookService.getFacebookUser(request.getFacebookAccessToken());
        Account account = accountFactory.createFromFacebookUser(facebookUser);
        accountRepository.save(account);
        eventPublisher.publishEvent(RegistrationEvent.createFromAccountAndFacebookUser(account, facebookUser));
        return accountMapper.mapToAccountResponse(account);
    }

}
