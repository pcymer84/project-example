package com.shop.account.facade;

import com.shop.account.dto.FacebookAuthenticateRequest;
import com.shop.account.service.AuthenticationService;
import com.shop.account.dto.AuthenticateRequest;
import com.shop.core.annotations.ApplicationService;
import com.shop.core.logger.Log;
import lombok.AllArgsConstructor;

@Log
@AllArgsConstructor
@ApplicationService
public class AccountAuthFacadeImpl implements AccountAuthFacade {

    private final AuthenticationService authenticationService;

    @Override
    public String authenticate(AuthenticateRequest request) {
        return authenticationService.authenticateCredentials(request.getLogin(), request.getPassword());
    }

    @Override
    public String authenticate(FacebookAuthenticateRequest request) {
        return authenticationService.authenticateFacebookToken(request.getFacebookAccessToken());
    }

}
