package com.shop.account.facade;

import com.shop.account.model.Account;
import com.shop.account.model.Role;
import com.shop.account.repository.AccountRepository;
import com.shop.core.annotations.ApplicationService;
import com.shop.core.logger.Log;
import lombok.AllArgsConstructor;
import java.util.UUID;

@Log
@AllArgsConstructor
@ApplicationService
public class AccountRoleFacadeImpl implements AccountRoleFacade {

    private final AccountRepository accountRepository;

    @Override
    public void assigneeRole(UUID accountId, Role role) {
        Account account = accountRepository.getById(accountId);
        account.assignRole(role);
        accountRepository.save(account);
    }

    @Override
    public void removeRole(UUID accountId, Role role) {
        Account account = accountRepository.getById(accountId);
        account.removeRole(role);
        accountRepository.save(account);
    }

}
