package com.shop.account.facade;

import com.shop.account.dto.ResetPasswordRequest;
import com.shop.account.model.Account;
import com.shop.account.policy.EmailPolicy;
import com.shop.account.policy.PasswordPolicy;
import com.shop.account.repository.AccountRepository;
import com.shop.account.service.AuthenticationService;
import com.shop.core.annotations.ApplicationService;
import com.shop.core.logger.Log;
import com.shop.account.dto.AcceptPasswordRequest;
import com.shop.account.dto.ChangeEmailRequest;
import com.shop.account.dto.ChangePasswordRequest;
import com.shop.account.service.AccountEmailSendingService;
import lombok.AllArgsConstructor;
import java.util.UUID;

@Log
@AllArgsConstructor
@ApplicationService
public class AccountFacadeImpl implements AccountFacade {

    private final AuthenticationService authenticationService;
    private final AccountEmailSendingService userEmailSendingService;
    private final AccountRepository accountRepository;
    private final EmailPolicy emailPolicy;
    private final PasswordPolicy passwordPolicy;

    @Override
    public void changeEmail(UUID accountId, ChangeEmailRequest request) {
        Account account = accountRepository.getById(accountId);
        authenticationService.authenticateCredentials(account.getEmail(), request.getPassword());
        account.changeEmail(request.getEmail(), emailPolicy);
        accountRepository.save(account);
        userEmailSendingService.sendChangeEmailAddressEmail(account);
    }

    @Override
    public void changePassword(UUID userId, ChangePasswordRequest request) {
        Account account = accountRepository.getById(userId);
        authenticationService.authenticateCredentials(account.getEmail(), request.getOldPassword());
        account.changePassword(request.getNewPassword(), passwordPolicy);
        accountRepository.save(account);
    }

    @Override
    public void resetPassword(ResetPasswordRequest request) {
        Account account = accountRepository.getByEmail(request.getEmail());
        account.generateResetPasswordCode();
        userEmailSendingService.sendResetPasswordEmail(account);
        accountRepository.save(account);
    }

    @Override
    public void acceptPassword(AcceptPasswordRequest request) {
        Account account = accountRepository.getByResetPasswordCode(request.getCode());
        account.changePassword(request.getPassword(), passwordPolicy);
        accountRepository.save(account);
    }

}
