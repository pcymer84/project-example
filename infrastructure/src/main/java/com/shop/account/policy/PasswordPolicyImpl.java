package com.shop.account.policy;

import com.shop.core.annotations.Policy;
import com.shop.core.validator.impl.PasswordValidation;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;

@Policy
@AllArgsConstructor
public class PasswordPolicyImpl implements PasswordPolicy {

    private static final PasswordValidation validation = new PasswordValidation();
    private final PasswordEncoder passwordEncoder;

    @Override
    public void checkPasswordStrength(String rawPassword) {
        validation.checkPassword(rawPassword);
    }

    @Override
    public String encodePassword(String rawPassword) {
        return passwordEncoder.encode(rawPassword);
    }

}
