package com.shop.account.policy;

import com.shop.account.ex.UserAlreadyUsingEmailException;
import com.shop.account.ex.EmailAlreadyUsedException;
import com.shop.account.model.Account;
import com.shop.account.repository.AccountRepository;
import com.shop.core.annotations.Policy;
import lombok.AllArgsConstructor;
import java.util.Optional;
import java.util.UUID;

@Policy
@AllArgsConstructor
public class EmailPolicyImpl implements EmailPolicy {

    private final AccountRepository accountRepository;

    @Override
    public void checkEmailUsed(String email) {
        accountRepository.findByEmail(email)
                .ifPresent(account -> { throw new UserAlreadyUsingEmailException(String.format("account with email '%s' already exists", email)); });
    }

    @Override
    public void checkEmailUsed(UUID accountId, String email) {
        Optional<Account> foundAccount = accountRepository.findByEmail(email);
        if (foundAccount.isPresent()) {
            Account account = foundAccount.get();
            if (account.getEmail().equals(email) && account.getId().equals(accountId)) {
                throw new EmailAlreadyUsedException(String.format("account with id '%s' is already using email '%s'", accountId, email));
            }
            throw new UserAlreadyUsingEmailException(String.format("account with email '%s' already exists", email));
        }
    }

}
