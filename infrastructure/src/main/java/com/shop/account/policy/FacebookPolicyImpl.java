package com.shop.account.policy;

import com.shop.account.ex.FacebookIdAlreadyUsedException;
import com.shop.account.repository.AccountRepository;
import com.shop.core.annotations.Policy;
import lombok.AllArgsConstructor;

@Policy
@AllArgsConstructor
public class FacebookPolicyImpl implements FacebookPolicy {

    private final AccountRepository accountRepository;

    @Override
    public void checkFacebookIdUsed(String facebookId) {
        accountRepository.findByFacebookId(facebookId)
                .ifPresent(account -> { throw new FacebookIdAlreadyUsedException(String.format("account with facebookId '%s' already exists", facebookId)); });
    }

}
