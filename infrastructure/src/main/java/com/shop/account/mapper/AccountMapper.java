package com.shop.account.mapper;

import com.shop.account.dto.AccountResponse;
import com.shop.account.model.Account;

public interface AccountMapper {

    AccountResponse mapToAccountResponse(Account account);

}
