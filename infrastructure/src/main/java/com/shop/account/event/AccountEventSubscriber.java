package com.shop.account.event;

import com.google.common.eventbus.Subscribe;
import com.shop.account.service.AccountEmailSendingService;
import com.shop.core.eventBus.EventSubscriber;
import com.shop.core.logger.Log;
import lombok.AllArgsConstructor;

@Log
@EventSubscriber
@AllArgsConstructor
public class AccountEventSubscriber {

    private final AccountEmailSendingService accountEmailSendingService;

    @Subscribe
    public void handleRegistrationEvent(RegistrationEvent event) {
        accountEmailSendingService.sendRegistrationEmail(event.getAccount());
    }

}
