package com.shop.account.event;

import com.shop.account.model.Account;
import com.shop.account.model.FacebookUser;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegistrationEvent {

    private Account account;
    private String firstName;
    private String lastName;

    public static RegistrationEvent createFromAccount(Account account) {
        return new RegistrationEvent(account, null, null);
    }

    public static RegistrationEvent createFromAccountAndFacebookUser(Account account, FacebookUser facebookUser) {
        return new RegistrationEvent(account, facebookUser.getFirstName(), facebookUser.getLastName());
    }

}
