package com.shop.account.endpoint;

import com.shop.account.dto.AccountResponse;
import com.shop.account.facade.AccountRegistrationFacade;
import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.account.dto.FacebookRegistrationRequest;
import com.shop.account.dto.RegistrationRequest;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class RegistrationRestEndpoint {

    private final AccountRegistrationFacade accountRegistrationFacade;

    @PostMapping("/accounts/register")
    public AccountResponse register(@Valid @RequestBody RegistrationRequest request) {
        return accountRegistrationFacade.register(request);
    }

    @PostMapping("/accounts/facebookRegister")
    public AccountResponse facebookRegister(@Valid @RequestBody FacebookRegistrationRequest request) {
        return accountRegistrationFacade.register(request);
    }

}
