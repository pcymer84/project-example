package com.shop.account.endpoint;

import com.shop.account.dto.AcceptPasswordRequest;
import com.shop.account.dto.ResetPasswordRequest;
import com.shop.account.ex.AccountNotFoundException;
import com.shop.account.ex.EmailAlreadyUsedException;
import com.shop.account.ex.UserAlreadyUsingEmailException;
import com.shop.account.ex.WeakPasswordException;
import com.shop.account.facade.AccountFacade;
import com.shop.account.model.AccountDetails;
import com.shop.core.annotations.ApiV1;
import com.shop.account.ex.AuthenticationException;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.account.dto.ChangeEmailRequest;
import com.shop.account.dto.ChangePasswordRequest;
import com.shop.core.restHandler.RestApiException;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

/**
 * example of how we can handle exceptions when api client need more information what went wrong
 */
@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class AccountRestEndpoint {

    private final AccountFacade accountFacade;

    @ApiResponses(value = {
            @ApiResponse(code = 1001, message = "invalid password"),
            @ApiResponse(code = 1002, message = "email already used by different user"),
            @ApiResponse(code = 1003, message = "email already used by same user")}
    )
    @PutMapping("/accounts/changeEmail")
    public void changeEmail(@AuthenticationPrincipal AccountDetails account,
                                @Valid @RequestBody ChangeEmailRequest request) {
        try {
            accountFacade.changeEmail(account.getId(), request);
        } catch (AuthenticationException e) {
            throw new RestApiException(HttpStatus.BAD_REQUEST, 1001, "invalid password", e);
        } catch (EmailAlreadyUsedException e) {
            throw new RestApiException(HttpStatus.BAD_REQUEST, 1002, e.getMessage(), e);
        } catch (UserAlreadyUsingEmailException e) {
            throw new RestApiException(HttpStatus.BAD_REQUEST, 1003, e.getMessage(), e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 1001, message = "invalid password"),
            @ApiResponse(code = 1002, message = "weak password")}
    )
    @PutMapping("/accounts/changePassword")
    public void changePassword(@AuthenticationPrincipal AccountDetails account,
                                   @Valid @RequestBody ChangePasswordRequest request) {
        try {
            accountFacade.changePassword(account.getId(), request);
        } catch (AuthenticationException e) {
            throw new RestApiException(HttpStatus.BAD_REQUEST, 1001, "invalid password", e);
        } catch (WeakPasswordException e) {
            throw new RestApiException(HttpStatus.BAD_REQUEST, 1002, e.getMessage(), e);
        }
    }

    @ApiResponses(value = @ApiResponse(code = 1001, message = "account not found"))
    @PutMapping("/accounts/resetPassword")
    public void resetPassword(@Valid @RequestBody ResetPasswordRequest request) {
        try {
            accountFacade.resetPassword(request);
        } catch (AccountNotFoundException e) {
            throw new RestApiException(HttpStatus.NOT_FOUND, 1001, e.getMessage(), e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 1001, message = "account not found"),
            @ApiResponse(code = 1002, message = "weak password")}
    )
    @PutMapping("/accounts/acceptPassword")
    public void acceptPassword(@Valid @RequestBody AcceptPasswordRequest request) {
        try {
            accountFacade.acceptPassword(request);
        } catch (AccountNotFoundException e) {
            throw new RestApiException(HttpStatus.NOT_FOUND, 1001, e.getMessage(), e);
        } catch (WeakPasswordException e) {
            throw new RestApiException(HttpStatus.BAD_REQUEST, 1002, e.getMessage(), e);
        }
    }

}
