package com.shop.account.endpoint;

import com.shop.account.dto.FacebookAuthenticateRequest;
import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.account.dto.AuthenticateRequest;
import com.shop.account.facade.AccountAuthFacade;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class AuthenticationRestEndpoint {

    private final AccountAuthFacade userAuthFacade;

    @PostMapping("/accounts/authenticate")
    public String loginUsingCredentials(@Valid @RequestBody AuthenticateRequest request) {
        return userAuthFacade.authenticate(request);
    }

    @PostMapping("/accounts/facebookAuthenticate")
    public String loginUsingFacebook(@Valid @RequestBody FacebookAuthenticateRequest request) {
        return userAuthFacade.authenticate(request);
    }

}
