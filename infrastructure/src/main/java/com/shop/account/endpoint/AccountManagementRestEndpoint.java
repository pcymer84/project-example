package com.shop.account.endpoint;

import com.shop.account.annotations.PermissionToDeleteAccount;
import com.shop.account.annotations.PermissionToShowAccount;
import com.shop.account.dto.AccountResponse;
import com.shop.account.facade.AccountManagementFacade;
import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.UUID;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class AccountManagementRestEndpoint {

    private final AccountManagementFacade accountManagementFacade;

    @PermissionToShowAccount
    @GetMapping("/accounts/{accountId}")
    public AccountResponse getAccount(@PathVariable("accountId") UUID accountId) {
        return accountManagementFacade.getAccount(accountId);
    }

    @PermissionToShowAccount
    @GetMapping("/accounts")
    public List<AccountResponse> getAccounts() {
        return accountManagementFacade.getAccounts();
    }

    @PermissionToDeleteAccount
    @DeleteMapping("/accounts/{accountId}")
    public void deleteAccount(@PathVariable("accountId") UUID accountId) {
        accountManagementFacade.deleteAccount(accountId);
    }

}

