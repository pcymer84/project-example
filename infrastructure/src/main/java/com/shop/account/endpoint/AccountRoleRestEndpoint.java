package com.shop.account.endpoint;

import com.shop.account.annotations.PermissionToRoleManagement;
import com.shop.account.facade.AccountRoleFacade;
import com.shop.account.model.Role;
import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class AccountRoleRestEndpoint {

    private final AccountRoleFacade accountRoleFacade;

    @PermissionToRoleManagement
    @PutMapping("/accounts/{accountId}/roles/{role}")
    public void assignRole(@PathVariable("accountId") UUID accountId,
                           @PathVariable("role") Role role) {
        accountRoleFacade.assigneeRole(accountId, role);
    }

    @PermissionToRoleManagement
    @DeleteMapping("/accounts/{accountId}/roles/{role}")
    public void removeRole(@PathVariable("accountId") UUID accountId,
                           @PathVariable("role") Role role) {
        accountRoleFacade.removeRole(accountId, role);
    }

}
