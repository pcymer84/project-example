package com.shop.account.repository;

import com.shop.account.ex.AccountNotFoundException;
import com.shop.account.model.Account;
import com.shop.core.annotations.Repository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@AllArgsConstructor
@Repository
public class CustomAccountRepositoryImpl implements CustomAccountRepository {
    
    private final CrudAccountRepository crudAccountRepository;

    @Override
    public Account getById(UUID userId) {
        return crudAccountRepository.findById(userId).orElseThrow(() -> new AccountNotFoundException(String.format("account with id '%s' not found", userId)));
    }

    @Override
    public Account getByEmail(String email) {
        return crudAccountRepository.findByEmail(email).orElseThrow(() -> new AccountNotFoundException(String.format("account with email '%s' not found", email)));
    }

    @Override
    public Account getByFacebookId(String facebookId) {
        return crudAccountRepository.findByFacebookId(facebookId).orElseThrow(() -> new AccountNotFoundException(String.format("account with facebookId '%s' not found", facebookId)));
    }

    @Override
    public Account getByResetPasswordCode(UUID resetPasswordCode) {
        return crudAccountRepository.findByResetPasswordCode(resetPasswordCode).orElseThrow(() -> new AccountNotFoundException(String.format("code '%s' not found or already used", resetPasswordCode)));
    }

}
