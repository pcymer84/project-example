package com.shop.account.factory;

import com.shop.account.model.AccountDetails;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AuthenticationFactory {

    public Authentication createAuthentication(String login, String password) {
        return new UsernamePasswordAuthenticationToken(login, password, null);
    }

    public Authentication createAuthentication(AccountDetails account) {
        return new UsernamePasswordAuthenticationToken(account, null, account.getAuthorities());
    }

}
