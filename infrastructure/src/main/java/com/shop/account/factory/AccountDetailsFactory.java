package com.shop.account.factory;

import com.shop.account.model.Account;
import com.shop.account.model.AccountDetails;
import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.UUID;

@Component
@AllArgsConstructor
public class AccountDetailsFactory {

    private final AuthorityFactory authorityFactory;

    public AccountDetails createFromAccount(Account account) {
        return AccountDetails.builder()
                .id(account.getId())
                .username(account.getEmail())
                .password(account.getPassword())
                .authorities(authorityFactory.createAuthorities(account))
                .build();
    }

    public AccountDetails createFromJwtTokenClaims(Claims claims) {
        return AccountDetails.builder()
                .id(UUID.fromString(claims.getId()))
                .username(claims.getSubject())
                .authorities(authorityFactory.createAuthorities(claims))
                .build();
    }

}
