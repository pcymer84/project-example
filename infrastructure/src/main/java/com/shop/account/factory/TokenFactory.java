package com.shop.account.factory;

import com.shop.account.model.AccountDetails;
import com.shop.core.properties.JwtSecurityProperties;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.AllArgsConstructor;
import org.joda.time.DateTime;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class TokenFactory {

    private final JwtSecurityProperties jwtSecurityProperties;

    public String createFromAuthentication(Authentication authentication) {
        DateTime now = DateTime.now();
        AccountDetails account = (AccountDetails) authentication.getPrincipal();
        return Jwts.builder()
                .setId(account.getId().toString())
                .setSubject(account.getUsername())
                .setIssuedAt(now.toDate())
                .setExpiration(now.plusMinutes(jwtSecurityProperties.getValidMinutes()).toDate())
                .signWith(SignatureAlgorithm.HS512, jwtSecurityProperties.getSecret())
                .claim(tokenPermissionsKey(), account.getAuthorities())
                .compact();
    }

    public String tokenPermissionsKey() {
        return "permissions";
    }

}
