package com.shop.account.service;

import com.shop.account.factory.AccountDetailsFactory;
import com.shop.account.model.AccountDetails;
import com.shop.account.repository.AccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AccountDetailsService implements UserDetailsService {

    private final AccountRepository accountRepository;
    private final AccountDetailsFactory accountFactory;

    @Override
    public AccountDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return accountRepository.findByEmail(email)
                .map(accountFactory::createFromAccount)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("user with email '%s' not found", email)));
    }

}
