package com.shop.account.service;

import com.shop.account.ex.InvalidFacebookTokenException;
import com.shop.account.model.FacebookUser;
import com.shop.core.client.facebook.SpringFacebookClient;
import lombok.AllArgsConstructor;
import org.springframework.social.InvalidAuthorizationException;
import org.springframework.social.facebook.api.User;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class SpringFacebookService implements FacebookService {

    private final SpringFacebookClient springFacebookClient;

    @Override
    public FacebookUser getFacebookUser(String accessToken) {
        return mapToFacebookUserDto(requestFacebookUser(accessToken));
    }

    private User requestFacebookUser(String accessToken) {
        try {
            return springFacebookClient.getFacebookUser(accessToken);
        } catch (InvalidAuthorizationException e) {
            throw new InvalidFacebookTokenException("invalid facebook token", e);
        }
    }

    private FacebookUser mapToFacebookUserDto(User facebookUser) {
        return FacebookUser.builder()
                .facebookId(facebookUser.getId())
                .email(facebookUser.getEmail())
                .firstName(facebookUser.getFirstName())
                .lastName(facebookUser.getLastName())
                .build();
    }

}
