package com.shop.account.service;

import com.shop.account.ex.AccountNotFoundException;
import com.shop.account.ex.AuthenticationException;
import com.shop.account.ex.InvalidFacebookTokenException;
import com.shop.account.factory.AccountDetailsFactory;
import com.shop.account.factory.AuthenticationFactory;
import com.shop.account.factory.TokenFactory;
import com.shop.account.model.Account;
import com.shop.account.model.AccountDetails;
import com.shop.account.model.FacebookUser;
import com.shop.account.repository.AccountRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class SpringAuthenticationServiceImpl implements AuthenticationService {

    private final AuthenticationManager authenticationManager;
    private final AuthenticationFactory authenticationFactory;
    private final AccountDetailsFactory accountDetailsFactory;
    private final TokenFactory tokenFactory;
    private final FacebookService facebookService;
    private final AccountRepository accountRepository;

    @Override
    public String authenticateCredentials(String login, String password) {
        try {
            Authentication authentication = authenticationFactory.createAuthentication(login, password);
            return tokenFactory.createFromAuthentication(authenticationManager.authenticate(authentication));
        } catch (org.springframework.security.core.AuthenticationException e) {
            throw new AuthenticationException("invalid login or password", e);
        }
    }

    @Override
    public String authenticateFacebookToken(String facebookToken) {
        Account account = authenticateFacebookUser(facebookToken);
        AccountDetails accountDetails = accountDetailsFactory.createFromAccount(account);
        Authentication authentication = authenticationFactory.createAuthentication(accountDetails);
        return tokenFactory.createFromAuthentication(authentication);
    }

    private Account authenticateFacebookUser(String facebookToken) {
        try {
            FacebookUser facebookUser = facebookService.getFacebookUser(facebookToken);
            return accountRepository.getByFacebookId(facebookUser.getFacebookId());
        } catch (InvalidFacebookTokenException | AccountNotFoundException e) {
            throw new AuthenticationException("invalid facebook token or account not found", e);
        }
    }

}
