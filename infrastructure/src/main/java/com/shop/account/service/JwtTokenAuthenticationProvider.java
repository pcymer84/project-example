package com.shop.account.service;

import com.shop.account.ex.AuthenticationException;
import com.shop.account.factory.AccountDetailsFactory;
import com.shop.account.factory.AuthenticationFactory;
import com.shop.account.model.AccountDetails;
import com.shop.core.properties.JwtSecurityProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class JwtTokenAuthenticationProvider implements TokenAuthenticationProvider {

    private final JwtSecurityProperties jwtSecurityProperties;
    private final AccountDetailsFactory accountDetailsFactory;
    private final AuthenticationFactory authenticationFactory;

    public Authentication authenticateToken(String token) {
        Claims claims = extractClaimsFromJwtToken(token);
        AccountDetails account = accountDetailsFactory.createFromJwtTokenClaims(claims);
        return authenticationFactory.createAuthentication(account);
    }

    private Claims extractClaimsFromJwtToken(String jwtToken) {
        try {
            return Jwts.parser()
                    .setSigningKey(jwtSecurityProperties.getSecret())
                    .parseClaimsJws(jwtToken)
                    .getBody();
        } catch (JwtException e) {
            throw new AuthenticationException("invalid jwt token", e);
        }
    }

}
