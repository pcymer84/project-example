package com.shop.account.service;

import com.shop.account.model.Account;
import com.shop.core.annotations.InfrastructureService;
import com.shop.core.logger.Log;
import com.shop.core.util.ThreadSleepUtil;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import java.util.concurrent.TimeUnit;

/**
 * implemented as stub to show usage
 */
@Log
@AllArgsConstructor
@InfrastructureService
public class AccountEmailSendingServiceImpl implements AccountEmailSendingService {

    @Async
    @Override
    public void sendRegistrationEmail(Account account) {
        threadSleep();
    }

    @Async
    @Override
    public void sendChangeEmailAddressEmail(Account account) {
        threadSleep();
    }

    @Override
    public void sendResetPasswordEmail(Account account) {
        threadSleep();
    }

    private void threadSleep() {
        ThreadSleepUtil.sleep(2, TimeUnit.SECONDS);
    }

}
