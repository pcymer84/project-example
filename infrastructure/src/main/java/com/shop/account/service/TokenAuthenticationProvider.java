package com.shop.account.service;

import org.springframework.security.core.Authentication;

public interface TokenAuthenticationProvider {

    Authentication authenticateToken(String token);

}
