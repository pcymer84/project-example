package com.shop.rating.mapper;

import com.shop.rating.dto.ProductRatingResponse;
import com.shop.rating.dto.ProductReviewRequest;
import com.shop.rating.dto.ProductReviewResponse;
import com.shop.rating.model.ProductRating;
import com.shop.rating.model.ProductReview;
import com.shop.rating.model.ReviewDetails;
import java.util.UUID;

public interface ProductRatingMapper {

    ProductRatingResponse mapToProductRatingResponse(ProductRating productRating);

    ProductReview mapToProductReview(UUID reviewerId, UUID productId, ProductReviewRequest request);

    ProductReviewResponse mapToProductReviewResponse(ProductReview productReview);

    ReviewDetails mapToReviewDetails(ProductReviewRequest request);

}
