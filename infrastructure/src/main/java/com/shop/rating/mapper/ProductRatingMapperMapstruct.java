package com.shop.rating.mapper;

import com.shop.rating.dto.ProductReviewRequest;
import com.shop.rating.dto.ProductReviewResponse;
import com.shop.rating.model.ProductReview;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import java.util.UUID;

@Mapper
public interface ProductRatingMapperMapstruct extends ProductRatingMapper {

    @Mappings({
        @Mapping(source = "request.comment", target = "review.comment"),
        @Mapping(source = "request.score", target = "review.score")
    })
    ProductReview mapToProductReview(UUID reviewerId, UUID productId, ProductReviewRequest request);

    @Mappings({
            @Mapping(source = "review.comment", target = "comment"),
            @Mapping(source = "review.score", target = "score")
    })
    ProductReviewResponse mapToProductReviewResponse(ProductReview productReview);

}
