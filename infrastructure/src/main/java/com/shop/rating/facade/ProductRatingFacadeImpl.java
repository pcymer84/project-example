package com.shop.rating.facade;

import com.shop.core.annotations.ApplicationService;
import com.shop.core.logger.Log;
import com.shop.rating.dto.ProductRatingResponse;
import com.shop.rating.mapper.ProductRatingMapper;
import com.shop.rating.repository.ProductRatingRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@Log
@AllArgsConstructor
@ApplicationService
public class ProductRatingFacadeImpl implements ProductRatingFacade {

    private final ProductRatingRepository productRatingRepository;
    private final ProductRatingMapper productRatingMapper;

    @Override
    public ProductRatingResponse getProductRating(UUID productId) {
        return productRatingMapper.mapToProductRatingResponse(productRatingRepository.getByProductId(productId));
    }

}
