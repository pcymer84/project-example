package com.shop.rating.facade;

import com.shop.core.annotations.ApplicationService;
import com.shop.core.logger.Log;
import com.shop.rating.dto.ProductReviewRequest;
import com.shop.rating.dto.ProductReviewResponse;
import com.shop.rating.mapper.ProductRatingMapper;
import com.shop.rating.model.ProductRating;
import com.shop.rating.model.ProductReview;
import com.shop.rating.model.ReviewDetails;
import com.shop.rating.policy.ReviewPolicy;
import com.shop.rating.repository.ProductRatingRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Log
@AllArgsConstructor
@ApplicationService
public class ProductReviewFacadeImpl implements ProductReviewFacade {

    private final ProductRatingRepository productRatingRepository;
    private final ProductRatingMapper productRatingMapper;
    private final ReviewPolicy reviewPolicy;

    @Override
    public List<ProductReviewResponse> getProductReviews(UUID productId) {
        return productRatingRepository.getByProductId(productId)
                .getReviews()
                .stream()
                .map(productRatingMapper::mapToProductReviewResponse)
                .collect(Collectors.toList());
    }

    @Override
    public ProductReviewResponse createProductReview(UUID reviewerId, UUID productId, ProductReviewRequest request) {
        ProductReview productReview = productRatingMapper.mapToProductReview(reviewerId, productId, request);
        ProductRating productRating = productRatingRepository.getByProductId(productId);
        productRating.addReview(productReview, reviewPolicy);
        productRatingRepository.save(productRating);
        return productRatingMapper.mapToProductReviewResponse(productReview);
    }

    @Override
    public ProductReviewResponse updateProductReview(UUID reviewerId, UUID productId, UUID reviewId, ProductReviewRequest request) {
        ProductRating productRating = productRatingRepository.getByProductId(productId);
        ProductReview productReview = productRating.getReview(reviewId, reviewerId);
        productReview.updateReview(productRatingMapper.mapToReviewDetails(request));
        productRatingRepository.save(productRating);
        return productRatingMapper.mapToProductReviewResponse(productReview);
    }

    @Override
    public ProductReviewResponse censorshipProductReview(UUID productId, UUID reviewId) {
        ProductRating productRating = productRatingRepository.getByProductId(productId);
        ProductReview productReview = productRating.getReview(reviewId);
        ReviewDetails reviewDetails = productReview.getReview().toBuilder().comment("censorship").build();
        productReview.updateReview(reviewDetails);
        productRatingRepository.save(productRating);
        return productRatingMapper.mapToProductReviewResponse(productReview);
    }

}
