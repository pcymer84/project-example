package com.shop.rating.repository;

import com.shop.core.annotations.Repository;
import com.shop.rating.ex.ProductRatingNotFoundException;
import com.shop.rating.model.ProductRating;
import lombok.AllArgsConstructor;
import java.util.UUID;

@AllArgsConstructor
@Repository
public class CustomProductRatingRepositoryImpl implements CustomProductRatingRepository {

    private final CrudProductRatingRepository productRatingRepository;

    @Override
    public ProductRating getByProductId(UUID productId) {
        return productRatingRepository.findByProductId(productId).orElseThrow(() -> new ProductRatingNotFoundException(String.format("rating for product with id '%s' not found", productId)));
    }

}
