package com.shop.rating.model;

import com.shop.core.config.SpringProfile;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Component
@AllArgsConstructor
public class ProductRatingInitDataFactory {

    private final ProductReviewInitDataFactory productReviewInitDataFactory;

    public ProductRating createProductRating(ProductRatingInitData rating) {
        return ProductRating.builder()
                .productId(rating.getProductId())
                .sellerId(rating.getSellerId())
                .reviews(productReviewInitDataFactory.createProductReviews(rating.getProductReviews()))
                .build();
    }

}
