package com.shop.rating.model;

import com.shop.core.config.SpringProfile;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Component
public class ProductReviewInitDataFactory {

    public Set<ProductReview> createProductReviews(List<ProductReviewInitData> reviews) {
        return reviews.stream()
                .map(this::createProductReview)
                .collect(Collectors.toSet());
    }

    private ProductReview createProductReview(ProductReviewInitData review) {
        return ProductReview.builder()
                .id(review.getId())
                .reviewerId(review.getReviewerId())
                .review(createReviewDetails(review))
                .lastModified(review.getLastModified())
                .build();
    }

    private ReviewDetails createReviewDetails(ProductReviewInitData review) {
        return ReviewDetails.builder()
                .comment(review.getComment())
                .score(review.getScore())
                .build();
    }

}
