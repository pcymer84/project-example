package com.shop.rating.model;

import com.shop.user.model.UserInitData;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
import java.util.UUID;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ProductReviewInitData {

    REVIEW_1(
            UUID.fromString("27334bae-75a6-4fdd-9276-e19b08141675"),
            UserInitData.USER_2.getId(),
            "good keyboard",
            5,
            new Date()
    ),
    REVIEW_2(
            UUID.fromString("af475fe7-13fd-477c-a64c-5b28447223ed"),
            UserInitData.USER_1.getId(),
            "decent monitor",
            4,
            new Date()
    ),
    UNSAVED_REVIEW(
            UUID.fromString("0f8f0fdd-6d2e-4e2d-be24-4efe19edfe47"),
            UserInitData.USER_1.getId(),
            "bad headset",
            2,
            null
    );

    private UUID id;
    private UUID reviewerId;
    private String comment;
    private Integer score;
    private Date lastModified;

}
