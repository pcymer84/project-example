package com.shop.rating.model;

import com.shop.product.model.ProductInitData;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.testng.collections.Lists;
import java.util.List;
import java.util.UUID;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ProductRatingInitData {

    RATING_1(
            ProductInitData.PRODUCT_1.getId(),
            ProductInitData.PRODUCT_1.getSellerId(),
            Lists.newArrayList(ProductReviewInitData.REVIEW_1)
    ),
    RATING_2(
            ProductInitData.PRODUCT_2.getId(),
            ProductInitData.PRODUCT_2.getSellerId(),
            Lists.newArrayList(ProductReviewInitData.REVIEW_2)
    ),
    RATING_4(
            ProductInitData.PRODUCT_4.getId(),
            ProductInitData.PRODUCT_4.getSellerId(),
            Lists.newArrayList(ProductReviewInitData.UNSAVED_REVIEW)
    );

    private UUID productId;
    private UUID sellerId;
    private List<ProductReviewInitData> productReviews;

    public ProductReviewInitData getFirstReview() {
        return productReviews.get(0);
    }

}
