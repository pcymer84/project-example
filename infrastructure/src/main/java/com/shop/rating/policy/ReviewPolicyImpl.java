package com.shop.rating.policy;

import com.shop.core.annotations.Policy;
import com.shop.rating.ex.InvalidReviewerException;
import com.shop.rating.ex.ReviewerAlreadyReviewedProductException;
import com.shop.rating.ex.ReviewerNotFinishedPurchaseException;
import com.shop.purchase.model.Purchase;
import com.shop.purchase.repository.PurchaseRepository;
import com.shop.rating.repository.ProductRatingRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@Policy
@AllArgsConstructor
public class ReviewPolicyImpl implements ReviewPolicy {

    private final ProductRatingRepository productRatingRepository;
    private final PurchaseRepository purchaseRepository;

    @Override
    public void checkReviewerNotSeller(UUID reviewerId, UUID sellerId) throws InvalidReviewerException {
        if (reviewerId.equals(sellerId)) {
            throw new InvalidReviewerException("seller cannot review own product");
        }
    }

    @Override
    public void checkReviewerFinishedPurchase(UUID reviewerId, UUID productId) {
        purchaseRepository.findByBuyerId(reviewerId)
                .stream()
                .filter(Purchase::isFinished)
                .flatMap(purchase -> purchase.getPurchaseProducts().stream())
                .filter(purchaseProduct -> purchaseProduct.getProduct().getProductId().equals(productId))
                .findAny()
                .orElseThrow(() -> new ReviewerNotFinishedPurchaseException((String.format("no finished purchase found for product '%s' and reviewer '%s'", productId, reviewerId))));
    }

    @Override
    public void checkSingleReviewPerReviewer(UUID reviewerId, UUID productId) {
        productRatingRepository.getByProductId(productId).getReviews()
                .stream()
                .filter(review -> review.getReviewerId().equals(reviewerId))
                .findAny()
                .ifPresent(review -> { throw new ReviewerAlreadyReviewedProductException(String.format("product already reviewed by user '%s'", reviewerId)); });
    }

}
