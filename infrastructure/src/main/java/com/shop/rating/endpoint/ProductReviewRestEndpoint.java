package com.shop.rating.endpoint;

import com.shop.account.model.AccountDetails;
import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.rating.annotations.PermissionToCensorshipReview;
import com.shop.rating.dto.ProductReviewRequest;
import com.shop.rating.dto.ProductReviewResponse;
import com.shop.rating.facade.ProductReviewFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class ProductReviewRestEndpoint {

    private final ProductReviewFacade productReviewFacade;

    @GetMapping("/products/{productId}/reviews")
    public List<ProductReviewResponse> getProductReviews(@PathVariable("productId") UUID productId) {
        return productReviewFacade.getProductReviews(productId);
    }

    @PostMapping("/products/{productId}/reviews")
    public ProductReviewResponse createProductReview(@AuthenticationPrincipal AccountDetails account,
                                                     @PathVariable("productId") UUID productId,
                                                     @Valid @RequestBody ProductReviewRequest request) {
        return productReviewFacade.createProductReview(account.getId(), productId, request);
    }

    @PutMapping("/products/{productId}/reviews/{reviewId}")
    public ProductReviewResponse updateProductReview(@AuthenticationPrincipal AccountDetails account,
                                                     @PathVariable("productId") UUID productId,
                                                     @PathVariable("reviewId") UUID reviewId,
                                                     @Valid @RequestBody ProductReviewRequest request) {
        return productReviewFacade.updateProductReview(account.getId(), productId, reviewId, request);
    }

    @PermissionToCensorshipReview
    @PutMapping("/products/{productId}/reviews/{reviewId}/censorship")
    public ProductReviewResponse censorshipProductReview(@PathVariable("productId") UUID productId,
                                                         @PathVariable("reviewId") UUID reviewId) {
        return productReviewFacade.censorshipProductReview(productId, reviewId);
    }

}
