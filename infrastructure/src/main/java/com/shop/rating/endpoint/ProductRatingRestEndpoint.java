package com.shop.rating.endpoint;

import com.shop.core.annotations.ApiV1;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.rating.dto.ProductRatingResponse;
import com.shop.rating.facade.ProductRatingFacade;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;

@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class ProductRatingRestEndpoint {

    private final ProductRatingFacade productRatingFacade;

    @GetMapping("/products/{productId}/rating")
    public ProductRatingResponse getProductReviews(@PathVariable("productId") UUID productId) {
        return productRatingFacade.getProductRating(productId);
    }

}
