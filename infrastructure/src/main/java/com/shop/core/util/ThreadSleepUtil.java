package com.shop.core.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import java.util.concurrent.TimeUnit;

@UtilityClass
public class ThreadSleepUtil {

    @SneakyThrows
    public void sleep(long time, TimeUnit unit) {
        Thread.sleep(unit.toMillis(time));
    }

}
