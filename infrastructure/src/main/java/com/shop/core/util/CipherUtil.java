package com.shop.core.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@UtilityClass
public class CipherUtil {

    @SneakyThrows
    public static String encrypt(String plainText, String key) {
        Cipher cipher = createCipher();
        cipher.init(Cipher.ENCRYPT_MODE, createSecretKeySpec(key));
        byte[] bytes = plainText.getBytes(StandardCharsets.UTF_8);
        return Base64.getUrlEncoder().encodeToString(cipher.doFinal(bytes));
    }

    @SneakyThrows
    public static String decrypt(String cipherText, String key) {
        Cipher cipher = createCipher();
        cipher.init(Cipher.DECRYPT_MODE, createSecretKeySpec(key));
        byte[] bytes = Base64.getUrlDecoder().decode(cipherText);
        return new String(cipher.doFinal(bytes), StandardCharsets.UTF_8);
    }

    @SneakyThrows
    private static Cipher createCipher() {
        return Cipher.getInstance("AES/ECB/PKCS5Padding");
    }

    private static SecretKeySpec createSecretKeySpec(String key) {
        return new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
    }

}
