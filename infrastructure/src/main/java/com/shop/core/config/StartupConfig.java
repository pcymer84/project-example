package com.shop.core.config;

import com.google.common.eventbus.EventBus;
import com.shop.core.eventBus.EventSubscriber;
import com.shop.core.initData.DataInitializer;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

@Configuration
@AllArgsConstructor
public class StartupConfig {

    private final ApplicationContext applicationContext;
    private final DataInitializer dataInitializer;
    private final EventBus eventBus;

    @EventListener(ContextRefreshedEvent.class)
    public void setupEventSubscribers() {
        applicationContext.getBeansWithAnnotation(EventSubscriber.class).values().forEach(eventBus::register);
    }

    @Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
    @EventListener(ContextRefreshedEvent.class)
    public void setupTestEntities() {
        dataInitializer.initializeData();
    }

}
