package com.shop.core.config;

import com.shop.core.client.http.HttpClient;
import com.shop.core.client.http.impl.SpringRestTemplateHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class HttpClientConfig {

    @Bean
    public RestTemplate defaultRestTemplate(RestTemplateBuilder restTemplateBuilder,
                                            @Value("${httpClient.timeoutMillis}") Integer timeout) {
        return restTemplateBuilder
                .setConnectTimeout(timeout)
                .setReadTimeout(timeout)
                .build();
    }

    @Bean
    public HttpClient httpClient(RestTemplate defaultRestTemplate) {
        return new SpringRestTemplateHttpClient(defaultRestTemplate);
    }

}
