package com.shop.core.config;

public class SpringProfile {

    public static final String DEV = "dev";
    public static final String TEST = "test";
    public static final String PROD = "prod";

    public static class Include {

        public static final String INIT = "init";

    }

}
