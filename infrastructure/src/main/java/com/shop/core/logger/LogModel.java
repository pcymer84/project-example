package com.shop.core.logger;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import java.lang.reflect.Method;

@Getter
@Setter
@Builder
public class LogModel {

    private LogLevel logLevel;
    private String className;
    private String methodName;
    private Class methodReturnType;
    private boolean logTime;
    private Object[] args;
    private Object result;
    private long inTime;
    private long outTime;

    public static LogModel createFromProceedingJoinPoint(ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Log logExecution = findLogAnnotation(signature);
        return LogModel.builder()
                .logLevel(logExecution.level())
                .logTime(logExecution.logTime())
                .args(joinPoint.getArgs())
                .className(joinPoint.getTarget().getClass().getName())
                .methodName(method.getName())
                .methodReturnType(method.getReturnType())
                .build();
    }

    public static Log findLogAnnotation(MethodSignature signature) {
        Log logAnnotation = (Log) signature.getDeclaringType().getAnnotation(Log.class);
        return logAnnotation == null ? signature.getMethod().getAnnotation(Log.class) : logAnnotation;
    }

    public long calculateTime() {
        return outTime - inTime;
    }

}
