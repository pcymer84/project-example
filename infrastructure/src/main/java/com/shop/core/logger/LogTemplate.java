package com.shop.core.logger;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;

@Component
public class LogTemplate {

    private static final String TEMPLATE = "%s %s %s %s";
    private static final String TEMPLATE_WITH_TIMER = TEMPLATE + " in %s ms";

    public String createLogInMessage(LogModel logModel) {
        return String.format(TEMPLATE,
                logModel.getClassName(),
                logModel.getMethodName(),
                ">>",
                Lists.newArrayList(logModel.getArgs())
        );
    }

    public String createLogOutMessage(LogModel logModel) {
       return String.format(outFormat(logModel),
               logModel.getClassName(),
               logModel.getMethodName(),
               "<<",
               isVoid(logModel.getMethodReturnType()) ? "void" : logModel.getResult(),
               logModel.calculateTime());
    }

    private String outFormat(LogModel logModel) {
        return logModel.isLogTime() ? TEMPLATE_WITH_TIMER : TEMPLATE;
    }

    private boolean isVoid(Class clazz) {
        return clazz == void.class || clazz == Void.class;
    }

}
