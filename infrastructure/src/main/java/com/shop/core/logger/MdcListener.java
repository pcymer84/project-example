package com.shop.core.logger;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import java.util.UUID;

@Component
public class MdcListener implements ServletRequestListener {

    private static final String LOG_REQ_ID_KEY = "requestId";

    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
        MDC.remove(LOG_REQ_ID_KEY);
    }

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        MDC.put(LOG_REQ_ID_KEY, UUID.randomUUID().toString().replace("-", ""));
    }

    public static String getLogReqIdKey() {
        return MDC.get(LOG_REQ_ID_KEY);
    }

}
