package com.shop.core.client.facebook;

import org.springframework.social.InvalidAuthorizationException;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Component;

@Component
public class SpringFacebookClient {

    private static final String[] FIELDS = { "id", "email", "first_name", "last_name"};

    public User getFacebookUser(String accessToken) throws InvalidAuthorizationException{
        return new FacebookTemplate(accessToken).fetchObject("me", User.class, FIELDS);
    }

}
