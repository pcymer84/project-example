package com.shop.core.client.http.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import lombok.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

/**
 * right now application is only highly dependable on spring framework
 * we are using spring httpMethod and httpHeaders instead of creating our own classes
 */
@Getter
@Value
@Builder
@ToString(exclude = "body")
public class HttpRequest {

    @NonNull
    private String address;
    @NonNull
    private HttpMethod method;
    private HttpHeaders headers;
    private Object body;

}
