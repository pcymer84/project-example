package com.shop.core.client.http.ex;

public class HttpClientException extends RuntimeException {

    public HttpClientException(String message) {
        super(message);
    }

    public HttpClientException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
