package com.shop.core.client.http;

import com.shop.core.client.http.ex.HttpClientException;
import com.shop.core.client.http.model.HttpRequest;
import com.shop.core.logger.Log;
import org.springframework.core.ParameterizedTypeReference;

public interface HttpClient {

    @Log(logTime = true)
    <T> T sendRequest(HttpRequest httpRequest, ParameterizedTypeReference<T> responseType) throws HttpClientException;

}
