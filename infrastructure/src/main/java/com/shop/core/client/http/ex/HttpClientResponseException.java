package com.shop.core.client.http.ex;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class HttpClientResponseException extends HttpClientException {

    private HttpStatus httpStatus;

    public HttpClientResponseException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public HttpClientResponseException(String message, Throwable throwable, HttpStatus httpStatus) {
        super(message, throwable);
        this.httpStatus = httpStatus;
    }

}
