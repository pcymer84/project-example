package com.shop.core.client.http.impl;

import com.shop.core.client.http.HttpClient;
import com.shop.core.client.http.ex.HttpClientException;
import com.shop.core.client.http.ex.HttpClientResponseException;
import com.shop.core.client.http.model.HttpRequest;
import com.shop.core.logger.Log;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class SpringRestTemplateHttpClient implements HttpClient {

    private RestTemplate restTemplate;

    public SpringRestTemplateHttpClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Log
    @Override
    public <T> T sendRequest(HttpRequest httpRequest, ParameterizedTypeReference<T> responseType) {
        ResponseEntity<T> response = processSendingRequest(httpRequest, responseType);
        validateResponse(response);
        return response.getBody();
    }

    private <T> ResponseEntity<T> processSendingRequest(HttpRequest httpRequest, ParameterizedTypeReference<T> responseType) {
        try {
            return restTemplate.exchange(
                    httpRequest.getAddress(),
                    httpRequest.getMethod(),
                    createHttpEntity(httpRequest),
                    responseType);
        } catch (HttpStatusCodeException e) {
            HttpStatus httpStatus = e.getStatusCode();
            throw new HttpClientResponseException(String.format("sending request '%s' result with response with status code '%s'", httpRequest, httpStatus), e, httpStatus);
        } catch (RestClientException e) {
            throw new HttpClientException(String.format("error while sending request '%s'", httpRequest), e);
        }
    }

    private HttpEntity createHttpEntity(HttpRequest httpRequest) {
        return new HttpEntity<>(httpRequest.getBody(), httpRequest.getHeaders());
    }

    private <T> void validateResponse(ResponseEntity<T> response) {
        HttpStatus httpStatus = response.getStatusCode();
        if (isUnsuccessfulStatusCode(httpStatus)) {
            throw new HttpClientResponseException(String.format("response contains unsuccessful status code '%s'", httpStatus), httpStatus);
        }
    }

    private boolean isUnsuccessfulStatusCode(HttpStatus httpStatus) {
        return !httpStatus.series().equals(HttpStatus.Series.SUCCESSFUL);
    }

}
