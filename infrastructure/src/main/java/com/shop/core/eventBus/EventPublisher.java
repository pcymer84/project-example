package com.shop.core.eventBus;

public interface EventPublisher {

    void publishEvent(Object event);

}
