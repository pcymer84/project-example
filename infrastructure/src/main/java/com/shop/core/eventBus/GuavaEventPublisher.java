package com.shop.core.eventBus;

import com.google.common.eventbus.EventBus;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class GuavaEventPublisher implements EventPublisher {

    private final EventBus eventBus;

    @Override
    public void publishEvent(Object event) {
        eventBus.post(event);
    }

}
