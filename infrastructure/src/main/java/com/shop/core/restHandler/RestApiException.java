package com.shop.core.restHandler;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public class RestApiException extends RuntimeException {

    private HttpStatus httpStatus;
    private Integer code;

    public RestApiException(HttpStatus status, Integer code, String message) {
        super(message);
        this.httpStatus = status;
        this.code = code;
    }

    public RestApiException(HttpStatus status, Integer code, String message, Throwable throwable) {
        super(message, throwable);
        this.httpStatus = status;
        this.code = code;
    }

}
