package com.shop.core.restHandler;

import com.shop.core.ex.ClientSideException;
import com.shop.core.ex.NotFoundException;
import com.shop.core.logger.MdcListener;
import com.shop.core.ex.UnprocessableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class RestExceptionsHandler {

    @ResponseBody
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<StandardResponseError> handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.UNSUPPORTED_MEDIA_TYPE, e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler({MethodArgumentNotValidException.class, BindException.class})
    public ResponseEntity<StandardResponseError> handleMethodArgumentNotValidExceptionAndBindException(MethodArgumentNotValidException e) {
        String message = getMessageFromBindingResult(e.getBindingResult());
        log.debug(message, e);
        return createHttpErrorResponse(HttpStatus.BAD_REQUEST, message);
    }

    @ResponseBody
    @ExceptionHandler({
            MissingServletRequestPartException.class,
            HttpRequestMethodNotSupportedException.class,
            MethodArgumentTypeMismatchException.class,
            ServletRequestBindingException.class,
            HttpMessageNotReadableException.class})
    public ResponseEntity<StandardResponseError> handleBadRequestException(Exception e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<StandardResponseError> handleNotFoundException(NotFoundException e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<StandardResponseError> handleAuthorizeException(RuntimeException e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.FORBIDDEN, e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(UnprocessableException.class)
    public ResponseEntity<StandardResponseError> handleNotFoundException(UnprocessableException e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(RestApiException.class)
    public ResponseEntity<StandardResponseError> handleRestApiException(RestApiException e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(e.getHttpStatus(), e.getMessage(), e.getCode());
    }

    /**
     * all client side exceptions that are not handled individually are treated as client side bad request
     */
    @ResponseBody
    @ExceptionHandler(ClientSideException.class)
    public ResponseEntity<StandardResponseError> handleClientSideException(ClientSideException e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    /**
     * all exceptions that are not handled individually are treated as internal/server side exceptions
     */
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public ResponseEntity<StandardResponseError> handleInternalException(Exception e) {
        log.error(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "internal exception");
    }

    private ResponseEntity<StandardResponseError> createHttpErrorResponse(HttpStatus httpStatus, String message, Integer code) {
        return new ResponseEntity<>(new StandardResponseError(code, message, MdcListener.getLogReqIdKey()), httpStatus);
    }

    private ResponseEntity<StandardResponseError> createHttpErrorResponse(HttpStatus httpStatus, String message) {
        return createHttpErrorResponse(httpStatus, message, httpStatus.value());
    }

    private String getMessageFromBindingResult(BindingResult bindingResult) {
        return "Request validation errors: " +
                bindingResult.getAllErrors().stream()
                        .map(this::createMessage)
                        .collect(Collectors.joining("; "));
    }

    private String createMessage(ObjectError error) {
        if (error instanceof FieldError) {
            FieldError fieldError = (FieldError) error;
            return "[" + fieldError.getField() + "] " + fieldError.getDefaultMessage();
        }
        return error.getDefaultMessage();
    }

}
