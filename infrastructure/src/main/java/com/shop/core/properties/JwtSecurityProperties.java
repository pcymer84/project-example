package com.shop.core.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "security.jwt")
public class JwtSecurityProperties {

    private List<String> unauthenticatedEndpoints;
    private String header;
    private String prefix;
    private Integer validMinutes;
    private String secret;

}
