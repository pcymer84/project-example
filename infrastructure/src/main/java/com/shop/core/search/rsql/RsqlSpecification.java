package com.shop.core.search.rsql;

import com.shop.core.search.rsql.ex.RsqlOperatorNotSupportedException;
import cz.jirutka.rsql.parser.ast.ComparisonOperator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.apache.commons.beanutils.ConvertUtils;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
public class RsqlSpecification<T> implements Specification<T> {

    private static final char REST_WILDCARD = '*';
    private static final char SQL_WILDCARD = '%';

    private String selector;
    private ComparisonOperator operator;
    private List<String> arguments;

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Path<String> attributePath = root.get(selector);
        List<Object> arguments = castArguments(attributePath.getJavaType());
        switch (RsqlOperator.findRsqlSearchOperator(operator)) {
            case EQUAL:
                return createEquals(attributePath, builder, arguments.get(0));
            case NOT_EQUAL:
                return createNotEquals(attributePath, builder, arguments.get(0));
            case GREATER:
                return createGreater(attributePath, builder, arguments.get(0));
            case GREATER_OR_EQUAL:
                return createGreaterOrEqual(attributePath, builder, arguments.get(0));
            case LESS:
                return createLess(attributePath, builder, arguments.get(0));
            case LESS_OR_EQUAL:
                return createLessOrEqual(attributePath, builder, arguments.get(0));
            case IN:
                return createIn(attributePath, arguments);
            case NOT_IN:
                return createNotIn(attributePath, builder, arguments);
            case BETWEEN:
                return createBetween(attributePath, builder, arguments.get(0), arguments.get(1));
            default:
                throw new RsqlOperatorNotSupportedException(String.format("unsupported operator symbol: '%s'", operator.getSymbol()));
        }
    }

    private List<Object> castArguments(Class<? extends Object> classType) {
        return this.arguments.stream()
                .map(argument -> ConvertUtils.convert(argument, classType))
                .collect(Collectors.toList());
    }

    private String castToString(Object argument) {
        return argument.toString().replace(REST_WILDCARD, SQL_WILDCARD);
    }

    private Predicate createEquals(Path<String> objectPath, CriteriaBuilder builder, Object argument) {
        if (argument instanceof String) {
            return builder.like(objectPath, castToString(argument));
        } else if (argument == null) {
            return builder.isNull(objectPath);
        } else {
            return builder.equal(objectPath, argument);
        }
    }

    private Predicate createNotEquals(Path<String> objectPath, CriteriaBuilder builder, Object argument) {
        if (argument instanceof String) {
            return builder.notLike(objectPath, castToString(argument));
        } else if (argument == null) {
            return builder.isNotNull(objectPath);
        } else {
            return builder.notEqual(objectPath, argument);
        }
    }

    private Predicate createGreater(Path<String> objectPath, CriteriaBuilder builder, Object argument) {
        return builder.greaterThan(objectPath, argument.toString());
    }

    private Predicate createGreaterOrEqual(Path<String> objectPath, CriteriaBuilder builder, Object argument) {
        return builder.greaterThanOrEqualTo(objectPath, argument.toString());
    }

    private Predicate createLess(Path<String> objectPath, CriteriaBuilder builder, Object argument) {
        return builder.lessThan(objectPath, argument.toString());
    }

    private Predicate createLessOrEqual(Path<String> objectPath, CriteriaBuilder builder, Object argument) {
        return builder.lessThanOrEqualTo(objectPath, argument.toString());
    }

    private Predicate createIn(Path<String> objectPath, List<Object> arguments) {
        return objectPath.in(arguments);
    }

    private Predicate createNotIn(Path<String> objectPath, CriteriaBuilder builder, List<Object> arguments) {
        return builder.not(objectPath.in(arguments));
    }

    private Predicate createBetween(Path objectPath, CriteriaBuilder builder, Object from, Object to) {
        if (from instanceof Double && to instanceof Double) {
            return builder.between(objectPath, (Double) from, (Double) to);
        } else if (from instanceof Integer && to instanceof Integer) {
            return builder.between(objectPath, (Integer) from, (Integer) to);
        }
        throw new RsqlOperatorNotSupportedException(String.format("between operator not supported for types : from = '%s', to = '%s'", from.getClass().getSimpleName(), to.getClass().getSimpleName()));
    }

}
