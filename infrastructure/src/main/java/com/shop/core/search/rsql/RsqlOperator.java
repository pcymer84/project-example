package com.shop.core.search.rsql;

import com.google.common.collect.Lists;
import com.shop.core.search.rsql.ex.RsqlOperatorNotFoundException;
import cz.jirutka.rsql.parser.ast.ComparisonOperator;
import cz.jirutka.rsql.parser.ast.RSQLOperators;
import lombok.Getter;
import java.util.List;
import java.util.stream.Stream;

@Getter
public enum RsqlOperator {

    EQUAL(RSQLOperators.EQUAL.getSymbol()),
    NOT_EQUAL(RSQLOperators.NOT_EQUAL.getSymbol()),
    GREATER(RSQLOperators.GREATER_THAN.getSymbol()),
    GREATER_OR_EQUAL(RSQLOperators.GREATER_THAN_OR_EQUAL.getSymbol()),
    LESS(RSQLOperators.LESS_THAN.getSymbol()),
    LESS_OR_EQUAL(RSQLOperators.LESS_THAN_OR_EQUAL.getSymbol()),
    IN(RSQLOperators.IN.getSymbol()),
    NOT_IN(RSQLOperators.NOT_IN.getSymbol()),
    BETWEEN("=btw=");

    private List<String> symbols;

    RsqlOperator(String... symbols) {
        this.symbols = Lists.newArrayList(symbols);
    }

    public String[] getSymbolsAsArray() {
        return symbols.toArray(new String[symbols.size()]);
    }

    public static RsqlOperator findRsqlSearchOperator(ComparisonOperator operator) {
        return Stream.of(values())
                .filter(rsqlOperator -> matchSymbols(rsqlOperator, operator))
                .findFirst()
                .orElseThrow(() -> new RsqlOperatorNotFoundException(String.format("rsql operator '%s' not found", operator)));
    }

    private static boolean matchSymbols(RsqlOperator rsqlOperator, ComparisonOperator comparisonOperator) {
        return matchAtLeastOne(rsqlOperator.getSymbols(), Lists.newArrayList(comparisonOperator.getSymbols()));
    }

    private static boolean matchAtLeastOne(List<String> matcher, List<String> match) {
        return matcher.stream().anyMatch(match::contains);
    }

}
