package com.shop.core.search.rsql.ex;

import com.shop.core.ex.ClientSideException;

public class RsqlOperatorNotFoundException extends ClientSideException {

    public RsqlOperatorNotFoundException(String message) {
        super(message);
    }

}
