package com.shop.core.search.rsql;

import cz.jirutka.rsql.parser.ast.AndNode;
import cz.jirutka.rsql.parser.ast.ComparisonNode;
import cz.jirutka.rsql.parser.ast.OrNode;
import cz.jirutka.rsql.parser.ast.RSQLVisitor;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class RsqlSpecificationVisitor<T> implements RSQLVisitor<Specification<T>, Void> {

    private final RsqlSpecificationFactory<T> factory;

    @Override
    public Specification<T> visit(AndNode node, Void param) {
        return factory.createSpecification(node);
    }

    @Override
    public Specification<T> visit(OrNode node, Void param) {
        return factory.createSpecification(node);
    }

    @Override
    public Specification<T> visit(ComparisonNode node, Void params) {
        return factory.createSpecification(node);
    }

}
