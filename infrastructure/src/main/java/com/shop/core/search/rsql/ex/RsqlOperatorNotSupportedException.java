package com.shop.core.search.rsql.ex;

public class RsqlOperatorNotSupportedException extends RuntimeException {

    public RsqlOperatorNotSupportedException(String message) {
        super(message);
    }

}
