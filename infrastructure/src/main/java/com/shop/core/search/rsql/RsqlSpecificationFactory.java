package com.shop.core.search.rsql;

import com.shop.core.search.rsql.ex.RsqlOperatorNotSupportedException;
import cz.jirutka.rsql.parser.ast.LogicalNode;
import cz.jirutka.rsql.parser.ast.LogicalOperator;
import cz.jirutka.rsql.parser.ast.Node;
import cz.jirutka.rsql.parser.ast.ComparisonNode;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

@Component
public class RsqlSpecificationFactory<T> {

    public Specification<T> createSpecification(Node node) {
        if (node instanceof ComparisonNode) {
            return createSpecification((ComparisonNode) node);
        }
        if (node instanceof LogicalNode) {
            return createSpecification((LogicalNode) node);
        }
        throw new RsqlOperatorNotSupportedException(String.format("unsupported node class : '%s'", node.getClass().getSimpleName()));
    }

    public Specification<T> createSpecification(ComparisonNode node) {
        return Specification.where(new RsqlSpecification<>(node.getSelector(), node.getOperator(), node.getArguments()));
    }

    public Specification<T> createSpecification(LogicalNode logicalNode) {
        List<Specification<T>> specifications = createSpecifications(logicalNode);
        Specification<T> result = specifications.stream().findFirst().orElseThrow(() -> new RuntimeException("empty rsql logical node collection"));
        for (Specification<T> specification : specifications) {
            result = findLogicalFunction(logicalNode.getOperator()).apply(result, specification);
        }
        return result;
    }

    private List<Specification<T>> createSpecifications(LogicalNode logicalNode) {
        return logicalNode.getChildren()
                .stream()
                .map(this::createSpecification)
                .collect(Collectors.toList());
    }

    private BiFunction<Specification<T>, Specification<T>, Specification<T>> findLogicalFunction(LogicalOperator logicalOperator) {
        switch (logicalOperator) {
            case OR: return Specification::or;
            case AND: return Specification::and;
            default: throw new RsqlOperatorNotSupportedException(String.format("unsupported logical operator : '%s'", logicalOperator));
        }
    }

}
