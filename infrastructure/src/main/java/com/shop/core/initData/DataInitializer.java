package com.shop.core.initData;

import com.shop.core.config.SpringProfile;
import com.shop.product.model.ProductInitData;
import com.shop.purchase.model.PurchaseInitData;
import com.shop.rating.model.ProductRatingInitData;
import com.shop.user.model.UserInitData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Slf4j
@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Component
public class DataInitializer {

    @Autowired
    private UserInitDataHelper userHelper;

    @Autowired
    private AccountInitDataHelper accountHelper;

    @Autowired
    private ProductInitDataHelper productHelper;

    @Autowired
    private ProductRatingInitDataHelper productRatingHelper;

    @Autowired
    private PurchaseInitDataHelper purchaseHelper;

    public void initializeData() {
        log.info("starting initialize data");
        accountHelper.setupAccount(UserInitData.USER_1);
        accountHelper.setupAccount(UserInitData.USER_2);
        accountHelper.setupAccount(UserInitData.USER_3);
        userHelper.setupUser(UserInitData.USER_1);
        userHelper.setupUser(UserInitData.USER_2);
        userHelper.setupUser(UserInitData.USER_3);
        productHelper.setupProduct(ProductInitData.PRODUCT_1);
        productHelper.setupProduct(ProductInitData.PRODUCT_2);
        productHelper.setupProduct(ProductInitData.PRODUCT_3);
        productHelper.setupProduct(ProductInitData.PRODUCT_4);
        productHelper.setupProduct(ProductInitData.PRODUCT_5);
        productRatingHelper.setupProductRating(ProductRatingInitData.RATING_1);
        productRatingHelper.setupProductRating(ProductRatingInitData.RATING_2);
        purchaseHelper.setupPurchase(PurchaseInitData.PURCHASE_1);
        purchaseHelper.setupPurchase(PurchaseInitData.PURCHASE_2);
        purchaseHelper.setupPurchase(PurchaseInitData.PURCHASE_3);
        log.info("finished initialize data");
    }

}
