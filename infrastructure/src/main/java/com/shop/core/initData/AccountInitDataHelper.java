package com.shop.core.initData;

import com.shop.account.model.Account;
import com.shop.account.model.AccountInitDataFactory;
import com.shop.account.repository.AccountRepository;
import com.shop.core.config.SpringProfile;
import com.shop.user.model.UserInitData;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;
import java.util.UUID;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Transactional
@Component
@AllArgsConstructor
public class AccountInitDataHelper {

    private final AccountRepository accountRepository;
    private final AccountInitDataFactory testAccountFactory;

    public void setupAccount(UserInitData testUser) {
        accountRepository.save(testAccountFactory.createAccount(testUser));
    }

    public Account getAccount(UUID id) {
        Account account = accountRepository.getById(id);
        account.toString();
        return account;
    }

    public void deleteAccount(UUID id) {
        accountRepository.deleteById(id);
    }

}
