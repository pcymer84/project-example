package com.shop.core.initData;

import com.shop.core.config.SpringProfile;
import com.shop.user.model.User;
import com.shop.user.model.UserInitData;
import com.shop.user.model.UserInitDataFactory;
import com.shop.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;
import java.util.UUID;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Transactional
@Component
public class UserInitDataHelper {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserInitDataFactory testUserFactory;

    public void setupUser(UserInitData testUser) {
        userRepository.save(testUserFactory.createUser(testUser));
    }

    public User getUser(UUID userId) {
        User user = userRepository.getById(userId);
        user.toString();
        return user;
    }

    public void deleteUser(UUID id) {
        userRepository.deleteById(id);
    }

}
