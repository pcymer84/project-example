package com.shop.core.initData;

import com.shop.core.annotations.ApiV1;
import com.shop.core.config.SpringProfile;
import com.shop.core.logger.Log;
import com.shop.core.logger.LogLevel;
import com.shop.product.model.ProductInitData;
import com.shop.purchase.model.PurchaseInitData;
import com.shop.user.model.UserInitData;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Log(level = LogLevel.INFO, logTime = true)
@ApiV1
@RestController
@AllArgsConstructor
public class InitDataEndpoint {

    private final AccountInitDataHelper accountInitDataHelper;
    private final UserInitDataHelper userInitDataHelper;
    private final ProductInitDataHelper productInitDataHelper;
    private final PurchaseInitDataHelper purchaseInitDataHelper;

    @RequestMapping(path = "/init/accounts/{account}", method = RequestMethod.PUT)
    public void resetAccount(@PathVariable("account") UserInitData account) {
        accountInitDataHelper.setupAccount(account);
    }

    @RequestMapping(path = "/init/users/{user}", method = RequestMethod.PUT)
    public void resetUser(@PathVariable("user") UserInitData user) {
        userInitDataHelper.setupUser(user);
    }

    @RequestMapping(path = "/init/products/{product}", method = RequestMethod.PUT)
    public void resetProduct(@PathVariable("product") ProductInitData product) {
        productInitDataHelper.setupProduct(product);
    }

    @RequestMapping(path = "/init/purchases/{purchase}", method = RequestMethod.PUT)
    public void resetPurchase(@PathVariable("purchase") PurchaseInitData purchase) {
        purchaseInitDataHelper.setupPurchase(purchase);
    }

}
