package com.shop.core.initData;

import com.shop.core.config.SpringProfile;
import com.shop.purchase.model.Purchase;
import com.shop.purchase.model.PurchaseInitData;
import com.shop.purchase.model.PurchaseInitDataFactory;
import com.shop.purchase.repository.PurchaseRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;
import java.util.UUID;

@Slf4j
@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Transactional
@Component
@AllArgsConstructor
public class PurchaseInitDataHelper {

    private final PurchaseRepository purchaseRepository;
    private final PurchaseInitDataFactory testPurchaseFactory;

    public void setupPurchase(PurchaseInitData testPurchase) {
        purchaseRepository.save(testPurchaseFactory.createPurchase(testPurchase));
    }

    public Purchase getPurchase(UUID purchaseId) {
        Purchase purchase = purchaseRepository.getById(purchaseId);
        purchase.toString();
        return purchase;
    }

    public void deletePurchase(UUID purchaseId) {
        purchaseRepository.deleteById(purchaseId);
    }

}
