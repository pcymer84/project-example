package com.shop.core.initData;

import com.shop.core.config.SpringProfile;
import com.shop.product.model.Product;
import com.shop.product.model.ProductInitData;
import com.shop.product.model.ProductInitDataFactory;
import com.shop.product.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;
import java.util.UUID;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Transactional
@Component
@AllArgsConstructor
public class ProductInitDataHelper {

    private ProductInitDataFactory testProductFactory;
    private ProductRepository productRepository;

    public void setupProduct(ProductInitData testProduct) {
        productRepository.save(testProductFactory.createProduct(testProduct));
    }

    public Product getProduct(UUID productId) {
        Product product = productRepository.getById(productId);
        product.toString();
        return product;
    }

    public void deleteProduct(UUID productId) {
        productRepository.deleteById(productId);
    }

}
