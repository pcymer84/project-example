package com.shop.core.initData;

import com.shop.core.config.SpringProfile;
import com.shop.rating.model.ProductRating;
import com.shop.rating.model.ProductRatingInitData;
import com.shop.rating.model.ProductRatingInitDataFactory;
import com.shop.rating.repository.ProductRatingRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;
import java.util.UUID;

@Profile({SpringProfile.TEST, SpringProfile.Include.INIT})
@Transactional
@Component
@AllArgsConstructor
public class ProductRatingInitDataHelper {

    private ProductRatingInitDataFactory productRatingFactory;
    private ProductRatingRepository productRatingRepository;

    public void setupProductRating(ProductRatingInitData product) {
        productRatingRepository.save(productRatingFactory.createProductRating(product));
    }

    public ProductRating getProductRating(UUID productId) {
        ProductRating product = productRatingRepository.getByProductId(productId);
        product.toString();
        return product;
    }

}
