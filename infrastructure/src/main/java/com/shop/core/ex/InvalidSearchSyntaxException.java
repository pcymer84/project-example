package com.shop.core.ex;

public class InvalidSearchSyntaxException extends ClientSideException {

    public InvalidSearchSyntaxException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public InvalidSearchSyntaxException(Throwable throwable) {
        super(throwable);
    }

}
