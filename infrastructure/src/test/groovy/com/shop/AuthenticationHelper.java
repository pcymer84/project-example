package com.shop;

import com.shop.account.dto.AuthenticateRequest;
import com.shop.account.facade.AccountAuthFacade;
import com.shop.user.model.UserInitData;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;

@Transactional
@Component
@AllArgsConstructor
public class AuthenticationHelper {

    private final AccountAuthFacade accountAuthFacade;

    public String authenticate(String login, String password) {
        return "Bearer " + accountAuthFacade.authenticate(new AuthenticateRequest(login, password));
    }

    public String authenticate(UserInitData user) {
        return authenticate(user.getEmail(), user.getPassword());
    }

}
