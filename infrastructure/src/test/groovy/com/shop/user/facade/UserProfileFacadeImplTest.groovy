package com.shop.user.facade

import com.shop.user.dto.UserProfileRequest
import com.shop.user.dto.UserProfileResponse
import com.shop.user.mapper.UserProfileMapper
import com.shop.user.model.User
import com.shop.user.repository.UserRepository
import spock.lang.Specification

class UserProfileFacadeImplTest extends Specification {

    UserProfileFacadeImpl userFacade
    UserRepository userRepository
    UserProfileMapper userProfileMapper

    def setup() {
        userRepository = Mock()
        userProfileMapper = Mock()
        userFacade = new UserProfileFacadeImpl(
                userRepository,
                userProfileMapper
        )
    }

    def "get user profile"() {
        given:
        UUID userId = UUID.randomUUID()
        User user = Mock(User)
        when:
        def result = userFacade.getUserProfile(userId)
        then:
        result != null
        1 * userRepository.getById(userId) >> user
        1 * userProfileMapper.mapToUserProfileResponse(user) >> new UserProfileResponse()
    }

    def "update user profile"() {
        given:
        UUID userId = UUID.randomUUID()
        def request = Mock(UserProfileRequest)
        def user = Mock(User)
        when:
        def result = userFacade.updateUserProfile(userId, request)
        then:
        result != null
        1 * userRepository.getById(userId) >> user
        1 * userProfileMapper.updateUserProfile(request, user)
        1 * userRepository.save(user)
        1 * userProfileMapper.mapToUserProfileResponse(user) >> new UserProfileResponse()
    }

}