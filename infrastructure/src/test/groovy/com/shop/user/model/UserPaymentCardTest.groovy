package com.shop.user.model

import com.shop.core.validator.ex.ValidationException
import spock.lang.Specification

class UserPaymentCardTest extends Specification {

    def "create user payment card"() {
        given:
        def builder = UserPaymentCard.builder()
            .cardNumber('cardNumber')
            .firstName('firstName')
            .lastName('lastName')
            .createdMonth(1)
            .createdYear(2016)
            .expiredMonth(5)
            .expiredYear(2030)
        when:
        def userAddress = builder.build()
        then:
        userAddress.getId() != null
        userAddress.getCardNumber() == builder.cardNumber
        userAddress.getFirstName() == builder.firstName
        userAddress.getLastName() == builder.lastName
        userAddress.getCreatedMonth() == builder.createdMonth
        userAddress.getCreatedYear() == builder.createdYear
        userAddress.getExpiredMonth() == builder.expiredMonth
        userAddress.getExpiredYear() == builder.expiredYear
    }

    def "create user payment card when invalid data"() {
        when:
        UserPaymentCard.builder()
                .cardNumber()
                .firstName()
                .lastName()
                .createdMonth()
                .createdYear()
                .expiredMonth()
                .expiredYear()
                .build()
        then:
        thrown(ValidationException)
        where:
        cardNumber   | firstName   | lastName   | createdMonth | createdYear | expiredMonth | expiredYear
        null         | 'firstName' | 'lastName' | 3            | 2016        | 5            | 2030
        'cardNumber' | null        | 'lastName' | 3            | 2016        | 5            | 2030
        'cardNumber' | 'firstName' | null       | 3            | 2016        | 5            | 2030
        'cardNumber' | 'firstName' | 'lastName' | null         | 2016        | 5            | 2030
        'cardNumber' | 'firstName' | 'lastName' | 3            | null        | 5            | 2030
        'cardNumber' | 'firstName' | 'lastName' | 3            | 2016        | null         | 2030
        'cardNumber' | 'firstName' | 'lastName' | 3            | 2016        | 5            | null
        'cardNumber' | 'firstName' | 'lastName' | 99           | 2016        | 5            | 2030
        'cardNumber' | 'firstName' | 'lastName' | 3            | 0           | 5            | 2030
        'cardNumber' | 'firstName' | 'lastName' | 3            | 2016        | 99           | 2030
        'cardNumber' | 'firstName' | 'lastName' | 3            | 2016        | 5            | 0
    }

}
