package com.shop.user.model

import spock.lang.Specification

class UserTest extends Specification {

    def "add user address"() {
        given:
        User user = User.builder().build()
        when:
        user.addAddress(UserAddress.builder().build())
        then:
        user.getAddresses().size() == 1
    }

    def "add user address when null value"() {
        given:
        User user = User.builder().build()
        when:
        user.addAddress(null)
        then:
        thrown(NullPointerException.class)
    }

    def "remove user address"() {
        given:
        User user = User.builder().build()
        def userAddress = UserAddress.builder().build()
        user.addAddress(userAddress)
        when:
        user.removeAddress(userAddress.getId())
        then:
        user.getAddresses().size() == 0
    }

    def "remove user address when not exists"() {
        given:
        User user = User.builder().build()
        def userAddress = UserAddress.builder().build()
        user.addAddress(userAddress)
        when:
        user.removeAddress(UUID.randomUUID())
        then:
        user.getAddresses().size() == 1
    }

    def "add user payment card"() {
        given:
        User user = User.builder().build()
        when:
        user.addPaymentCard(createPaymentCard())
        then:
        user.getPaymentCards().size() == 1
    }

    def "add user payment card when null value"() {
        given:
        User user = User.builder().build()
        when:
        user.addPaymentCard(null)
        then:
        thrown(NullPointerException.class)
    }

    def "remove user payment card"() {
        given:
        User user = User.builder().build()
        def paymentCard = createPaymentCard()
        user.addPaymentCard(paymentCard)
        when:
        user.removeAddress(paymentCard.getId())
        then:
        user.getAddresses().size() == 0
    }

    def "remove user payment card when not exists"() {
        given:
        User user = User.builder().build()
        def paymentCard = createPaymentCard()
        user.addPaymentCard(paymentCard)
        when:
        user.removeAddress(UUID.randomUUID())
        then:
        user.getPaymentCards().size() == 1
    }

    private UserPaymentCard createPaymentCard() {
        return UserPaymentCard.builder()
            .cardNumber('cardNumber')
            .firstName('firstName')
            .lastName('lastName')
            .createdMonth(1)
            .createdYear(2016)
            .expiredMonth(1)
            .expiredYear(2030)
            .build()
    }

}
