package com.shop.user.model

import spock.lang.Specification

class UserAddressTest extends Specification {

    def "create user address"() {
        given:
        def builder = UserAddress.builder()
            .postcode('postcode')
            .city('city')
            .street('street')
            .description('description')
        when:
        def userAddress = builder.build()
        then:
        userAddress.getPostcode() == builder.postcode
        userAddress.getCity() == builder.city
        userAddress.getStreet() == builder.street
        userAddress.getDescription() == builder.description
    }

}
