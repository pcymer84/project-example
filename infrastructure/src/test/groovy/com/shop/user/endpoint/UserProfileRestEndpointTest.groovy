package com.shop.user.endpoint

import com.fasterxml.jackson.databind.ObjectMapper
import com.shop.AuthenticationHelper
import com.shop.FunctionalTest
import com.shop.core.initData.AccountInitDataHelper
import com.shop.user.model.UserInitData
import com.shop.core.initData.UserInitDataHelper
import com.shop.user.dto.UserProfileRequest
import com.shop.user.dto.UserProfileResponse
import com.shop.user.model.User
import com.shop.user.model.enums.Gender
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import java.time.LocalDate

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@FunctionalTest
class UserProfileRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    AuthenticationHelper authenticationHelper

    @Autowired
    AccountInitDataHelper accountHelper

    @Autowired
    UserInitDataHelper userHelper

    def "request get user profile endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def request = get('/api/v1/users/profile')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with ((UserProfileResponse) objectMapper.readValue(response.contentAsString, UserProfileResponse)) {
            it.id == testUser.getId()
            it.phone == testUser.getPhone()
            it.firstName == testUser.getFirstName()
            it.lastName == testUser.getLastName()
            it.gender == testUser.getGender()
            it.birthDate == testUser.getBirthDate()
        }
    }

    def "request get user profile endpoint when user unauthorized"() {
        when:
        def response = mockMvc.perform(get('/api/v1/users/profile')).andReturn().response
        then:
        response.status == 401
    }

    def "request update user profile endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        UserProfileRequest dto = new UserProfileRequest()
        dto.firstName = 'update first name'
        dto.lastName = 'update last name'
        dto.phone = "987654321"
        dto.gender = Gender.FEMALE
        dto.birthDate = LocalDate.of(1990, 5, 10)
        def request = put('/api/v1/users/profile')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        User user = userHelper.getUser(testUser.getId())
        with (user) {
            it.id == testUser.getId()
            it.phone == dto.getPhone()
            it.firstName == dto.getFirstName()
            it.lastName == dto.getLastName()
            it.gender == dto.getGender()
            it.birthDate == dto.getBirthDate()
        }
        with ((UserProfileResponse) objectMapper.readValue(response.contentAsString, UserProfileResponse)) {
            it.id == testUser.getId()
            it.phone == user.getPhone()
            it.firstName == user.getFirstName()
            it.lastName == user.getLastName()
            it.gender == user.getGender()
            it.birthDate == user.getBirthDate()
        }
    }

    def "request update user profile endpoint when empty fields"() {
        given:
        def testUser = UserInitData.USER_1
        UserProfileRequest dto = new UserProfileRequest()
        def request = put('/api/v1/users/profile')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        User user = userHelper.getUser(testUser.getId())
        with (user) {
            it.id == testUser.getId()
            it.phone == null
            it.firstName == null
            it.lastName == null
            it.gender == null
            it.birthDate == null
        }
        with ((UserProfileResponse) objectMapper.readValue(response.contentAsString, UserProfileResponse)) {
            it.id == testUser.getId()
            it.phone == null
            it.firstName == null
            it.lastName == null
            it.gender == null
            it.birthDate == null
        }
    }

    def "request update user profile endpoint user unauthorized"() {
        given:
        UserProfileRequest dto = new UserProfileRequest()
        dto.firstName = 'update first name'
        dto.lastName = 'update last name'
        def request = put('/api/v1/users/profile')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 401
    }

}