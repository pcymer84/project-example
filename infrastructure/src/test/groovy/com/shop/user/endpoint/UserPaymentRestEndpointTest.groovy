package com.shop.user.endpoint

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.shop.AuthenticationHelper
import com.shop.FunctionalTest
import com.shop.core.initData.AccountInitDataHelper
import com.shop.user.model.UserInitData
import com.shop.core.initData.UserInitDataHelper
import com.shop.user.dto.UserPaymentCardRequest
import com.shop.user.dto.UserPaymentCardResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@FunctionalTest
class UserPaymentRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    AuthenticationHelper authenticationHelper

    @Autowired
    AccountInitDataHelper accountHelper

    @Autowired
    UserInitDataHelper userHelper

    def "request get user payment card endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def request = get('/api/v1/users/paymentCards')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        def paymentCard = testUser.getPaymentCard()
        with((UserPaymentCardResponse) objectMapper.readValue(response.contentAsString, new TypeReference<List<UserPaymentCardResponse>>(){}).find()) {
            it.id == paymentCard.getId()
            it.cardNumber == paymentCard.getCardNumber()
            it.firstName == paymentCard.getFirstName()
            it.lastName == paymentCard.getLastName()
            it.createdMonth == paymentCard.getCreatedMonth()
            it.createdYear == paymentCard.getCreatedYear()
            it.expiredMonth == paymentCard.getExpiredMonth()
            it.expiredYear == paymentCard.getExpiredYear()
        }
    }

    def "request get user payment card endpoint when no cards"() {
        given:
        def testUser = UserInitData.USER_2
        def request = get('/api/v1/users/paymentCards')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        !objectMapper.readValue(response.contentAsString, new TypeReference<List<UserPaymentCardResponse>>(){}).any()
    }

    def "request create user payment card endpoint"() {
        given:
        def testUser = UserInitData.USER_2
        def dto = new UserPaymentCardRequest()
        dto.cardNumber = 'update cardNumber'
        dto.firstName = 'update firstName'
        dto.lastName = 'update lastName'
        dto.createdMonth = 1
        dto.createdYear = 2016
        dto.expiredMonth = 1
        dto.expiredYear = 2022
        def request = post('/api/v1/users/paymentCards')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        def paymentCard = userHelper.getUser(testUser.getId()).getPaymentCards().find()
        with (paymentCard) {
            it.id == paymentCard.getId()
            it.cardNumber == dto.getCardNumber()
            it.firstName == dto.getFirstName()
            it.lastName == dto.getLastName()
            it.createdMonth == dto.getCreatedMonth()
            it.createdYear == dto.getCreatedYear()
            it.expiredMonth == dto.getExpiredMonth()
            it.expiredYear == dto.getExpiredYear()
        }
        with (objectMapper.readValue(response.contentAsString, UserPaymentCardResponse)) {
            it.id == paymentCard.getId()
            it.cardNumber == paymentCard.getCardNumber()
            it.firstName == paymentCard.getFirstName()
            it.lastName == paymentCard.getLastName()
            it.createdMonth == paymentCard.getCreatedMonth()
            it.createdYear == paymentCard.getCreatedYear()
            it.expiredMonth == paymentCard.getExpiredMonth()
            it.expiredYear == paymentCard.getExpiredYear()
        }
        cleanup:
        userHelper.setupUser(testUser)
    }

    def "request create user payment card endpoint when invalid data"() {
        given:
        def testUser = UserInitData.USER_2
        def dto = new UserPaymentCardRequest()
        def request = post('/api/v1/users/paymentCards')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
    }

    def "request update user payment card endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new UserPaymentCardRequest()
        dto.cardNumber = 'update cardNumber'
        dto.firstName = 'update firstName'
        dto.lastName = 'update lastName'
        dto.createdMonth = 2
        dto.createdYear = 2015
        dto.expiredMonth = 3
        dto.expiredYear = 2023
        def request = put('/api/v1/users/paymentCards/' + testUser.getPaymentCard().getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        def paymentCard = userHelper.getUser(testUser.getId()).getPaymentCards().find()
        with (paymentCard) {
            it.id == paymentCard.getId()
            it.cardNumber == dto.getCardNumber()
            it.firstName == dto.getFirstName()
            it.lastName == dto.getLastName()
            it.createdMonth == dto.getCreatedMonth()
            it.createdYear == dto.getCreatedYear()
            it.expiredMonth == dto.getExpiredMonth()
            it.expiredYear == dto.getExpiredYear()
        }
        with (objectMapper.readValue(response.contentAsString, UserPaymentCardResponse)) {
            it.id == paymentCard.getId()
            it.cardNumber == paymentCard.getCardNumber()
            it.firstName == paymentCard.getFirstName()
            it.lastName == paymentCard.getLastName()
            it.createdMonth == paymentCard.getCreatedMonth()
            it.createdYear == paymentCard.getCreatedYear()
            it.expiredMonth == paymentCard.getExpiredMonth()
            it.expiredYear == paymentCard.getExpiredYear()
        }
        cleanup:
        userHelper.setupUser(testUser)
    }

    def "request update user payment card endpoint when invalid data"() {
        given:
        def testUser = UserInitData.USER_1
        def request = put('/api/v1/users/paymentCards/' + testUser.getPaymentCard().getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
    }

    def "request update user payment card endpoint when payment not exists"() {
        given:
        def testUser = UserInitData.USER_2
        def dto = new UserPaymentCardRequest()
        dto.cardNumber = 'update cardNumber'
        dto.firstName = 'update firstName'
        dto.lastName = 'update lastName'
        dto.createdMonth = 2
        dto.createdYear = 2015
        dto.expiredMonth = 3
        dto.expiredYear = 2023
        def request = put('/api/v1/users/paymentCards/' + UserInitData.USER_1.getPaymentCard().getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
    }

    def "request delete user payment card endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def request = delete('/api/v1/users/paymentCards/' + testUser.getPaymentCard().getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        !userHelper.getUser(testUser.getId()).findPaymentCard(testUser.getPaymentCard().getId()).isPresent()
        cleanup:
        userHelper.setupUser(testUser)
    }

}