package com.shop.user.endpoint

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.shop.AuthenticationHelper
import com.shop.FunctionalTest
import com.shop.core.initData.AccountInitDataHelper
import com.shop.user.model.UserInitData
import com.shop.core.initData.UserInitDataHelper
import com.shop.user.dto.UserAddressRequest
import com.shop.user.dto.UserAddressResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@FunctionalTest
class UserAddressRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    AuthenticationHelper authenticationHelper

    @Autowired
    AccountInitDataHelper accountHelper

    @Autowired
    UserInitDataHelper userHelper

    def "request get user address endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def request = get('/api/v1/users/addresses')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        def address = UserInitData.USER_1.getAddress()
        with((UserAddressResponse) objectMapper.readValue(response.contentAsString, new TypeReference<List<UserAddressResponse>>(){}).find()) {
            it.id == address.getId()
            it.postcode == address.getPostcode()
            it.city == address.getCity()
            it.street == address.getStreet()
            it.description == address.getDescription()
        }
    }

    def "request create user address endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new UserAddressRequest()
        dto.postcode = 'postcode'
        dto.city = 'city'
        dto.street = 'street'
        dto.description = 'description'
        def request = post('/api/v1/users/addresses')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        def result = objectMapper.readValue(response.contentAsString, UserAddressResponse)
        def address = userHelper.getUser(testUser.getId()).findAddress(result.getId()).get()
        with(address) {
            it.postcode == dto.postcode
            it.city == dto.city
            it.street == dto.street
            it.description == dto.description
        }
        with (result) {
            it.id == address.getId()
            it.postcode == dto.postcode
            it.city == dto.city
            it.street == dto.street
            it.description == dto.description
        }
        cleanup:
        userHelper.setupUser(testUser)
    }

    def "request update user address endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new UserAddressRequest()
        dto.postcode = 'postcode'
        dto.city = 'city'
        dto.street = 'street'
        dto.description = 'description'
        def request = put('/api/v1/users/addresses/' + testUser.getAddress().getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        def result = objectMapper.readValue(response.contentAsString, UserAddressResponse)
        def address = userHelper.getUser(testUser.getId()).findAddress(result.getId()).get()
        with(address) {
            it.postcode == dto.postcode
            it.city == dto.city
            it.street == dto.street
            it.description == dto.description
        }
        with (result) {
            it.id == address.getId()
            it.postcode == dto.postcode
            it.city == dto.city
            it.street == dto.street
            it.description == dto.description
        }
        cleanup:
        userHelper.setupUser(testUser)
    }

    def "request update user address endpoint when invalid data"() {
        given:
        def testUser = UserInitData.USER_1
        def request = put('/api/v1/users/addresses/' + testUser.getAddress().getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
    }

    def "request update user address endpoint when payment not exists"() {
        given:
        def testUser = UserInitData.USER_2
        def dto = new UserAddressRequest()
        dto.postcode = 'postcode'
        dto.city = 'city'
        dto.street = 'street'
        dto.description = 'description'
        def request = put('/api/v1/users/addresses/' + UserInitData.USER_1.getAddress().getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
    }

    def "request delete user address endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def request = delete('/api/v1/users/addresses/' + testUser.getAddress().getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        !userHelper.getUser(testUser.getId()).findAddress(testUser.getAddress().getId()).isPresent()
        cleanup:
        userHelper.setupUser(testUser)
    }

}