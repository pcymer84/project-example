package com.shop.rating.endpoint

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.shop.AuthenticationHelper
import com.shop.FunctionalTest
import com.shop.core.initData.AccountInitDataHelper
import com.shop.core.initData.ProductRatingInitDataHelper
import com.shop.core.restHandler.StandardResponseError
import com.shop.rating.dto.ProductReviewRequest
import com.shop.rating.dto.ProductReviewResponse
import com.shop.product.model.ProductInitData
import com.shop.rating.model.ProductRatingInitData
import com.shop.rating.model.ProductReviewInitData
import com.shop.user.model.UserInitData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@FunctionalTest
class ProductReviewRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    AuthenticationHelper authenticationHelper

    @Autowired
    AccountInitDataHelper accountHelper

    @Autowired
    ProductRatingInitDataHelper productRatingHelper

    def "request get product reviews endpoint"() {
        given:
        def testRating = ProductRatingInitData.RATING_1
        def testReview = testRating.getFirstReview()
        def request = get('/api/v1/products/' + testRating.getProductId() + '/reviews/')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        List<ProductReviewResponse> reviews = objectMapper.readValue(response.contentAsString, new TypeReference<List<ProductReviewResponse>>(){})
        reviews.size() == 1
        with(reviews.find { found -> found.id == testReview.id}) {
            it.id == testReview.getId()
            it.reviewerId == testReview.getReviewerId()
            it.comment == testReview.getComment()
            it.score == testReview.getScore()
            it.lastModified == testReview.getLastModified()
        }
    }

    def "request get product reviews endpoint when product not found"() {
        given:
        def testProduct = ProductInitData.UNSAVED_PRODUCT
        def request = get('/api/v1/products/' + testProduct.getId() + '/reviews/')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
    }

    def "request create product review endpoint"() {
        given:
        def testProductRating = ProductRatingInitData.RATING_4
        def testReview = testProductRating.getFirstReview()
        def dto = new ProductReviewRequest()
        dto.score = testReview.score
        dto.comment = testReview.comment
        def request = post('/api/v1/products/' + testProductRating.getProductId() + '/reviews/')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with(objectMapper.readValue(response.contentAsString, ProductReviewResponse)) {
            it.id != null
            it.reviewerId == testReview.getReviewerId()
            it.comment == testReview.getComment()
            it.score == testReview.getScore()
            it.lastModified != null
        }
        cleanup:
        productRatingHelper.setupProductRating(testProductRating)
    }

    def "request create product review endpoint when user already reviewed product"() {
        given:
        def testUser = UserInitData.USER_1
        def testProduct = ProductInitData.PRODUCT_2
        def testReview = ProductReviewInitData.UNSAVED_REVIEW
        def dto = new ProductReviewRequest()
        dto.score = testReview.score
        dto.comment = testReview.comment
        def request = post('/api/v1/products/' + testProduct.getId() + '/reviews/')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('product already reviewed by user \'%s\'',testUser.getId())
        }
    }

    def "request create product review endpoint when purchase not finished"() {
        given:
        def testUser = UserInitData.USER_1
        def testProduct = ProductInitData.PRODUCT_3
        def testReview = ProductReviewInitData.UNSAVED_REVIEW
        def dto = new ProductReviewRequest()
        dto.score = testReview.score
        dto.comment = testReview.comment
        def request = post('/api/v1/products/' + testProduct.getId() + '/reviews/')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('no finished purchase found for product \'%s\' and reviewer \'%s\'', testProduct.getId(), testUser.getId())
        }
    }

    def "request create product review endpoint when seller review product"() {
        given:
        def testUser = UserInitData.USER_1
        def testProduct = ProductInitData.PRODUCT_1
        def testReview = ProductReviewInitData.UNSAVED_REVIEW
        def dto = new ProductReviewRequest()
        dto.score = testReview.score
        dto.comment = testReview.comment
        def request = post('/api/v1/products/' + testProduct.getId() + '/reviews/')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == 'seller cannot review own product'
        }
    }

    def "request update product review endpoint"() {
        given:
        def testProductRating = ProductRatingInitData.RATING_2
        def testReview = testProductRating.getFirstReview()
        def dto = new ProductReviewRequest()
        dto.score = 3
        dto.comment = 'mediocre monitor'
        def request = put('/api/v1/products/' + testProductRating.getProductId() + '/reviews/' + testReview.getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with(objectMapper.readValue(response.contentAsString, ProductReviewResponse)) {
            it.id == testReview.getId()
            it.reviewerId == testReview.getReviewerId()
            it.comment == dto.getComment()
            it.score == dto.getScore()
            it.lastModified != testReview.getLastModified()
        }
        cleanup:
        productRatingHelper.setupProductRating(testProductRating)
    }

    def "request update product review endpoint when review not found"() {
        given:
        def testReview = ProductReviewInitData.REVIEW_2
        def testReviewedProduct = ProductInitData.PRODUCT_1
        def dto = new ProductReviewRequest()
        dto.score = 3
        dto.comment = 'mediocre monitor'
        def request = put('/api/v1/products/' + testReviewedProduct.getId() + '/reviews/' + testReview.getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('rating with id \'%s\' for reviewer \'%s\' not found', testReview.getId(), testReview.getReviewerId())
        }
    }

    def "request censorship product review endpoint"() {
        given:
        def testProductRating = ProductRatingInitData.RATING_2
        def testReview = testProductRating.getFirstReview()
        def request = put('/api/v1/products/' + testProductRating.getProductId() + '/reviews/' + testReview.getId() + '/censorship')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with(objectMapper.readValue(response.contentAsString, ProductReviewResponse)) {
            it.id == testReview.getId()
            it.reviewerId == testReview.getReviewerId()
            it.comment == 'censorship'
            it.score == testReview.score
            it.lastModified != testReview.getLastModified()
        }
        cleanup:
        productRatingHelper.setupProductRating(testProductRating)
    }

    def "request censorship product review endpoint when review not found"() {
        given:
        def testReview = ProductReviewInitData.REVIEW_2
        def testReviewedProduct = ProductInitData.PRODUCT_1
        def request = put('/api/v1/products/' + testReviewedProduct.getId() + '/reviews/' + testReview.getId() + '/censorship')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('rating with id \'%s\' not found', testReview.getId() )
        }
    }

    def "request censorship product review endpoint when no permissions"() {
        given:
        def testReview = ProductReviewInitData.REVIEW_2
        def testReviewedProduct = ProductInitData.PRODUCT_2
        def request = put('/api/v1/products/' + testReviewedProduct.getId() + '/reviews/' + testReview.getId() + '/censorship')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_2))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 403
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == 'Access is denied'
        }
    }

}
