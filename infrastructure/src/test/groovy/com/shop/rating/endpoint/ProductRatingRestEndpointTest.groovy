package com.shop.rating.endpoint

import com.fasterxml.jackson.databind.ObjectMapper
import com.shop.AuthenticationHelper
import com.shop.FunctionalTest
import com.shop.core.initData.AccountInitDataHelper
import com.shop.core.initData.ProductRatingInitDataHelper
import com.shop.rating.dto.ProductRatingResponse
import com.shop.rating.model.ProductRatingInitData
import com.shop.user.model.UserInitData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@FunctionalTest
class ProductRatingRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    AuthenticationHelper authenticationHelper

    @Autowired
    AccountInitDataHelper accountHelper

    @Autowired
    ProductRatingInitDataHelper productRatingHelper

    def "request get product rating endpoint"() {
        given:
        def testRating = ProductRatingInitData.RATING_1
        def testReview = ProductRatingInitData.RATING_1.getFirstReview()
        def request = get('/api/v1/products/' + testRating.getProductId() + '/rating')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with(objectMapper.readValue(response.contentAsString, ProductRatingResponse)) {
            it.productId == testRating.getProductId()
            it.sellerId == testRating.getSellerId()
            it.averageScore == testReview.getScore()
            it.reviews[0].score == testReview.getScore()
            it.reviews[0].comment == testReview.getComment()
            it.reviews[0].lastModified == testReview.getLastModified()
        }
    }

}
