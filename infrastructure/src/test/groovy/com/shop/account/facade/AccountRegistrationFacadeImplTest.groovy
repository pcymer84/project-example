package com.shop.account.facade

import com.shop.account.dto.AccountResponse
import com.shop.account.dto.RegistrationRequest
import com.shop.account.dto.FacebookRegistrationRequest
import com.shop.account.mapper.AccountMapper
import com.shop.account.model.Account
import com.shop.account.model.AccountFactory
import com.shop.account.repository.AccountRepository
import com.shop.account.service.FacebookService
import com.shop.account.model.FacebookUser
import com.shop.core.eventBus.EventPublisher
import com.shop.user.mapper.UserProfileMapper
import com.shop.user.repository.UserRepository
import spock.lang.Specification

class AccountRegistrationFacadeImplTest extends Specification {

    AccountRegistrationFacadeImpl accountRegistrationFacade
    AccountFactory accountFactory
    AccountRepository accountRepository
    UserRepository userRepository
    FacebookService facebookService
    AccountMapper accountMapper
    UserProfileMapper userProfileMapper
    EventPublisher eventPublisher

    def setup() {
        accountFactory = Mock()
        accountRepository = Mock()
        userRepository = Mock()
        facebookService = Mock()
        accountMapper = Mock()
        userProfileMapper = Mock()
        eventPublisher = Mock()
        accountRegistrationFacade = new AccountRegistrationFacadeImpl(
                accountFactory,
                accountRepository,
                facebookService,
                accountMapper,
                eventPublisher
        )
    }

    def "register user using credentials"() {
        given:
        def request = new RegistrationRequest()
        def account = new Account()
        def response = new AccountResponse()
        when:
        def result = accountRegistrationFacade.register(request)
        then:
        result != null
        1 * accountFactory.createForRegistration(request) >> account
        1 * accountRepository.save(account)
        1 * eventPublisher.publishEvent(_)
        1 * accountMapper.mapToAccountResponse(account) >> response
    }

    def "register user using facebook"() {
        given:
        def accessToken = 'accessToken'
        def request = new FacebookRegistrationRequest()
        request.facebookAccessToken = accessToken
        def facebookDto = new FacebookUser()
        def account = new Account()
        def response = new AccountResponse()
        when:
        def result = accountRegistrationFacade.register(request)
        then:
        result != null
        1 * facebookService.getFacebookUser(accessToken) >> facebookDto
        1 * accountFactory.createFromFacebookUser(facebookDto) >> account
        1 * accountRepository.save(account)
        1 * eventPublisher.publishEvent(_)
        1 * accountMapper.mapToAccountResponse(account) >> response
    }

}