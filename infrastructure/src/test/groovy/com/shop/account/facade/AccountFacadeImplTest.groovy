package com.shop.account.facade

import com.shop.account.dto.AcceptPasswordRequest
import com.shop.account.dto.ChangeEmailRequest
import com.shop.account.dto.ChangePasswordRequest
import com.shop.account.dto.ResetPasswordRequest
import com.shop.account.model.Account
import com.shop.account.policy.EmailPolicy
import com.shop.account.policy.PasswordPolicy
import com.shop.account.repository.AccountRepository
import com.shop.account.service.AccountEmailSendingService
import com.shop.account.service.SpringAuthenticationServiceImpl
import org.springframework.security.core.Authentication
import spock.lang.Specification

class AccountFacadeImplTest extends Specification {

    AccountFacadeImpl accountFacade
    SpringAuthenticationServiceImpl authenticationService
    EmailPolicy accountEmailPolicy
    PasswordPolicy accountPasswordPolicy
    AccountRepository accountRepository
    AccountEmailSendingService accountEmailSendingService

    def setup() {
        authenticationService = Mock()
        accountEmailPolicy = Mock()
        accountPasswordPolicy = Mock()
        accountRepository = Mock()
        accountEmailSendingService = Mock()
        accountFacade = new AccountFacadeImpl(
                authenticationService,
                accountEmailSendingService,
                accountRepository,
                accountEmailPolicy,
                accountPasswordPolicy
        )
    }

    def "change user email"() {
        given:
        def accountId = UUID.randomUUID()
        def oldEmail = 'oldEmail'
        def newEmail = 'newEmail'
        def password = 'password'
        def account = Mock(Account)
        def authentication = Mock(Authentication)
        def request = new ChangeEmailRequest()
        request.email = newEmail
        request.password = password
        account.getEmail() >> oldEmail
        authentication.getPrincipal() >> account
        when:
        accountFacade.changeEmail(accountId, request)
        then:
        1 * accountRepository.getById(accountId) >> account
        1 * authenticationService.authenticateCredentials(oldEmail, password) >> authentication
        1 * account.changeEmail(newEmail, accountEmailPolicy)
        1 * accountRepository.save(account)
        1 * accountEmailSendingService.sendChangeEmailAddressEmail(account)
    }

    def "change user password"() {
        given:
        def userId = UUID.randomUUID()
        def email = 'email'
        def oldPassword = 'oldPassword'
        def newPassword = 'newPassword'
        def account = Mock(Account)
        def authentication = Mock(Authentication)
        def request = new ChangePasswordRequest()
        request.oldPassword = oldPassword
        request.newPassword = newPassword
        account.getEmail() >> email
        when:
        accountFacade.changePassword(userId, request)
        then:
        1 * accountRepository.getById(userId) >> account
        1 * authenticationService.authenticateCredentials(email, oldPassword) >> authentication
        1 * account.changePassword(newPassword, accountPasswordPolicy)
        1 * accountRepository.save(account)
    }

    def "reset user password"() {
        given:
        def email = 'email'
        def account = Mock(Account)
        def request = new ResetPasswordRequest()
        request.email = email
        account.getEmail() >> email
        when:
        accountFacade.resetPassword(request)
        then:
        1 * accountRepository.getByEmail(email) >> account
        1 * account.generateResetPasswordCode()
        1 * accountEmailSendingService.sendResetPasswordEmail(account)
        1 * accountRepository.save(account)
    }

    def "accept user password"() {
        given:
        def code = UUID.randomUUID()
        def password = 'password'
        def account = Mock(Account)
        def request = new AcceptPasswordRequest(code, password)
        when:
        accountFacade.acceptPassword(request)
        then:
        1 * accountRepository.getByResetPasswordCode(code) >> account
        1 * account.changePassword(password, accountPasswordPolicy)
        1 * accountRepository.save(account)
    }

}