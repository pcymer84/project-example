package com.shop.account.facade

import com.shop.account.dto.AuthenticateRequest
import com.shop.account.dto.FacebookAuthenticateRequest
import com.shop.account.service.AuthenticationService
import spock.lang.Specification

class AccountAuthFacadeImplTest extends Specification {

    AccountAuthFacadeImpl userAuthFacade
    AuthenticationService authenticationService

    def setup() {
        authenticationService = Mock()
        userAuthFacade = new AccountAuthFacadeImpl(
                authenticationService
        )
    }

    def "login user using credentials"() {
        given:
        def login = 'login'
        def password = 'password'
        def jwtToken = 'jwtToken'
        def request = new AuthenticateRequest(login, password)
        when:
        def result = userAuthFacade.authenticate(request)
        then:
        result == jwtToken
        1 * authenticationService.authenticateCredentials(login, password) >> jwtToken
    }

    def "login user using facebook"() {
        given:
        def accessToken = 'accessToken'
        def jwtToken = 'jwtToken'
        def request = new FacebookAuthenticateRequest()
        request.facebookAccessToken = accessToken
        when:
        def result = userAuthFacade.authenticate(request)
        then:
        result == jwtToken
        1 * authenticationService.authenticateFacebookToken(accessToken) >> jwtToken
    }

}