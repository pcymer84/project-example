package com.shop.account.model

import com.shop.account.ex.UserAlreadyUsingEmailException
import com.shop.account.ex.WeakPasswordException
import com.shop.account.policy.EmailPolicy
import com.shop.account.policy.PasswordPolicy
import com.shop.core.validator.ex.ValidationException
import spock.lang.Specification

class AccountTest extends Specification {

    def "change email"() {
        given:
        def account = new Account()
        account.id = UUID.randomUUID()
        account.email = 'test@test.com'
        def emailPolicy = Mock(EmailPolicy)
        when:
        account.changeEmail('email@email.com', emailPolicy)
        then:
        account.getEmail() == 'email@email.com'
    }

    def "change email when invalid email"() {
        given:
        def account = new Account()
        account.id = UUID.randomUUID()
        account.email = 'test@test.com'
        def emailPolicy = Mock(EmailPolicy)
        when:
        account.changeEmail('email.com', emailPolicy)
        then:
        thrown(ValidationException.class)
    }

    def "change email when account already exists"() {
        given:
        def account = new Account()
        account.id = UUID.randomUUID()
        account.email = 'test@test.com'
        def emailPolicy = Mock(EmailPolicy)
        emailPolicy.checkEmailUsed(account.id, 'email@email.com') >> { throw new UserAlreadyUsingEmailException("account already exists") }
        when:
        account.changeEmail('email@email.com', emailPolicy )
        then:
        thrown(UserAlreadyUsingEmailException.class)
    }

    def "change password"() {
        given:
        def password = 'Qwerty123!'
        def account = new Account()
        def passwordPolicy = Mock(PasswordPolicy)
        passwordPolicy.encodePassword(password) >> password
        when:
        account.changePassword(password, passwordPolicy)
        then:
        account.getPassword() == password
    }

    def "change password when password too weak"() {
        given:
        def password = 'password!'
        def account = new Account()
        def passwordPolicy = Mock(PasswordPolicy)
        passwordPolicy.checkPasswordStrength(password) >> { throw new WeakPasswordException("password too weak") }
        passwordPolicy.encodePassword(password) >> password
        when:
        account.changePassword(password, passwordPolicy)
        then:
        thrown(WeakPasswordException.class)
    }

    def "generate reset password code"() {
        given:
        def account = new Account()
        when:
        account.generateResetPasswordCode()
        then:
        account.getResetPasswordCode() != null
    }

    def "generate reset password code when already generated"() {
        given:
        def passwordCode = UUID.randomUUID()
        def account = new Account()
        account.resetPasswordCode = passwordCode
        when:
        account.generateResetPasswordCode()
        then:
        account.getResetPasswordCode() == passwordCode
    }

}
