package com.shop.account.endpoint

import com.fasterxml.jackson.databind.ObjectMapper
import com.shop.FunctionalTest
import com.shop.account.service.FacebookService
import com.shop.account.model.FacebookUser
import com.shop.account.ex.InvalidFacebookTokenException
import com.shop.account.dto.AuthenticateRequest
import com.shop.account.dto.FacebookAuthenticateRequest
import com.shop.user.model.UserInitData
import com.shop.core.restHandler.StandardResponseError
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

@FunctionalTest
class AuthenticationRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    FacebookService facebookService

    def "request authenticate user using credentials endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new AuthenticateRequest()
        dto.login = testUser.getEmail()
        dto.password = testUser.getPassword()
        def request = post('/api/v1/accounts/authenticate')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        response.contentAsString != null
        response.contentAsString.length() == 538
    }

    def "request authenticate user using credentials endpoint when wrong password"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new AuthenticateRequest()
        dto.login = testUser.getEmail()
        dto.password = testUser.getPassword() + '!'
        def request = post('/api/v1/accounts/authenticate')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == 'invalid login or password'
        }
    }

    def "request authenticate user using credentials endpoint when user doesn't exists"() {
        given:
        def testUser = UserInitData.UNSAVED_USER
        def dto = new AuthenticateRequest()
        dto.login = testUser.getEmail()
        dto.password = testUser.getPassword()
        def request = post('/api/v1/accounts/authenticate')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == 'invalid login or password'
        }
    }

    def "request authenticate user using facebook endpoint"() {
        given:
        def dto = new FacebookAuthenticateRequest()
        dto.facebookAccessToken = 'facebookToken'
        def facebookUser = new FacebookUser()
        facebookUser.facebookId = UserInitData.USER_1.getFacebookId()
        facebookService.getFacebookUser(dto.facebookAccessToken) >> facebookUser
        def request = post('/api/v1/accounts/facebookAuthenticate')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        response.contentAsString != null
        response.contentAsString.length() == 538
    }

    def "request authenticate user using facebook endpoint when user not found"() {
        given:
        def dto = new FacebookAuthenticateRequest()
        dto.facebookAccessToken = 'accessToken'
        def facebookUser = new FacebookUser()
        facebookUser.facebookId = UserInitData.UNSAVED_USER.facebookId
        facebookService.getFacebookUser('accessToken') >> facebookUser
        def request = post('/api/v1/accounts/facebookAuthenticate')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == 'invalid facebook token or account not found'
        }
    }

    def "request authenticate user using facebook endpoint when invalid token"() {
        given:
        def dto = new FacebookAuthenticateRequest()
        dto.facebookAccessToken = 'accessToken'
        facebookService.getFacebookUser('accessToken') >> { throw new InvalidFacebookTokenException('invalid token')}
        def request = post('/api/v1/accounts/facebookAuthenticate')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == 'invalid facebook token or account not found'
        }
    }

}