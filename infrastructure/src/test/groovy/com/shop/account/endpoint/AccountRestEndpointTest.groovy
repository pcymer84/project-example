package com.shop.account.endpoint

import com.fasterxml.jackson.databind.ObjectMapper
import com.shop.AuthenticationHelper
import com.shop.FunctionalTest
import com.shop.account.dto.AcceptPasswordRequest
import com.shop.account.dto.ChangeEmailRequest
import com.shop.account.dto.ChangePasswordRequest
import com.shop.account.dto.ResetPasswordRequest
import com.shop.core.initData.AccountInitDataHelper
import com.shop.user.model.UserInitData
import com.shop.core.initData.UserInitDataHelper
import com.shop.core.restHandler.StandardResponseError
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@FunctionalTest
class AccountRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    AuthenticationHelper authenticationHelper

    @Autowired
    AccountInitDataHelper accountHelper

    @Autowired
    UserInitDataHelper userHelper

    def "request change user email endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def email = UserInitData.UNSAVED_USER.getEmail()
        def dto = new ChangeEmailRequest()
        dto.email = email
        dto.password = testUser.getPassword()
        def request = put('/api/v1/accounts/changeEmail')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with(accountHelper.getAccount(testUser.getId())) {
            it.email == email
        }
        cleanup:
        accountHelper.setupAccount(testUser)
    }

    def "request change user email when unauthorized"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new ChangeEmailRequest()
        dto.email = testUser.getEmail()
        dto.password = testUser.getPassword()
        def request = put('/api/v1/accounts/changeEmail')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 401
    }

    def "request change user email when wrong password"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new ChangeEmailRequest()
        dto.email = testUser.getEmail()
        dto.password = testUser.getPassword() + "!"
        def request = put('/api/v1/accounts/changeEmail')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.code == 1001
            it.message == 'invalid password'
        }
    }

    def "request change user email when email already used"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new ChangeEmailRequest()
        dto.email = testUser.getEmail()
        dto.password = testUser.getPassword()
        def request = put('/api/v1/accounts/changeEmail')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.code == 1002
            it.message == String.format('account with id \'%s\' is already using email \'%s\'', testUser.getId(), testUser.getEmail())
        }
    }

    def "request change user email when user with email already exists"() {
        given:
        def testUser = UserInitData.USER_1
        def email = UserInitData.USER_2.getEmail()
        def dto = new ChangeEmailRequest()
        dto.email = email
        dto.password = testUser.getPassword()
        def request = put('/api/v1/accounts/changeEmail')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.code == 1003
            it.message == String.format('account with email \'%s\' already exists', email)
        }
    }

    def "request change user password endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new ChangePasswordRequest()
        dto.oldPassword = testUser.getPassword()
        dto.newPassword = testUser.getPassword() + '!'
        def request = put('/api/v1/accounts/changePassword')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with(accountHelper.getAccount(testUser.getId())) {
            it.password != testUser.getHashedPassword()
        }
        cleanup:
        accountHelper.setupAccount(testUser)
    }

    def "request change user password endpoint when unauthorized"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new ChangePasswordRequest()
        dto.oldPassword = testUser.getPassword()
        dto.newPassword = testUser.getPassword() + '!'
        def request = put('/api/v1/accounts/changePassword')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 401
    }

    def "request change user password endpoint when weak password"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new ChangePasswordRequest()
        dto.oldPassword = testUser.getPassword()
        dto.newPassword = 'Qwe1!'
        def request = put('/api/v1/accounts/changePassword')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == 'Request validation errors: [newPassword] Password must be 8 or more characters in length.'
        }
    }

    def "request change user password endpoint when wrong password"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new ChangePasswordRequest()
        dto.oldPassword = testUser.getPassword() + "@"
        dto.newPassword = testUser.getPassword() + '!'
        def request = put('/api/v1/accounts/changePassword')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.code == 1001
            it.message == 'invalid password'
        }
    }

    def "request reset user password endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new ResetPasswordRequest()
        dto.email = testUser.getEmail()
        def request = put('/api/v1/accounts/resetPassword')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with(accountHelper.getAccount(testUser.getId())) {
            it.resetPasswordCode == testUser.getResetPasswordCode()
        }
        cleanup:
        accountHelper.setupAccount(testUser)
    }

    def "request reset user password endpoint when user not found"() {
        given:
        def email = UserInitData.UNSAVED_USER.getEmail()
        def dto = new ResetPasswordRequest()
        dto.email = email
        def request = put('/api/v1/accounts/resetPassword')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.code == 1001
            it.message == String.format('account with email \'%s\' not found', email)
        }
    }

    def "request accept user password endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def dto = new AcceptPasswordRequest()
        dto.code = testUser.getResetPasswordCode()
        dto.password = 'Qwerty1!'
        def request = put('/api/v1/accounts/acceptPassword')
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with (accountHelper.getAccount(testUser.getId())) {
            it.resetPasswordCode == null
            it.password != null
        }
        cleanup:
        accountHelper.setupAccount(testUser)
    }

    def "request accept user password endpoint when code not found"() {
        given:
        def resetPasswordCode = UserInitData.UNSAVED_USER.getResetPasswordCode()
        def dto = new AcceptPasswordRequest()
        dto.code = resetPasswordCode
        dto.password = 'Qwerty1!'
        def request = put('/api/v1/accounts/acceptPassword')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.code == 1001
            it.message == String.format('code \'%s\' not found or already used', resetPasswordCode)
        }
    }

}