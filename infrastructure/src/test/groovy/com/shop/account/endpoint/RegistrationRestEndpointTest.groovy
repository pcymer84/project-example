package com.shop.account.endpoint

import com.fasterxml.jackson.databind.ObjectMapper
import com.shop.FunctionalTest
import com.shop.account.dto.AccountResponse
import com.shop.account.service.FacebookService
import com.shop.account.model.FacebookUser
import com.shop.account.ex.InvalidFacebookTokenException
import com.shop.account.dto.RegistrationRequest
import com.shop.account.dto.FacebookRegistrationRequest
import com.shop.core.initData.AccountInitDataHelper
import com.shop.user.model.UserInitData
import com.shop.core.initData.UserInitDataHelper
import com.shop.core.restHandler.StandardResponseError
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

@FunctionalTest
class RegistrationRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    FacebookService facebookService

    @Autowired
    UserInitDataHelper userHelper

    @Autowired
    AccountInitDataHelper accountHelper

    def "request register user endpoint"() {
        given:
        def testUser = UserInitData.UNSAVED_USER
        def dto = new RegistrationRequest()
        dto.email = testUser.getEmail()
        dto.password = testUser.getPassword()
        def request = post('/api/v1/accounts/register')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        def result = objectMapper.readValue(response.contentAsString, AccountResponse)
        with (result) {
            it.id != null
            it.email == testUser.getEmail()
        }
        with (accountHelper.getAccount(result.getId())) {
            it.email == testUser.getEmail()
            it.password != null
        }
        with (userHelper.getUser(result.getId())) {
            it.firstName == null
            it.lastName == null
        }
        cleanup:
        accountHelper.deleteAccount(result.id)
        userHelper.deleteUser(result.id)
    }

    def "request register user endpoint when invalid data"() {
        given:
        def dto = new RegistrationRequest()
        dto.email = email
        dto.password = password
        def request = post('/api/v1/accounts/register')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        where:
        email       | password
        null        | 'Qwerty123!'
        'test'      | 'Qwerty123!'
        'test@test' | null
    }

    def "request register user endpoint when invalid email"() {
        given:
        def dto = new RegistrationRequest()
        dto.email = 'test'
        dto.password = 'Qwerty123!'
        def request = post('/api/v1/accounts/register')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == 'Request validation errors: [email] must be a well-formed email address'
        }
    }

    def "request register user endpoint when invalid password"() {
        given:
        def dto = new RegistrationRequest()
        dto.email = 'test2@test2'
        dto.password = 'Test1234'
        def request = post('/api/v1/accounts/register')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == 'Request validation errors: [password] Password must contain 1 or more special characters.'
        }
    }

    def "request register facebook user endpoint"() {
        given:
        def testUser = UserInitData.UNSAVED_USER
        def facebookUser = new FacebookUser()
        facebookUser.facebookId = testUser.getFacebookId()
        facebookUser.email = testUser.getEmail()
        facebookUser.firstName = testUser.getFirstName()
        facebookUser.lastName = testUser.getLastName()
        facebookService.getFacebookUser('accessToken') >> facebookUser
        def dto = new FacebookRegistrationRequest()
        dto.facebookAccessToken = 'accessToken'
        def request = post('/api/v1/accounts/facebookRegister')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        def result = objectMapper.readValue(response.contentAsString, AccountResponse)
        with (result) {
            it.id != null
            it.email == testUser.getEmail()
            it.facebookId == testUser.getFacebookId()
        }
        with (accountHelper.getAccount(result.getId())) {
            it.email == testUser.getEmail()
            it.password == null
            it.facebookId == testUser.getFacebookId()
        }
        with (userHelper.getUser(result.getId())) {
            it.firstName == testUser.getFirstName()
            it.lastName == testUser.getLastName()
        }
        cleanup:
        accountHelper.deleteAccount(result.id)
        userHelper.deleteUser(result.id)
    }

    def "request register facebook user endpoint when invalid facebook token"() {
        given:
        facebookService.getFacebookUser('accessToken') >> { throw new InvalidFacebookTokenException('invalid token') }
        FacebookRegistrationRequest dto = new FacebookRegistrationRequest()
        dto.facebookAccessToken = 'accessToken'
        def request = post('/api/v1/accounts/facebookRegister')
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == 'invalid token'
        }
    }

}