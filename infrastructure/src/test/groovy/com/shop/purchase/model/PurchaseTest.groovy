package com.shop.purchase.model

import com.google.common.collect.Sets
import com.shop.purchase.ex.InvalidPurchaseStateException
import com.shop.purchase.ex.NotEnoughPurchaseProductsException
import com.shop.purchase.ex.PurchaseProductFromDIfferentSellerException
import com.shop.purchase.ex.UnmodifiablePurchaseException
import spock.lang.Specification

class PurchaseTest extends Specification {

    def "create purchase when all fields filled"() {
        given:
        def purchaseId = UUID.randomUUID()
        def buyer = UUID.randomUUID()
        def productId = UUID.randomUUID()
        def sellerId = UUID.randomUUID()
        def createDate = new Date()
        def realizedDate = new Date()
        def finishedDate = new Date()
        def invoice = createInvoice()
        def delivery = createDelivery()
        def builder = Purchase.builder()
                .id(purchaseId)
                .buyerId(buyer)
                .sellerId(sellerId)
                .status(PurchaseStatus.OPEN)
                .createdAt(createDate)
                .realizedAt(realizedDate)
                .finishedAt(finishedDate)
                .invoice(invoice)
                .delivery(delivery)
                .purchaseProducts(Sets.newHashSet(createPurchaseProduct(productId, sellerId)))
        when:
        def result = builder.build()
        then:
        with(result) {
            it.id == purchaseId
            it.buyerId == buyer
            it.sellerId == sellerId
            it.status == PurchaseStatus.OPEN
            it.createdAt == createDate
            it.realizedAt == realizedDate
            it.finishedAt == finishedDate
            it.invoice == invoice
            it.delivery == delivery
            it.purchaseProducts[0].product.productId == productId
            it.purchaseProducts[0].product.sellerId == sellerId
        }
    }

    def "add product to purchase"() {
        given:
        def productId = UUID.randomUUID()
        def sellerId = UUID.randomUUID()
        def purchaseProduct = createPurchaseProduct(productId, sellerId)
        def purchase = createPurchase(UUID.randomUUID(), sellerId)
        when:
        purchase.addProduct(purchaseProduct)
        then:
        purchase.findPurchaseProduct(productId).isPresent()
        purchase.getPurchaseProducts().size() == 2
    }

    def "add product to purchase when order unmodifiable"() {
        given:
        def sellerId = UUID.randomUUID()
        def purchaseProduct = createPurchaseProduct(UUID.randomUUID(), sellerId)
        def purchase = createPurchase(UUID.randomUUID(), sellerId)
        purchase.status = PurchaseStatus.REALIZED
        when:
        purchase.addProduct(purchaseProduct)
        then:
        thrown(UnmodifiablePurchaseException.class)
        purchase.getPurchaseProducts().size() == 1
    }

    def "add product to purchase when product from different seller"() {
        given:
        def purchaseProduct = createPurchaseProduct(UUID.randomUUID(), UUID.randomUUID())
        def purchase = createPurchase(UUID.randomUUID(), UUID.randomUUID())
        when:
        purchase.addProduct(purchaseProduct)
        then:
        thrown(PurchaseProductFromDIfferentSellerException.class)
        purchase.getPurchaseProducts().size() == 1
    }

    def "remove product from purchase"() {
        given:
        def productId = UUID.randomUUID()
        def sellerId = UUID.randomUUID()
        def purchase = createPurchase(UUID.randomUUID(), sellerId)
        def purchaseProduct = createPurchaseProduct(productId, sellerId)
        purchase.addProduct(purchaseProduct)
        when:
        purchase.removeProduct(purchaseProduct)
        then:
        purchase.getPurchaseProducts().size() == 1
    }

    def "remove product from purchase when purchase is unmodifiable"() {
        given:
        def sellerId = UUID.randomUUID()
        def purchase = createPurchase(UUID.randomUUID(), sellerId)
        def purchaseProduct = createPurchaseProduct(UUID.randomUUID(), sellerId)
        purchase.status = PurchaseStatus.REALIZED
        when:
        purchase.removeProduct(purchaseProduct)
        then:
        thrown(UnmodifiablePurchaseException.class)
        purchase.getPurchaseProducts().size() == 1
    }

    def "remove product from purchase when not enough products in purchase to delete one"() {
        given:
        def productId = UUID.randomUUID()
        def sellerId = UUID.randomUUID()
        def purchase = createPurchase(productId, sellerId)
        def purchaseProduct = createPurchaseProduct(UUID.randomUUID(), sellerId)
        when:
        purchase.removeProduct(purchaseProduct)
        then:
        thrown(NotEnoughPurchaseProductsException.class)
        purchase.getPurchaseProducts().size() == 1
    }

    def "cancel purchase"() {
        given:
        def purchase = createPurchase(UUID.randomUUID(), UUID.randomUUID())
        when:
        purchase.cancelPurchase()
        then:
        purchase.status == PurchaseStatus.CANCELLED
    }

    def "cancel purchase when purchase unmodifiable"() {
        given:
        def purchase = createPurchase(UUID.randomUUID(), UUID.randomUUID())
        purchase.status = PurchaseStatus.REALIZED
        when:
        purchase.cancelPurchase()
        then:
        thrown(UnmodifiablePurchaseException.class)
        purchase.status == PurchaseStatus.REALIZED
    }

    def "realize purchase"() {
        given:
        def purchase = createPurchase(UUID.randomUUID(), UUID.randomUUID())
        when:
        purchase.realizePurchase()
        then:
        purchase.status == PurchaseStatus.REALIZED
        purchase.realizedAt != null
    }

    def "realize purchase when purchase unmodifiable"() {
        given:
        def purchase = createPurchase(UUID.randomUUID(), UUID.randomUUID())
        purchase.status = PurchaseStatus.REALIZED
        when:
        purchase.realizePurchase()
        then:
        thrown(UnmodifiablePurchaseException.class)
        purchase.status == PurchaseStatus.REALIZED
    }

    def "send purchase"() {
        given:
        def purchase = createPurchase(UUID.randomUUID(), UUID.randomUUID())
        purchase.status = PurchaseStatus.REALIZED
        when:
        purchase.sendPurchase()
        then:
        purchase.status == PurchaseStatus.SENT
        purchase.sendAt != null
    }

    def "send purchase when purchase unmodifiable"() {
        given:
        def purchase = createPurchase(UUID.randomUUID(), UUID.randomUUID())
        purchase.status = PurchaseStatus.FINISHED
        when:
        purchase.sendPurchase()
        then:
        thrown(InvalidPurchaseStateException.class)
        purchase.status == PurchaseStatus.FINISHED
    }

    def "finish purchase"() {
        given:
        def purchase = createPurchase(UUID.randomUUID(), UUID.randomUUID())
        purchase.status = PurchaseStatus.SENT
        when:
        purchase.finishPurchase()
        then:
        purchase.status == PurchaseStatus.FINISHED
        purchase.finishedAt != null
    }

    def "finish purchase when purchase unmodifiable"() {
        given:
        def purchase = createPurchase(UUID.randomUUID(), UUID.randomUUID())
        purchase.status = PurchaseStatus.REALIZED
        when:
        purchase.finishPurchase()
        then:
        thrown(InvalidPurchaseStateException.class)
        purchase.status == PurchaseStatus.REALIZED
    }

    private static Purchase createPurchase(UUID productId, UUID sellerId) {
        return Purchase.builder()
                .buyerId(UUID.randomUUID())
                .sellerId(sellerId)
                .status(PurchaseStatus.OPEN)
                .createdAt(new Date())
                .invoice(createInvoice())
                .delivery(createDelivery())
                .purchaseProducts(Sets.newHashSet(createPurchaseProduct(productId, sellerId)))
                .build()
    }

    private static Invoice createInvoice() {
        return new Invoice('name', 'nip', 'address', new Date())
    }

    private static Delivery createDelivery() {
        return new Delivery('postcode', 'city', 'street', 'description')
    }

    private static PurchaseProduct createPurchaseProduct(UUID productId, UUID sellerId) {
        return PurchaseProduct.builder()
                .amount(1)
                .product(createProductDetails(productId, sellerId))
                .build()
    }

    private static ProductDetails createProductDetails(UUID productId, UUID sellerId) {
        return ProductDetails.builder()
                .sellerId(sellerId)
                .productId(productId)
                .name('name')
                .price(77.7)
                .description('description')
                .imageUrl('http://imgUrl.com')
                .build()
    }

}
