package com.shop.purchase.endpoint

import com.fasterxml.jackson.databind.ObjectMapper
import com.shop.AuthenticationHelper
import com.shop.FunctionalTest
import com.shop.purchase.model.DeliveryInitData
import com.shop.purchase.model.DeliveryInitDataFactory
import com.shop.purchase.model.PurchaseInitData
import com.shop.core.initData.PurchaseInitDataHelper
import com.shop.user.model.UserInitData
import com.shop.core.restHandler.StandardResponseError
import com.shop.purchase.dto.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@FunctionalTest
class PurchaseDeliveryRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    AuthenticationHelper authenticationHelper

    @Autowired
    PurchaseInitDataHelper purchaseHelper

    @Autowired
    DeliveryInitDataFactory deliveryInitDataFactory

    def "request get purchase delivery endpoint"() {
        given:
        def testPurchase = PurchaseInitData.PURCHASE_1
        def request = get('/api/v1/purchases/' + testPurchase.getId() + '/delivery')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with (objectMapper.readValue(response.contentAsString, DeliveryResponse)) {
            it.postcode == testPurchase.getDelivery().getPostcode()
            it.city == testPurchase.getDelivery().getCity()
            it.street == testPurchase.getDelivery().getStreet()
            it.description == testPurchase.getDelivery().getDescription()
        }
    }

    def "request get purchase delivery when purchase doesn't exists endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def purchaseId = PurchaseInitData.UNSAVED_PURCHASE.getId()
        def request = get('/api/v1/purchases/' + purchaseId + '/delivery')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('purchase with id \'%s\' for buyer \'%s\' not found', purchaseId, testUser.getId())
        }
    }

    def "request update delivery address endpoint"() {
        given:
        def testPurchase = PurchaseInitData.PURCHASE_1
        def testDelivery = DeliveryInitData.UPDATE_DELIVERY
        def request = put('/api/v1/purchases/' + testPurchase.getId() + '/delivery')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(deliveryInitDataFactory.createDeliveryRequest(testDelivery)))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with (objectMapper.readValue(response.contentAsString, DeliveryResponse)) {
            it.postcode == testDelivery.getPostcode()
            it.city == testDelivery.getCity()
            it.street == testDelivery.getStreet()
            it.description == testDelivery.getDescription()
        }
        with (purchaseHelper.getPurchase(testPurchase.getId()).getDelivery()) {
            it.postcode == testDelivery.getPostcode()
            it.city == testDelivery.getCity()
            it.street == testDelivery.getStreet()
            it.description == testDelivery.getDescription()
        }
        cleanup:
        purchaseHelper.setupPurchase(testPurchase)
    }

    def "request update delivery address when purchase doesn't exists endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def purchaseId = PurchaseInitData.UNSAVED_PURCHASE.getId()
        def request = put('/api/v1/purchases/' + purchaseId + '/delivery')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(deliveryInitDataFactory.createDeliveryRequest(DeliveryInitData.UPDATE_DELIVERY)))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('purchase with id \'%s\' for buyer \'%s\' not found', purchaseId, testUser.getId())
        }
    }

}
