package com.shop.purchase.endpoint

import com.fasterxml.jackson.databind.ObjectMapper
import com.shop.AuthenticationHelper
import com.shop.FunctionalTest
import com.shop.purchase.model.InvoiceInitDataFactory
import com.shop.purchase.model.InvoiceInitData
import com.shop.purchase.model.PurchaseInitData
import com.shop.core.initData.PurchaseInitDataHelper
import com.shop.user.model.UserInitData
import com.shop.core.restHandler.StandardResponseError
import com.shop.purchase.dto.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@FunctionalTest
class PurchaseInvoiceRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    AuthenticationHelper authenticationHelper

    @Autowired
    PurchaseInitDataHelper purchaseHelper

    @Autowired
    InvoiceInitDataFactory invoiceInitDataFactory

    def "request get purchase invoice endpoint"() {
        given:
        def testPurchase = PurchaseInitData.PURCHASE_1
        def request = get('/api/v1/purchases/' + testPurchase.getId() + '/invoice')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with (objectMapper.readValue(response.contentAsString, InvoiceResponse)) {
            it.name == testPurchase.getInvoice().getName()
            it.address == testPurchase.getInvoice().getAddress()
            it.date == testPurchase.getInvoice().getDate()
            it.nip == testPurchase.getInvoice().getNip()
        }
    }

    def "request get purchase invoice when purchase doesn't exists endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def purchaseId = PurchaseInitData.UNSAVED_PURCHASE.getId()
        def request = get('/api/v1/purchases/' + purchaseId + '/invoice')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('purchase with id \'%s\' for buyer \'%s\' not found', purchaseId, testUser.getId())
        }
    }

    def "request update invoice endpoint"() {
        given:
        def testPurchase = PurchaseInitData.PURCHASE_1
        def testInvoiceUpdate = InvoiceInitData.UPDATE_INVOICE
        def request = put('/api/v1/purchases/' + testPurchase.getId() + '/invoice')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(invoiceInitDataFactory.createInvoiceRequest(testInvoiceUpdate)))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with (objectMapper.readValue(response.contentAsString, InvoiceResponse)) {
            it.nip == testInvoiceUpdate.getNip()
            it.name == testInvoiceUpdate.getName()
            it.address == testInvoiceUpdate.getAddress()
            it.date == testInvoiceUpdate.getDate()
        }
        with (purchaseHelper.getPurchase(testPurchase.getId()).getInvoice()) {
            it.nip == testInvoiceUpdate.getNip()
            it.name == testInvoiceUpdate.getName()
            it.address == testInvoiceUpdate.getAddress()
            it.date == testInvoiceUpdate.getDate()
        }
        cleanup:
        purchaseHelper.setupPurchase(testPurchase)
    }

    def "request update invoice when purchase doesn't exists endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def purchaseId = PurchaseInitData.UNSAVED_PURCHASE.getId()
        def request = put('/api/v1/purchases/' + purchaseId + '/invoice')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(invoiceInitDataFactory.createInvoiceRequest(InvoiceInitData.UPDATE_INVOICE)))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('purchase with id \'%s\' for buyer \'%s\' not found', purchaseId, testUser.getId())
        }
    }

}
