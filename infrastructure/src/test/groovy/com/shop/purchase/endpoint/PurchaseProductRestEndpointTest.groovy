package com.shop.purchase.endpoint

import com.fasterxml.jackson.databind.ObjectMapper
import com.shop.AuthenticationHelper
import com.shop.FunctionalTest
import com.shop.core.initData.ProductInitDataHelper
import com.shop.product.model.ProductInitData
import com.shop.purchase.model.PurchaseInitData
import com.shop.core.initData.PurchaseInitDataHelper
import com.shop.user.model.UserInitData
import com.shop.core.restHandler.StandardResponseError
import com.shop.purchase.dto.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@FunctionalTest
class PurchaseProductRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    AuthenticationHelper authenticationHelper

    @Autowired
    PurchaseInitDataHelper purchaseHelper

    @Autowired
    ProductInitDataHelper productHelper

    def "request add product to purchase endpoint"() {
        given:
        def amount = 3
        def testPurchase = PurchaseInitData.PURCHASE_1
        def testProduct = ProductInitData.PRODUCT_4
        def request = put('/api/v1/purchases/' + testPurchase.getId() + '/products/' + testProduct.getId() + '/amount/' + amount)
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with (objectMapper.readValue(response.contentAsString, PurchaseProductResponse)) {
            it.productId == testProduct.getId()
            it.sellerId == testProduct.getSellerId()
            it.name == testProduct.getName()
            it.price == testProduct.getPrice()
            it.description == testProduct.getDescription()
            it.imageUrl == testProduct.getImageUrl()
            it.amount == amount
        }
        with (purchaseHelper.getPurchase(testPurchase.getId()).getPurchaseProduct(testProduct.getId())) {
            it.product.productId == testProduct.getId()
            it.product.sellerId == testProduct.getSellerId()
            it.product.name == testProduct.getName()
            it.product.price == testProduct.getPrice()
            it.product.description == testProduct.getDescription()
            it.product.imageUrl == testProduct.getImageUrl()
            it.amount == amount
        }
        with (productHelper.getProduct(testProduct.getId())) {
            it.stockNumber == testProduct.stockNumber - amount
        }
        cleanup:
        purchaseHelper.setupPurchase(testPurchase)
        productHelper.setupProduct(testProduct)
    }

    def "request add product to purchase endpoint when purchase doesn't exists"() {
        given:
        def testPurchase = PurchaseInitData.UNSAVED_PURCHASE
        def testProduct = ProductInitData.PRODUCT_2
        def request = put('/api/v1/purchases/' + testPurchase.getId() + '/products/' + testProduct.getId() + '/amount/1')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('purchase with id \'%s\' for buyer \'%s\' not found', testPurchase.getId(), testPurchase.getBuyerId())
        }
    }

    def "request add product to purchase endpoint when purchase doesn't belong to user"() {
        given:
        def testUser = UserInitData.USER_1
        def testPurchase = PurchaseInitData.PURCHASE_2
        def testProduct = ProductInitData.UNSAVED_PRODUCT
        def request = put('/api/v1/purchases/' + testPurchase.getId() + '/products/' + testProduct.getId() + '/amount/1')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('purchase with id \'%s\' for buyer \'%s\' not found', testPurchase.getId(), testUser.getId())
        }
    }

    def "request add product to purchase endpoint when product doesn't exists"() {
        given:
        def testPurchase = PurchaseInitData.PURCHASE_1
        def testProduct = ProductInitData.UNSAVED_PRODUCT
        def request = put('/api/v1/purchases/' + testPurchase.getId() + '/products/' + testProduct.getId() + '/amount/1')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('product with id \'%s\' not found', testProduct.getId())
        }
    }

    def "request remove product from purchase endpoint"() {
        given:
        def testPurchase = PurchaseInitData.PURCHASE_1
        def testProduct = ProductInitData.PRODUCT_2
        def testPurchaseProduct = testPurchase.findPurchaseProduct(testProduct.id)
        def request = delete('/api/v1/purchases/' + testPurchase.getId() + '/products/' + testProduct.getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        !purchaseHelper.getPurchase(testPurchase.getId()).findPurchaseProduct(testProduct.getId()).isPresent()
        with (productHelper.getProduct(testProduct.getId())) {
            it.stockNumber == testProduct.stockNumber + testPurchaseProduct.amount
        }
        cleanup:
        purchaseHelper.setupPurchase(testPurchase)
        productHelper.setupProduct(testProduct)
    }

    def "request remove product from purchase endpoint when purchase doesn't exists"() {
        given:
        def testPurchase = PurchaseInitData.UNSAVED_PURCHASE
        def testProduct = ProductInitData.PRODUCT_2
        def request = delete('/api/v1/purchases/' + testPurchase.getId() + '/products/' + testProduct.getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('purchase with id \'%s\' for buyer \'%s\' not found', testPurchase.getId(), testPurchase.getBuyerId())
        }
    }

    def "request remove product from purchase endpoint when purchase doesn't belong to user"() {
        given:
        def testUser = UserInitData.USER_1
        def testPurchase = PurchaseInitData.PURCHASE_2
        def testProduct = ProductInitData.PRODUCT_2
        def request = delete('/api/v1/purchases/' + testPurchase.getId() + '/products/' + testProduct.getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('purchase with id \'%s\' for buyer \'%s\' not found', testPurchase.getId(), testUser.getId())
        }
    }

    def "request remove product from purchase endpoint when product not found in purchase"() {
        given:
        def testPurchase = PurchaseInitData.PURCHASE_1
        def testProduct = ProductInitData.PRODUCT_4
        def request = delete('/api/v1/purchases/' + testPurchase.getId() + '/products/' + testProduct.getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format('product \'%s\' not found in purchase \'%s\'', testProduct.getId(), testPurchase.getId())
        }
    }

}
