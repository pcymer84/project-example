package com.shop.purchase.endpoint

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.collect.Lists
import com.shop.AuthenticationHelper
import com.shop.FunctionalTest
import com.shop.core.initData.ProductInitDataHelper
import com.shop.product.model.ProductInitData
import com.shop.purchase.dto.PurchaseProductRequest
import com.shop.purchase.model.PurchaseInitData
import com.shop.purchase.model.PurchaseInitDataFactory
import com.shop.core.initData.PurchaseInitDataHelper
import com.shop.user.model.UserInitData
import com.shop.purchase.dto.DeliveryRequest
import com.shop.purchase.dto.PurchaseRequest
import com.shop.purchase.dto.PurchaseResponse
import com.shop.purchase.model.PurchaseStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static com.shop.product.model.ProductInitData.*
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@FunctionalTest
class PurchaseRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    AuthenticationHelper authenticationHelper

    @Autowired
    PurchaseInitDataHelper purchaseHelper

    @Autowired
    ProductInitDataHelper productHelper

    @Autowired
    PurchaseInitDataFactory purchaseInitDataFactory

    def "request get purchase endpoint"() {
        given:
        def testPurchase = PurchaseInitData.PURCHASE_2
        def testPurchaseProduct = testPurchase.getFirstPurchaseProduct()
        def request = get('/api/v1/purchases/' + testPurchase.getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_2))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with (objectMapper.readValue(response.contentAsString, PurchaseResponse)) {
            it.id == testPurchase.getId()
            it.buyerId == testPurchase.getBuyerId()
            it.status == testPurchase.getStatus()
            it.delivery.postcode == testPurchase.getDelivery().getPostcode()
            it.delivery.city == testPurchase.getDelivery().getCity()
            it.delivery.street == testPurchase.getDelivery().getStreet()
            it.delivery.description == testPurchase.getDelivery().getDescription()
            it.invoice.name == testPurchase.getInvoice().getName()
            it.invoice.address == testPurchase.getInvoice().getAddress()
            it.invoice.date == testPurchase.getInvoice().getDate()
            it.invoice.nip == testPurchase.getInvoice().getNip()
            it.products[0].productId == testPurchaseProduct.getProductId()
            it.products[0].sellerId == testPurchaseProduct.getSellerId()
            it.products[0].name == testPurchaseProduct.getName()
            it.products[0].price == testPurchaseProduct.getPrice()
            it.products[0].description == testPurchaseProduct.getDescription()
        }
    }

    def "request submit purchase endpoint"() {
        given:
        def testPurchase = PurchaseInitData.UNSAVED_PURCHASE
        def testPurchaseProduct = testPurchase.getFirstPurchaseProduct()
        def request = post('/api/v1/purchases')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(purchaseInitDataFactory.createPurchaseRequest(testPurchase)))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        PurchaseResponse result = objectMapper.readValue(response.contentAsString, PurchaseResponse)
        with (result) {
            it.id != null
            it.buyerId == testPurchase.getBuyerId()
            it.status == PurchaseStatus.OPEN
            it.delivery.postcode == testPurchase.getDelivery().getPostcode()
            it.delivery.city == testPurchase.getDelivery().getCity()
            it.delivery.street == testPurchase.getDelivery().getStreet()
            it.delivery.description == testPurchase.getDelivery().getDescription()
            it.invoice.name == testPurchase.getInvoice().getName()
            it.invoice.address == testPurchase.getInvoice().getAddress()
            it.invoice.date == testPurchase.getInvoice().getDate()
            it.invoice.nip == testPurchase.getInvoice().getNip()
            it.products[0].productId == testPurchaseProduct.getProductId()
            it.products[0].sellerId == testPurchaseProduct.getSellerId()
            it.products[0].name == testPurchaseProduct.getName()
            it.products[0].price == testPurchaseProduct.getPrice()
            it.products[0].description == testPurchaseProduct.getDescription()
        }
        with (purchaseHelper.getPurchase(result.getId())) {
            it.id == result.getId()
            it.buyerId == testPurchase.getBuyerId()
            it.status == PurchaseStatus.OPEN
            it.delivery.postcode == testPurchase.getDelivery().getPostcode()
            it.delivery.city == testPurchase.getDelivery().getCity()
            it.delivery.street == testPurchase.getDelivery().getStreet()
            it.delivery.description == testPurchase.getDelivery().getDescription()
            it.invoice.name == testPurchase.getInvoice().getName()
            it.invoice.address == testPurchase.getInvoice().getAddress()
            it.invoice.date == testPurchase.getInvoice().getDate()
            it.invoice.nip == testPurchase.getInvoice().getNip()
            it.purchaseProducts[0].product.productId == testPurchaseProduct.getProductId()
            it.purchaseProducts[0].product.sellerId == testPurchaseProduct.getSellerId()
            it.purchaseProducts[0].product.name == testPurchaseProduct.getName()
            it.purchaseProducts[0].product.price == testPurchaseProduct.getPrice()
            it.purchaseProducts[0].product.description == testPurchaseProduct.getDescription()
        }
        with (productHelper.getProduct(testPurchaseProduct.productId)) {
            it.stockNumber == PRODUCT_1.stockNumber - testPurchaseProduct.amount
        }
        cleanup:
        purchaseHelper.deletePurchase(result.getId())
        productHelper.setupProduct(PRODUCT_1)
    }

    def "request submit purchase endpoint when invalid data"() {
        given:
        def delivery = new DeliveryRequest()
        delivery.postcode = postcode
        delivery.city = city
        delivery.street = street
        def dto = new PurchaseRequest()
        dto.products = products
        dto.delivery = delivery
        def request = post('/api/v1/purchases')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == status
        where:
        products                                                        | postcode   | city   | street   | status
        Lists.newArrayList()                                            | 'postcode' | 'city' | 'street' | 400
        Lists.newArrayList(createProductRequest(UNSAVED_PRODUCT))       | 'postcode' | 'city' | 'street' | 404
        Lists.newArrayList(createProductRequest(PRODUCT_2, -1)) | 'postcode' | 'city' | 'street' | 400
        Lists.newArrayList(createProductRequest(PRODUCT_2))             | null       | 'city' | 'street' | 400
        Lists.newArrayList(createProductRequest(PRODUCT_2))             | 'postcode' | null   | 'street' | 400
        Lists.newArrayList(createProductRequest(PRODUCT_2))             | 'postcode' | 'city' | null     | 400
    }

    PurchaseProductRequest createProductRequest(ProductInitData init) {
        return new PurchaseProductRequest(init.getId(), 1)
    }

    PurchaseProductRequest createProductRequest(ProductInitData init, Integer amount) {
        return new PurchaseProductRequest(init.getId(), amount)
    }

}

