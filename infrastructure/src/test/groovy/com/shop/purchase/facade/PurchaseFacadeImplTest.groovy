package com.shop.purchase.facade

import com.shop.core.eventBus.EventPublisher
import com.shop.purchase.dto.DeliveryRequest
import com.shop.purchase.dto.DeliveryResponse
import com.shop.purchase.dto.InvoiceRequest
import com.shop.purchase.dto.InvoiceResponse
import com.shop.purchase.dto.PurchaseProductResponse
import com.shop.purchase.dto.PurchaseRequest
import com.shop.purchase.dto.PurchaseResponse
import com.shop.purchase.event.AddProductToPurchaseEvent
import com.shop.purchase.event.CancelPurchaseEvent
import com.shop.purchase.event.PurchaseEventFactory
import com.shop.purchase.event.RemoveProductFromPurchaseEvent
import com.shop.purchase.event.SubmitPurchaseEvent
import com.shop.purchase.mapper.DeliveryMapper
import com.shop.purchase.mapper.InvoiceMapper
import com.shop.purchase.mapper.PurchaseMapper
import com.shop.purchase.model.Delivery
import com.shop.purchase.model.Invoice
import com.shop.purchase.model.Purchase
import com.shop.purchase.model.PurchaseFactory
import com.shop.purchase.model.PurchaseProduct
import com.shop.purchase.model.PurchaseProductFactory
import com.shop.purchase.repository.PurchaseRepository
import spock.lang.Specification

class PurchaseFacadeImplTest extends Specification {

    private PurchaseFacadeImpl purchaseFacade
    private PurchaseFactory purchaseFactory
    private PurchaseRepository purchaseRepository
    private PurchaseProductFactory purchaseProductFactory
    private PurchaseMapper purchaseMapper
    private DeliveryMapper deliveryMapper
    private InvoiceMapper invoiceMapper
    private EventPublisher eventPublisher
    private PurchaseEventFactory purchaseEventFactory

    def setup() {
        purchaseFactory = Mock()
        purchaseRepository = Mock()
        purchaseProductFactory = Mock()
        purchaseMapper = Mock()
        deliveryMapper = Mock()
        invoiceMapper = Mock()
        eventPublisher = Mock()
        purchaseEventFactory = Mock()
        purchaseFacade = new PurchaseFacadeImpl(
                purchaseFactory,
                purchaseRepository,
                purchaseProductFactory,
                purchaseMapper,
                deliveryMapper,
                invoiceMapper,
                eventPublisher,
                purchaseEventFactory
        )
    }

    def "get purchase"() {
        given:
        def buyerId = UUID.randomUUID()
        def purchaseId = UUID.randomUUID()
        def purchase = Mock(Purchase)
        def response = Mock(PurchaseResponse)
        when:
        def result = purchaseFacade.getPurchase(buyerId, purchaseId)
        then:
        result != null
        1 * purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId) >> purchase
        1 * purchaseMapper.mapToPurchaseOrderResponse(purchase) >> response
    }

    def "get purchase invoice"() {
        given:
        def buyerId = UUID.randomUUID()
        def purchaseId = UUID.randomUUID()
        def purchase = Mock(Purchase)
        def invoice = Mock(Invoice)
        def invoiceResponse = Mock(InvoiceResponse)
        when:
        def result = purchaseFacade.getPurchaseInvoice(buyerId, purchaseId)
        then:
        result != null
        1 * purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId) >> purchase
        1 * purchase.getInvoice() >> invoice
        1 * invoiceMapper.mapToInvoiceResponse(invoice) >> invoiceResponse
    }

    def "get purchase delivery"() {
        given:
        def buyerId = UUID.randomUUID()
        def purchaseId = UUID.randomUUID()
        def purchase = Mock(Purchase)
        def delivery = Mock(Delivery)
        def deliveryResponse = Mock(DeliveryResponse)
        when:
        def result = purchaseFacade.getPurchaseDelivery(buyerId, purchaseId)
        then:
        result != null
        1 * purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId) >> purchase
        1 * purchase.getDelivery() >> delivery
        1 * deliveryMapper.mapToDeliveryResponse(delivery) >> deliveryResponse
    }

    def "submit purchase"() {
        given:
        def buyerId = UUID.randomUUID()
        def purchaseRequest = Mock(PurchaseRequest)
        def purchase = Mock(Purchase)
        def purchaseResponse = Mock(PurchaseResponse)
        def submitPurchaseEvent = Mock(SubmitPurchaseEvent)
        when:
        def result = purchaseFacade.submitPurchase(buyerId, purchaseRequest)
        then:
        result != null
        1 * purchaseFactory.createPurchaseToSubmit(buyerId, purchaseRequest) >> purchase
        1 * purchaseEventFactory.createSubmitPurchaseEvent(purchase) >> submitPurchaseEvent
        1 * purchaseRepository.save(purchase)
        1 * purchaseMapper.mapToPurchaseOrderResponse(purchase) >> purchaseResponse
        1 * eventPublisher.publishEvent(submitPurchaseEvent)
    }

    def "change purchase invoice"() {
        given:
        def buyerId = UUID.randomUUID()
        def purchaseId = UUID.randomUUID()
        def purchase = Mock(Purchase)
        def invoiceRequest = Mock(InvoiceRequest)
        def invoice = Mock(Invoice)
        def invoiceResponse = Mock(InvoiceResponse)
        when:
        def result = purchaseFacade.changePurchaseInvoice(buyerId, purchaseId, invoiceRequest)
        then:
        result != null
        1 * purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId) >> purchase
        1 * invoiceMapper.mapToInvoice(invoiceRequest) >> invoice
        1 * purchase.changeInvoice(invoice)
        1 * purchaseRepository.save(purchase)
        1 * invoiceMapper.mapToInvoiceResponse(_) >> invoiceResponse
    }

    def "change purchase delivery"() {
        given:
        def buyerId = UUID.randomUUID()
        def purchaseId = UUID.randomUUID()
        def purchase = Mock(Purchase)
        def deliveryRequest = Mock(DeliveryRequest)
        def delivery = Mock(Delivery)
        def deliveryResponse = Mock(DeliveryResponse)
        when:
        def result = purchaseFacade.changePurchaseDelivery(buyerId, purchaseId, deliveryRequest)
        then:
        result != null
        1 * purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId) >> purchase
        1 * deliveryMapper.mapToDelivery(deliveryRequest) >> delivery
        1 * purchase.changeDelivery(delivery)
        1 * purchaseRepository.save(purchase)
        1 * deliveryMapper.mapToDeliveryResponse(_) >> deliveryResponse
    }

    def "add product to purchase"() {
        given:
        def buyerId = UUID.randomUUID()
        def purchaseId = UUID.randomUUID()
        def productId = UUID.randomUUID()
        def amount = 5
        def purchase = Mock(Purchase)
        def purchaseProduct = Mock(PurchaseProduct)
        def purchaseProductResponse = Mock(PurchaseProductResponse)
        def addPurchaseEvent = Mock(AddProductToPurchaseEvent)
        when:
        def result = purchaseFacade.addProductToPurchase(buyerId, purchaseId, productId, amount)
        then:
        result != null
        1 * purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId) >> purchase
        1 * purchaseProductFactory.createPurchaseProduct(productId, amount) >> purchaseProduct
        1 * purchaseEventFactory.createAddProductToPurchaseEvent(purchaseProduct) >> addPurchaseEvent
        1 * purchase.addProduct(purchaseProduct)
        1 * purchaseRepository.save(purchase)
        1 * purchaseMapper.mapToPurchaseProductResponse(purchaseProduct) >> purchaseProductResponse
        1 * eventPublisher.publishEvent(addPurchaseEvent)
    }

    def "remove product from purchase"() {
        given:
        def buyerId = UUID.randomUUID()
        def purchaseId = UUID.randomUUID()
        def productId = UUID.randomUUID()
        def purchaseProduct = Mock(PurchaseProduct)
        def purchase = Mock(Purchase)
        def removeProductFromPurchaseEvent = Mock(RemoveProductFromPurchaseEvent)
        purchase.getPurchaseProduct(productId) >> purchaseProduct
        when:
        purchaseFacade.removeProductFromPurchase(buyerId, purchaseId, productId)
        then:
        1 * purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId) >> purchase
        1 * purchaseEventFactory.createRemoveProductFromPurchaseEvent(purchaseProduct) >> removeProductFromPurchaseEvent
        1 * purchase.removeProduct(purchaseProduct)
        1 * purchaseRepository.save(purchase)
        1 * eventPublisher.publishEvent(removeProductFromPurchaseEvent)
    }

    def "cancel purchase"() {
        given:
        def buyerId = UUID.randomUUID()
        def purchaseId = UUID.randomUUID()
        def purchase = Mock(Purchase)
        def purchaseResponse = Mock(PurchaseResponse)
        def cancelPurchaseEvent = Mock(CancelPurchaseEvent)
        when:
        def result = purchaseFacade.cancelPurchase(buyerId, purchaseId)
        then:
        result != null
        1 * purchaseRepository.getByIdAndBuyerId(purchaseId, buyerId) >> purchase
        1 * purchaseEventFactory.createCancelPurchaseEvent(purchase) >> cancelPurchaseEvent
        1 * purchase.cancelPurchase()
        1 * purchaseRepository.save(purchase)
        1 * purchaseMapper.mapToPurchaseOrderResponse(purchase) >> purchaseResponse
        1 * eventPublisher.publishEvent(cancelPurchaseEvent)
    }

    def "realize purchase"() {
        given:
        def sellerId = UUID.randomUUID()
        def purchaseId = UUID.randomUUID()
        def purchase = Mock(Purchase)
        def purchaseResponse = Mock(PurchaseResponse)
        when:
        def result = purchaseFacade.realizePurchase(sellerId, purchaseId)
        then:
        result != null
        1 * purchaseRepository.getByIdAndSellerId(purchaseId, sellerId) >> purchase
        1 * purchase.realizePurchase()
        1 * purchaseRepository.save(purchase)
        1 * purchaseMapper.mapToPurchaseOrderResponse(purchase) >> purchaseResponse
    }

    def "send purchase"() {
        given:
        def sellerId = UUID.randomUUID()
        def purchaseId = UUID.randomUUID()
        def purchase = Mock(Purchase)
        def purchaseResponse = Mock(PurchaseResponse)
        when:
        def result = purchaseFacade.sendPurchase(sellerId, purchaseId)
        then:
        result != null
        1 * purchaseRepository.getByIdAndSellerId(purchaseId, sellerId) >> purchase
        1 * purchase.sendPurchase()
        1 * purchaseRepository.save(purchase)
        1 * purchaseMapper.mapToPurchaseOrderResponse(purchase) >> purchaseResponse
    }

    def "finish purchase"() {
        given:
        def sellerId = UUID.randomUUID()
        def purchaseId = UUID.randomUUID()
        def purchase = Mock(Purchase)
        def purchaseResponse = Mock(PurchaseResponse)
        when:
        def result = purchaseFacade.finishPurchase(sellerId, purchaseId)
        then:
        result != null
        1 * purchaseRepository.getByIdAndSellerId(purchaseId, sellerId) >> purchase
        1 * purchase.finishPurchase()
        1 * purchaseRepository.save(purchase)
        1 * purchaseMapper.mapToPurchaseOrderResponse(purchase) >> purchaseResponse
    }

}
