package com.shop.core.validator.impl

import spock.lang.Specification

class CurrencyValidationTest extends Specification {

    CurrencyValidation validator = new CurrencyValidation()

    def "currency validation"() {
        expect:
        validator.isValid(Double.valueOf(value), null) == result
        where:
        value     | result
        '5'       | true
        '1.50'    | true
        '1.550'   | true
        '0.25'    | true
        '111.0'   | true
        '0.0'     | true
        '0'       | true
        '-10.73'  | false
        '-5'      | false
        '1.222'   | false
    }

}
