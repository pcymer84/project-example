package com.shop.core.validator.impl

import spock.lang.Specification

class UrlValidationTest extends Specification {

    UrlValidation validator = new UrlValidation()

    def "url validation"() {
        expect:
        validator.isValid(value, null) == result
        where:
        value                   | result
        'http://qwe.pl'         | true
        'http://qwe.pl/qwe.qwe' | true
        'https://qwe.pl'        | true
        'ftp://qwe.pl'          | true
        'http://qwe'            | false
        'http:/qwe.pl'          | false
        'http//qwe.pl'          | false
        'qwe.pl'                | false
        'http:://qwe.pl'        | false
    }

}
