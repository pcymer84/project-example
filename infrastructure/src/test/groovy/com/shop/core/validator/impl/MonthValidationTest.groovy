package com.shop.core.validator.impl

import spock.lang.Specification

class MonthValidationTest extends Specification {

    MonthValidation validator = new MonthValidation()

    def "month validation"() {
        expect:
        validator.isValid(value, null) == result
        where:
        value | result
        -1    | false
        0     | false
        5     | true
        12    | true
        13    | false
    }

}
