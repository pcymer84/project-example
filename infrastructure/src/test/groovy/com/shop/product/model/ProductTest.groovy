package com.shop.product.model

import com.shop.core.validator.ex.ValidationException
import spock.lang.Specification

class ProductTest extends Specification {

    def "create product when all fields filled"() {
        given:
        UUID productId = UUID.randomUUID()
        UUID userId = UUID.randomUUID()
        def productBuilder = new Product.ProductBuilder()
                .id(productId)
                .sellerId(userId)
                .name("name")
                .description("description")
                .price(77.7)
                .imageUrl("http://foo.bar.com")
        when:
        def result = productBuilder.build()
        then:
        with(result) {
            id == productId
            sellerId == userId
            name == "name"
            description == "description"
            price == 77.7
            imageUrl == "http://foo.bar.com"
        }
    }

    def "create product when url is invalid"() {
        given:
        UUID productId = UUID.randomUUID()
        UUID userId = UUID.randomUUID()
        def productBuilder = new Product.ProductBuilder()
                .id(productId)
                .sellerId(userId)
                .name("name")
                .description("description")
                .price(77.7)
                .imageUrl("foo.bar.com")
        when:
        productBuilder.build()
        then:
        def ex = thrown(ValidationException.class)
        ex.message == 'validation exception when creating object \'Product\' with violations : [{property=\'imageUrl\', violation=\'invalid url format\', value=\'foo.bar.com\'}]'
    }

    def "create product when price is invalid"() {
        given:
        UUID productId = UUID.randomUUID()
        UUID userId = UUID.randomUUID()
        def productBuilder = new Product.ProductBuilder()
                .id(productId)
                .sellerId(userId)
                .name("name")
                .price(-77.77777)
        when:
        productBuilder.build()
        then:
        def ex = thrown(ValidationException.class)
        ex.message == 'validation exception when creating object \'Product\' with violations : [{property=\'price\', violation=\'currency value should be positive decimal with maximum 2 places after dot\', value=\'-77.77777\'}]'
    }

    def "create product when sellerId is null"() {
        given:
        UUID productId = UUID.randomUUID()
        def productBuilder = new Product.ProductBuilder()
                .id(productId)
                .name("name")
                .price(77.77)
        when:
        productBuilder.build()
        then:
        thrown(ValidationException.class)
    }

}
