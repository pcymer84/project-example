package com.shop.product.facade

import com.shop.product.dto.ProductRequest
import com.shop.product.dto.ProductResponse
import com.shop.product.factory.ProductSpecificationFactory
import com.shop.product.mapper.ProductMapper
import com.shop.product.model.Product
import com.shop.product.model.ProductTransfer
import com.shop.product.model.ProductTransferFactory
import com.shop.product.repository.ProductRepository
import spock.lang.Specification

class ProductFacadeImplTest extends Specification {

    ProductFacadeImpl productFacade
    ProductRepository productRepository
    ProductMapper productMapper
    ProductTransferFactory productTransferFactory
    ProductSpecificationFactory productSpecificationFactory

    def setup() {
        productRepository = Mock()
        productMapper = Mock()
        productTransferFactory = Mock()
        productSpecificationFactory = Mock()
        productFacade = new ProductFacadeImpl(
                productRepository,
                productMapper,
                productTransferFactory,
                productSpecificationFactory
        )
    }

    def "register product"() {
        given:
        def userId = UUID.randomUUID()
        def request = Mock(ProductRequest)
        def product = Mock(Product)
        def productTransfer = Mock(ProductTransfer)
        def response = Mock(ProductResponse)
        def amount = 5
        request.getStockNumber() >> amount
        when:
        def result = productFacade.registerProduct(userId, request)
        then:
        result != null
        1 * productMapper.mapToProduct(userId, request) >> product
        1 * productTransferFactory.createAddProductTransfer(amount) >> productTransfer
        1 * product.addProductTransfer(productTransfer)
        1 * productRepository.save(product)
        1 * productMapper.mapToProductResponse(product) >> response
    }

    def "delete product"() {
        given:
        def sellerId = UUID.randomUUID()
        def productId = UUID.randomUUID()
        def product = Mock(Product)
        when:
        productFacade.deleteProduct(sellerId, productId)
        then:
        1 * productRepository.getByIdAndSellerId(productId, sellerId) >> product
        1 * productRepository.delete(product)
    }

    def "add products to stock"() {
        given:
        def sellerId = UUID.randomUUID()
        def productId = UUID.randomUUID()
        def amount = 5
        def productTransfer = Mock(ProductTransfer)
        def product = Mock(Product)
        when:
        productFacade.addProductsToStock(sellerId, productId, amount)
        then:
        1 * productRepository.getByIdAndSellerId(productId, sellerId) >> product
        1 * productTransferFactory.createAddProductTransfer(amount) >> productTransfer
        1 * product.addProductTransfer(productTransfer)
        1 * productRepository.save(product)
        1 * productMapper.mapToProductResponse(product)
    }

    def "remove products from stock"() {
        given:
        def sellerId = UUID.randomUUID()
        def productId = UUID.randomUUID()
        def amount = 5
        def productTransfer = Mock(ProductTransfer)
        def product = Mock(Product)
        when:
        productFacade.removeProductsFromStock(sellerId, productId, amount)
        then:
        1 * productRepository.getByIdAndSellerId(productId, sellerId) >> product
        1 * productTransferFactory.createRemoveProductTransfer(amount) >> productTransfer
        1 * product.addProductTransfer(productTransfer)
        1 * productRepository.save(product)
        1 * productMapper.mapToProductResponse(product)
    }

}