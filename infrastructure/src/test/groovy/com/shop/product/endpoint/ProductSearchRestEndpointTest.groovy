package com.shop.product.endpoint

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.shop.AuthenticationHelper
import com.shop.FunctionalTest
import com.shop.core.initData.AccountInitDataHelper
import com.shop.product.model.ProductInitData
import com.shop.core.initData.ProductInitDataHelper
import com.shop.user.model.UserInitData
import com.shop.product.dto.ProductResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@FunctionalTest
class ProductSearchRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    AuthenticationHelper authenticationHelper

    @Autowired
    AccountInitDataHelper accountHelper

    @Autowired
    ProductInitDataHelper productHelper

    def "request search gaming products endpoint"() {
        given:
        def testProduct = ProductInitData.PRODUCT_1
        def request = get('/api/v1/products/search?query=description==*gaming*')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        List<ProductResponse> products = objectMapper.readValue(response.contentAsString, new TypeReference<List<ProductResponse>>(){})
        products.size() == 2
        products.any {found -> found.id == ProductInitData.PRODUCT_1.id}
        products.any {found -> found.id == ProductInitData.PRODUCT_3.id}
        with(products.find { found -> found.id == testProduct.id}) {
            it.id == testProduct.getId()
            it.sellerId == testProduct.getSellerId()
            it.price == testProduct.getPrice()
            it.name == testProduct.getName()
            it.description == testProduct.getDescription()
            it.imageUrl == testProduct.getImageUrl()
        }
    }

    def "request search mouse products endpoint"() {
        given:
        def request = get('/api/v1/products/search?query=name==mouse')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        List<ProductResponse> products = objectMapper.readValue(response.contentAsString, new TypeReference<List<ProductResponse>>(){})
        products.size() == 2
        products.any {found -> found.id == ProductInitData.PRODUCT_3.id}
        products.any {found -> found.id == ProductInitData.PRODUCT_5.id}
    }

    def "request search products by price endpoint"() {
        given:
        def request = get('/api/v1/products/search?query=price<300')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        List<ProductResponse> products = objectMapper.readValue(response.contentAsString, new TypeReference<List<ProductResponse>>(){})
        products.size() == 3
        products.any {found -> found.id == ProductInitData.PRODUCT_1.id}
        products.any {found -> found.id == ProductInitData.PRODUCT_3.id}
        products.any {found -> found.id == ProductInitData.PRODUCT_5.id}

    }

    def "request search products with price between endpoint"() {
        given:
        def request = get('/api/v1/products/search?query=price=btw=(100,300)')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        List<ProductResponse> products = objectMapper.readValue(response.contentAsString, new TypeReference<List<ProductResponse>>(){})
        products.size() == 2
        products.any {found -> found.id == ProductInitData.PRODUCT_1.id}
        products.any {found -> found.id == ProductInitData.PRODUCT_3.id}
    }

}
