package com.shop.product.endpoint

import com.fasterxml.jackson.databind.ObjectMapper
import com.shop.AuthenticationHelper
import com.shop.product.model.ProductInitData
import com.shop.core.initData.ProductInitDataHelper
import com.shop.user.model.UserInitData
import com.shop.core.restHandler.StandardResponseError
import com.shop.product.dto.ProductRequest
import com.shop.product.dto.ProductResponse
import com.shop.FunctionalTest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@FunctionalTest
class ProductRestEndpointTest extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    AuthenticationHelper authenticationHelper

    @Autowired
    ProductInitDataHelper productHelper

    def "request get product endpoint"() {
        given:
        def testProduct = ProductInitData.PRODUCT_1
        def request = get('/api/v1/products/' + testProduct.getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        with (objectMapper.readValue(response.contentAsString, ProductResponse.class)) {
            it.id == testProduct.getId()
            it.sellerId == testProduct.getSellerId()
            it.price == testProduct.getPrice()
            it.name == testProduct.getName()
            it.description == testProduct.getDescription()
            it.imageUrl == testProduct.getImageUrl()
            it.stockNumber == testProduct.getStockNumber()
        }
    }

    def "request get product endpoint when product doesn't exists"() {
        given:
        def testProduct = ProductInitData.UNSAVED_PRODUCT
        def request = get('/api/v1/products/' + testProduct.getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(UserInitData.USER_1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format("product with id '%s' not found", testProduct.getId())
        }
    }

    def "request register product endpoint"() {
        given:
        def testUser = UserInitData.USER_2
        def testProduct = ProductInitData.UNSAVED_PRODUCT
        ProductRequest dto = new ProductRequest()
        dto.price = testProduct.getPrice()
        dto.name = testProduct.getName()
        dto.description = testProduct.getDescription()
        dto.imageUrl = testProduct.getImageUrl()
        dto.stockNumber = testProduct.getStockNumber()
        def request = post('/api/v1/products')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        def result = objectMapper.readValue(response.contentAsString, ProductResponse.class)
        with (result) {
            it.id != null
            it.sellerId == testProduct.getSellerId()
            it.price == testProduct.getPrice()
            it.name == testProduct.getName()
            it.description == testProduct.getDescription()
            it.imageUrl == testProduct.getImageUrl()
            it.stockNumber == testProduct.getStockNumber()
        }
        with(productHelper.getProduct(result.getId())) {
            it.id == result.getId()
            it.sellerId == testProduct.getSellerId()
            it.price == testProduct.getPrice()
            it.name == testProduct.getName()
            it.description == testProduct.getDescription()
            it.imageUrl == testProduct.getImageUrl()
            it.stockNumber == testProduct.getStockNumber()
        }
        cleanup:
        productHelper.deleteProduct(result.getId())
    }

    def "request register product when invalid data endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        ProductRequest dto = new ProductRequest()
        dto.price = price
        dto.name = name
        dto.description = description
        dto.imageUrl = imageUrl
        def request = post('/api/v1/products')
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dto))
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 400
        where:
        price | name   | description   | imageUrl
        null  | 'name' | 'description' | 'http://qwe.pl'
        -7    | 'name' | 'description' | 'http://qwe.pl'
        7     | null   | 'description' | 'http://qwe.pl'
        7     | 'name' | 'description' | 'http://qwe/pl'
        7     | 'name' | 'description' | 'test.test'
    }

    def "request delete product endpoint"() {
        given:
        def testUser = UserInitData.USER_1
        def testProduct = ProductInitData.PRODUCT_1
        def request = delete('/api/v1/products/' + testProduct.getId())
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 200
        cleanup:
        productHelper.setupProduct(testProduct)
    }

    def "request delete product endpoint when product belong to different user"() {
        given:
        def testUser = UserInitData.USER_1
        def productId = ProductInitData.PRODUCT_2.getId()
        def request = delete('/api/v1/products/' + productId)
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format("product with id '%s' for seller '%s' not found", productId, testUser.getId())
        }
    }

    def "request delete product endpoint when product doesn't exists"() {
        given:
        def testUser = UserInitData.USER_1
        def productId = UUID.randomUUID()
        def request = delete('/api/v1/products/' + productId)
                .header(HttpHeaders.AUTHORIZATION, authenticationHelper.authenticate(testUser))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        when:
        def response = mockMvc.perform(request).andReturn().response
        then:
        response.status == 404
        with (objectMapper.readValue(response.contentAsString, StandardResponseError)) {
            it.message == String.format("product with id '%s' for seller '%s' not found", productId, testUser.getId())
        }
    }

}
