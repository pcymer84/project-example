package com.shop;

import com.shop.account.service.FacebookService;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import spock.mock.DetachedMockFactory;

@TestConfiguration
public class MvcTestConfiguration {

    private DetachedMockFactory factory = new DetachedMockFactory();

    @Bean
    @Primary
    public FacebookService mockFacebookClient() {
        return factory.Mock(FacebookService.class);
    }

}
