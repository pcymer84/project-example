package com.shop.user.repository;

import com.shop.user.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
public interface CrudUserRepository extends CrudRepository<User, UUID> {
}
