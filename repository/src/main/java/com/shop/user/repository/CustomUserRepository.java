package com.shop.user.repository;

import com.shop.user.model.User;
import java.util.UUID;

public interface CustomUserRepository {

    User getById(UUID userId);

}
