package com.shop.user.repository;

import com.shop.core.annotations.Repository;

@Repository
public interface UserRepository extends CrudUserRepository, CustomUserRepository {
}
