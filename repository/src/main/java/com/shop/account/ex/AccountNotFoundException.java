package com.shop.account.ex;

import com.shop.core.ex.NotFoundException;

public class AccountNotFoundException extends NotFoundException {

    public AccountNotFoundException(String message) {
        super(message);
    }

}
