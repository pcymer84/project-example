package com.shop.account.repository;

import com.shop.core.annotations.Repository;

@Repository
public interface AccountRepository extends CrudAccountRepository, CustomAccountRepository {
}
