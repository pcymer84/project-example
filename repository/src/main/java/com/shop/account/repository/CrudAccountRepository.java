package com.shop.account.repository;

import com.shop.account.model.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CrudAccountRepository extends CrudRepository<Account, UUID> {

    Optional<Account> findByEmail(String email);

    Optional<Account> findByFacebookId(String facebookId);

    Optional<Account> findByResetPasswordCode(UUID resetPasswordCode);

}
