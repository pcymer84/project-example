package com.shop.account.repository;

import com.shop.account.ex.AccountNotFoundException;
import com.shop.account.model.Account;
import java.util.UUID;

public interface CustomAccountRepository {

    Account getById(UUID userId) throws AccountNotFoundException;

    Account getByEmail(String email) throws AccountNotFoundException;

    Account getByFacebookId(String facebookId) throws AccountNotFoundException;

    Account getByResetPasswordCode(UUID resetPasswordCode) throws AccountNotFoundException;

}
