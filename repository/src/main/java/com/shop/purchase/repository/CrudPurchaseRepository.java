package com.shop.purchase.repository;

import com.shop.purchase.model.Purchase;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CrudPurchaseRepository extends CrudRepository<Purchase, UUID> {

    List<Purchase> findByBuyerId(UUID buyerId);

    Optional<Purchase> findByIdAndBuyerId(UUID id, UUID buyerId);

    Optional<Purchase> findByIdAndSellerId(UUID id, UUID sellerId);

}
