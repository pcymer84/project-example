package com.shop.purchase.repository;

import com.shop.purchase.model.Purchase;
import java.util.UUID;

public interface CustomPurchaseRepository {

    Purchase getById(UUID purchaseOrderId);

    Purchase getByIdAndBuyerId(UUID id, UUID buyerId);

    Purchase getByIdAndSellerId(UUID id, UUID sellerId);

}
