package com.shop.purchase.repository;

import com.shop.core.annotations.Repository;

@Repository
public interface PurchaseRepository extends CrudPurchaseRepository, CustomPurchaseRepository {
}
