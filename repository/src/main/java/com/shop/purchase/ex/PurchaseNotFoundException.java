package com.shop.purchase.ex;

import com.shop.core.ex.NotFoundException;

public class PurchaseNotFoundException extends NotFoundException {

    public PurchaseNotFoundException(String message) {
        super(message);
    }

}
