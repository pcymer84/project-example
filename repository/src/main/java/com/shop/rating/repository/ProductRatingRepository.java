package com.shop.rating.repository;

import com.shop.core.annotations.Repository;

@Repository
public interface ProductRatingRepository extends CrudProductRatingRepository, CustomProductRatingRepository {
}
