package com.shop.rating.repository;

import com.shop.rating.model.ProductRating;
import java.util.UUID;

public interface CustomProductRatingRepository {

    ProductRating getByProductId(UUID productId);

}
