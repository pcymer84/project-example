package com.shop.rating.repository;

import com.shop.rating.model.ProductRating;
import org.springframework.data.repository.CrudRepository;
import java.util.Optional;
import java.util.UUID;

public interface CrudProductRatingRepository extends CrudRepository<ProductRating, UUID> {

    Optional<ProductRating> findByProductId(UUID productId);

}
