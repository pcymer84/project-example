package com.shop.rating.ex;

import com.shop.core.ex.NotFoundException;

public class ProductRatingNotFoundException extends NotFoundException {

    public ProductRatingNotFoundException(String message) {
        super(message);
    }

}
