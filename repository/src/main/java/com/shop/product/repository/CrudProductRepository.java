package com.shop.product.repository;

import com.shop.product.model.Product;
import org.springframework.data.repository.CrudRepository;
import java.util.Optional;
import java.util.UUID;

public interface CrudProductRepository extends CrudRepository<Product, UUID> {

    Optional<Product> findByIdAndSellerId(UUID id, UUID sellerId);

    void deleteBySellerId(UUID sellerId);

}
