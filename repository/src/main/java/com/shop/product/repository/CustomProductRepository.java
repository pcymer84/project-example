package com.shop.product.repository;

import com.shop.product.model.Product;
import java.util.UUID;

public interface CustomProductRepository {

    Product getById(UUID productId);

    Product getByIdAndSellerId(UUID id, UUID sellerId);

}
