package com.shop.product.repository;

import com.shop.core.annotations.Repository;
import com.shop.product.model.Product;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

@Repository
public interface ProductRepository extends CrudProductRepository, CustomProductRepository, JpaSpecificationExecutor<Product> {
}
