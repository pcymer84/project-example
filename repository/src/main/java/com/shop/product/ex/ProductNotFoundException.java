package com.shop.product.ex;

import com.shop.core.ex.NotFoundException;

public class ProductNotFoundException extends NotFoundException {

    public ProductNotFoundException(String message) {
        super(message);
    }

}
