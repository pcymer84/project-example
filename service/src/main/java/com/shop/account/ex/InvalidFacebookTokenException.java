package com.shop.account.ex;

import com.shop.core.ex.ClientSideException;

public class InvalidFacebookTokenException extends ClientSideException {

    public InvalidFacebookTokenException(String message) {
        super(message);
    }

    public InvalidFacebookTokenException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
