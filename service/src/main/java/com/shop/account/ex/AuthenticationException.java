package com.shop.account.ex;

import com.shop.core.ex.ClientSideException;

public class AuthenticationException extends ClientSideException {

    public AuthenticationException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
