package com.shop.account.service;

public interface AuthenticationService {

    String authenticateCredentials(String login, String password);

    String authenticateFacebookToken(String facebookToken);

}
