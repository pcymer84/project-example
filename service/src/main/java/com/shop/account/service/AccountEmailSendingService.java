package com.shop.account.service;

import com.shop.account.model.Account;

public interface AccountEmailSendingService {

    void sendRegistrationEmail(Account account);

    void sendChangeEmailAddressEmail(Account account);

    void sendResetPasswordEmail(Account account);

}
