package com.shop.account.service;

import com.shop.account.model.FacebookUser;

public interface FacebookService {

    FacebookUser getFacebookUser(String accessToken);

}
