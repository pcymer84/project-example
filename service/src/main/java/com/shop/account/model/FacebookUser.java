package com.shop.account.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FacebookUser {

    private String facebookId;
    private String email;
    private String firstName;
    private String lastName;

}
