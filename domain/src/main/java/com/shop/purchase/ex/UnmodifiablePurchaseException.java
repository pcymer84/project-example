package com.shop.purchase.ex;

import com.shop.core.ex.UnprocessableException;

public class UnmodifiablePurchaseException extends UnprocessableException {

    public UnmodifiablePurchaseException(String message) {
        super(message);
    }

}
