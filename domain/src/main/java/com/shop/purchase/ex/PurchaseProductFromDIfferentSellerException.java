package com.shop.purchase.ex;

import com.shop.core.ex.UnprocessableException;

public class PurchaseProductFromDIfferentSellerException extends UnprocessableException {

    public PurchaseProductFromDIfferentSellerException(String message) {
        super(message);
    }

}
