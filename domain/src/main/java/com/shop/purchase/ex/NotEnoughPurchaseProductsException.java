package com.shop.purchase.ex;

import com.shop.core.ex.UnprocessableException;

public class NotEnoughPurchaseProductsException extends UnprocessableException {

    public NotEnoughPurchaseProductsException(String message) {
        super(message);
    }

}
