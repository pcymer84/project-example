package com.shop.purchase.ex;

import com.shop.core.ex.UnprocessableException;

public class InvalidPurchaseStateException extends UnprocessableException {

    public InvalidPurchaseStateException(String message) {
        super(message);
    }

}
