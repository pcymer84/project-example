package com.shop.purchase.ex;

import com.shop.core.ex.NotFoundException;

public class PurchaseProductNotFoundException extends NotFoundException {

    public PurchaseProductNotFoundException(String message) {
        super(message);
    }

}
