package com.shop.purchase.model;

import com.google.common.collect.Sets;
import com.shop.core.annotations.AggregateRoot;
import com.shop.core.validator.Validator;
import com.shop.purchase.ex.InvalidPurchaseStateException;
import com.shop.purchase.ex.NotEnoughPurchaseProductsException;
import com.shop.purchase.ex.PurchaseProductFromDIfferentSellerException;
import com.shop.purchase.ex.PurchaseProductNotFoundException;
import com.shop.purchase.ex.UnmodifiablePurchaseException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;

@Table(name="purchase")
@Entity
@Builder
@ToString
@Getter
@EqualsAndHashCode(of = "id")
@AggregateRoot
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Purchase {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private UUID buyerId;
    @NotNull
    private UUID sellerId;
    @NotNull
    @Embedded
    private Invoice invoice;
    @NotNull
    @Embedded
    private Delivery delivery;
    @NotNull
    @Enumerated(EnumType.STRING)
    private PurchaseStatus status;
    @NotNull
    private Date createdAt;
    private Date realizedAt;
    private Date sendAt;
    private Date finishedAt;
    @NotEmpty
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "purchase_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<PurchaseProduct> purchaseProducts;

    public Set<PurchaseProduct> getPurchaseProducts() {
        return Sets.newHashSet(purchaseProducts);
    }

    public void changeInvoice(Invoice invoice) {
        this.invoice = invoice;
        Validator.validate(this);
    }

    public void changeDelivery(Delivery delivery) {
        validateIsPurchaseModifiable();
        this.delivery = delivery;
        Validator.validate(this);
    }

    public void addProduct(PurchaseProduct purchaseProduct) {
        validateIsPurchaseModifiable();
        validateIsProductPurchasedFromSameSeller(purchaseProduct);
        purchaseProducts.add(purchaseProduct);
    }

    public void removeProduct(PurchaseProduct purchaseProduct) {
        validateIsPurchaseModifiable();
        validateIsEnoughProductsToRemoveOne();
        purchaseProducts.remove(purchaseProduct);
    }

    public void cancelPurchase() {
        validateIsPurchaseModifiable();
        status = PurchaseStatus.CANCELLED;
    }

    public void realizePurchase() {
        validateIsPurchaseModifiable();
        status = PurchaseStatus.REALIZED;
        realizedAt = new Date();
    }

    public void sendPurchase() {
        if (status != PurchaseStatus.REALIZED) {
            throw new InvalidPurchaseStateException(String.format("purchase must be %s to change state to sent but is '%s'", PurchaseStatus.REALIZED, status));
        }
        status = PurchaseStatus.SENT;
        sendAt = new Date();
    }

    public void finishPurchase() {
        if (status != PurchaseStatus.SENT) {
            throw new InvalidPurchaseStateException(String.format("purchase must be %s to change state to sent but is '%s'", PurchaseStatus.SENT, status));
        }
        status = PurchaseStatus.FINISHED;
        finishedAt = new Date();
    }

    public boolean isFinished() {
        return status == PurchaseStatus.FINISHED;
    }

    public PurchaseProduct getPurchaseProduct(UUID productId) throws PurchaseProductNotFoundException {
        return findPurchaseProduct(productId).orElseThrow(() -> new PurchaseProductNotFoundException(String.format("product '%s' not found in purchase '%s'", productId, id)));
    }

    public Optional<PurchaseProduct> findPurchaseProduct(UUID productId) {
        return purchaseProducts.stream().filter(purchaseProduct -> purchaseProduct.isProductId(productId)).findFirst();
    }

    private void validateIsPurchaseModifiable() {
        if (!isPurchaseModifiable()) {
            throw new UnmodifiablePurchaseException(String.format("purchase is unmodifiable in state %s", status));
        }
    }

    private void validateIsProductPurchasedFromSameSeller(PurchaseProduct purchaseProduct) {
        if (!purchaseProduct.getProduct().getSellerId().equals(sellerId)) {
            ProductDetails product = purchaseProduct.getProduct();
            throw new PurchaseProductFromDIfferentSellerException(String.format("purchase is from seller '%s' and product '%s' is from different seller '%s'", sellerId, product.getProductId(), product.getSellerId()));
        }
    }

    private void validateIsEnoughProductsToRemoveOne() {
        if (purchaseProducts.size() <= 1) {
            throw new NotEnoughPurchaseProductsException("can't remove product because purchase must have at least one product");
        }
    }

    private boolean isPurchaseModifiable() {
        return status == PurchaseStatus.OPEN;
    }

    private Purchase(Purchase.PurchaseBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        status = builder.status;
        buyerId = builder.buyerId;
        sellerId = builder.sellerId;
        createdAt = builder.createdAt;
        realizedAt = builder.realizedAt;
        sendAt = builder.sendAt;
        finishedAt = builder.finishedAt;
        delivery = builder.delivery;
        invoice = builder.invoice;
        purchaseProducts = CollectionUtils.isEmpty(builder.purchaseProducts) ? Sets.newHashSet() : Sets.newHashSet(builder.purchaseProducts);
        Validator.validate(this);
        if (purchaseProducts.stream().anyMatch(purchaseProduct -> !purchaseProduct.isFromSellerId(sellerId))) {
            throw new PurchaseProductFromDIfferentSellerException("purchase products are from different sellers");
        }
    }

    static class PurchaseBuilder {

        Purchase.PurchaseBuilder with(Consumer<PurchaseBuilder> builderFunction) {
            builderFunction.accept(this);
            return this;
        }

        Purchase build() {
            return new Purchase(this);
        }

    }

}
