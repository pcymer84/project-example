package com.shop.purchase.model;

import com.shop.core.annotations.ValueObject;
import com.shop.core.validator.Validator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.util.Date;

@ValueObject
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Getter
@Embeddable
public class Invoice {

    @NotNull
    private String name;
    private String nip;
    private String address;
    private Date date;

    private Invoice(Invoice.InvoiceBuilder builder) {
        name = builder.name;
        nip = builder.nip;
        address = builder.address;
        date = builder.date;
        Validator.validate(this);
    }

    public static class InvoiceBuilder {

        public Invoice build() {
            return new Invoice(this);
        }

    }

}
