package com.shop.purchase.model;

import com.shop.core.annotations.ValueObject;
import com.shop.core.validator.Validator;
import com.shop.core.validator.annotations.Currency;
import com.shop.core.validator.annotations.URL;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@ToString
@ValueObject
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Getter
@Embeddable
public class ProductDetails {

    @NotNull
    @Column(name = "product_id")
    private UUID productId;
    @NotNull
    private UUID sellerId;
    @NotNull
    private String name;
    @NotNull
    @Currency
    private Double price;
    @URL
    private String imageUrl;
    private String description;

    private ProductDetails(ProductDetails.ProductDetailsBuilder builder) {
        productId = builder.productId;
        sellerId = builder.sellerId;
        name = builder.name;
        price = builder.price;
        description = builder.description;
        imageUrl = builder.imageUrl;
        Validator.validate(this);
    }

    public static class ProductDetailsBuilder {

        public ProductDetails build() {
            return new ProductDetails(this);
        }

    }

}
