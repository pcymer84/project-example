package com.shop.purchase.model;

import com.shop.core.annotations.ValueObject;
import com.shop.core.validator.Validator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@ToString
@ValueObject
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Embeddable
public class Delivery {

    @NotNull
    private String postcode;
    @NotNull
    private String city;
    @NotNull
    private String street;
    private String description;

    private Delivery(Delivery.DeliveryBuilder builder) {
        postcode = builder.postcode;
        city = builder.city;
        street = builder.street;
        description = builder.description;
        Validator.validate(this);
    }

    public static class DeliveryBuilder {

        public Delivery build() {
            return new Delivery(this);
        }

    }

}
