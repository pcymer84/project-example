package com.shop.purchase.model;

import com.shop.core.validator.Validator;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Table(name="purchase_product")
@Entity
@Builder
@ToString
@Getter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PurchaseProduct {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    @Min(1)
    private Integer amount;
    @NotNull
    @Embedded
    private ProductDetails product;

    public boolean isProductId(UUID productId) {
        return this.product.getProductId().equals(productId);
    }

    public boolean isFromSellerId(UUID sellerId) {
        return this.product.getSellerId().equals(sellerId);
    }

    private PurchaseProduct(PurchaseProduct.PurchaseProductBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        amount = builder.amount;
        product = builder.product;
        Validator.validate(this);
    }

    public static class PurchaseProductBuilder {

        public PurchaseProduct build() {
            return new PurchaseProduct(this);
        }

    }

}
