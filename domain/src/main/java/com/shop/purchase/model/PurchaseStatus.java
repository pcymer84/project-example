package com.shop.purchase.model;

public enum PurchaseStatus {
    CANCELLED,
    OPEN,
    REALIZED,
    SENT,
    FINISHED,
}