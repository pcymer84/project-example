package com.shop.rating.model;

import com.google.common.collect.Sets;
import com.shop.core.annotations.AggregateRoot;
import com.shop.rating.ex.ProductReviewNotFoundException;
import com.shop.rating.policy.ReviewPolicy;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Table(name="product")
@Entity(name = "product")
@ToString
@Getter
@Builder
@EqualsAndHashCode(of = "productId")
@AggregateRoot
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductRating {

    @Id
    @NotNull
    @Column(name = "id")
    private UUID productId;
    @NotNull
    private UUID sellerId;
    @JoinColumn(name = "product_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProductReview> reviews;

    public Double getAverageScore() {
        return reviews.stream()
                .mapToDouble(productReview -> productReview.getReview().getScore())
                .average()
                .orElse(0);
    }

    public void addReview(ProductReview productReview, ReviewPolicy reviewPolicy) {
        reviewPolicy.checkReviewerNotSeller(productReview.getReviewerId(), sellerId);
        reviewPolicy.checkSingleReviewPerReviewer(productReview.getReviewerId(), productId);
        reviewPolicy.checkReviewerFinishedPurchase(productReview.getReviewerId(), productId);
        reviews.add(productReview);
    }

    public Set<ProductReview> getReviews() {
        return Sets.newHashSet(reviews);
    }

    public ProductReview getReview(UUID reviewId, UUID reviewerId) {
        return findReview(reviewId, reviewerId).orElseThrow(() -> new ProductReviewNotFoundException(String.format("rating with id '%s' for reviewer '%s' not found", reviewId, reviewerId)));
    }

    public ProductReview getReview(UUID reviewId) {
        return findReview(reviewId).orElseThrow(() -> new ProductReviewNotFoundException(String.format("rating with id '%s' not found", reviewId)));
    }

    public Optional<ProductReview> findReview(UUID reviewId, UUID reviewerId) {
        return reviews.stream()
                .filter(review -> review.getId().equals(reviewId))
                .filter(review -> review.getReviewerId().equals(reviewerId))
                .findFirst();
    }

    public Optional<ProductReview> findReview(UUID reviewId) {
        return reviews.stream().filter(review -> review.getId().equals(reviewId)).findFirst();
    }

}
