package com.shop.rating.model;

import com.shop.core.annotations.ValueObject;
import com.shop.core.validator.Validator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ValueObject
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true)
@Getter
@ToString
@Embeddable
public class ReviewDetails {

    @Min(1)
    @Max(5)
    @NotNull
    private Integer score;
    @Size(max = 255)
    private String comment;

    private ReviewDetails(ReviewDetails.ReviewDetailsBuilder builder) {
        score = builder.score;
        comment = builder.comment;
        Validator.validate(this);
    }

    public static class ReviewDetailsBuilder {

        public ReviewDetails build() {
            return new ReviewDetails(this);
        }

    }

}
