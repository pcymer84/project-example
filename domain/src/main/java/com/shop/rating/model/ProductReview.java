package com.shop.rating.model;

import com.shop.core.validator.Validator;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Table(name="review")
@Entity
@Builder
@ToString
@Getter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductReview {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private UUID reviewerId;
    @NotNull
    @Embedded
    private ReviewDetails review;
    private Date lastModified;

    public void updateReview(ReviewDetails review) {
        this.review = review;
        this.lastModified = new Date();
    }

    private ProductReview(ProductReview.ProductReviewBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        reviewerId = builder.reviewerId;
        review = builder.review;
        lastModified = builder.lastModified == null ? new Date() : builder.lastModified;
        Validator.validate(this);
    }

    public static class ProductReviewBuilder {

        public ProductReview build() {
            return new ProductReview(this);
        }

    }

}
