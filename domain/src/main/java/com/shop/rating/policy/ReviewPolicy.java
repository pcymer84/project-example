package com.shop.rating.policy;

import com.shop.rating.ex.InvalidReviewerException;
import com.shop.rating.ex.ReviewerAlreadyReviewedProductException;
import com.shop.rating.ex.ReviewerNotFinishedPurchaseException;
import java.util.UUID;

public interface ReviewPolicy {

    void checkReviewerNotSeller(UUID reviewerId, UUID sellerId) throws InvalidReviewerException;

    void checkReviewerFinishedPurchase(UUID reviewerId, UUID productId) throws ReviewerNotFinishedPurchaseException;

    void checkSingleReviewPerReviewer(UUID reviewerId, UUID productId) throws ReviewerAlreadyReviewedProductException;

}
