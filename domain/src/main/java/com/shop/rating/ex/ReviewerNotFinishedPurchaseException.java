package com.shop.rating.ex;

import com.shop.core.ex.ClientSideException;

public class ReviewerNotFinishedPurchaseException extends ClientSideException {

    public ReviewerNotFinishedPurchaseException(String message) {
        super(message);
    }

}
