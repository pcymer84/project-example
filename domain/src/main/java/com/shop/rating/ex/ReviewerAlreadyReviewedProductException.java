package com.shop.rating.ex;

import com.shop.core.ex.ClientSideException;

public class ReviewerAlreadyReviewedProductException extends ClientSideException {

    public ReviewerAlreadyReviewedProductException(String message) {
        super(message);
    }

}
