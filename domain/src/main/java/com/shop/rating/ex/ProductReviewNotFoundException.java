package com.shop.rating.ex;

import com.shop.core.ex.NotFoundException;

public class ProductReviewNotFoundException extends NotFoundException {

    public ProductReviewNotFoundException(String message) {
        super(message);
    }

}
