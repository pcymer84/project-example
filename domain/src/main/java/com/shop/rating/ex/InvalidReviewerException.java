package com.shop.rating.ex;

import com.shop.core.ex.ClientSideException;

public class InvalidReviewerException extends ClientSideException {

    public InvalidReviewerException(String message) {
        super(message);
    }

}
