package com.shop.account.ex;

import com.shop.core.ex.ClientSideException;

public class FacebookIdAlreadyUsedException extends ClientSideException {

    public FacebookIdAlreadyUsedException(String message) {
        super(message);
    }

}
