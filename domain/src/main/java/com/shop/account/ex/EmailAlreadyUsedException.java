package com.shop.account.ex;

import com.shop.core.ex.ClientSideException;

public class EmailAlreadyUsedException extends ClientSideException {

    public EmailAlreadyUsedException(String message) {
        super(message);
    }

}
