package com.shop.account.ex;

import com.shop.core.ex.ClientSideException;

public class InvalidResetPasswordCodeException extends ClientSideException {

    public InvalidResetPasswordCodeException(String message) {
        super(message);
    }

}
