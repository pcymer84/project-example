package com.shop.account.ex;

import com.shop.core.ex.ClientSideException;

public class WeakPasswordException extends ClientSideException {

    public WeakPasswordException(String message) {
        super(message);
    }

}
