package com.shop.account.ex;

import com.shop.core.ex.ClientSideException;

public class UserAlreadyUsingEmailException extends ClientSideException {

    public UserAlreadyUsingEmailException(String message) {
        super(message);
    }

}
