package com.shop.account.model;

import com.google.common.collect.Sets;
import com.shop.account.policy.EmailPolicy;
import com.shop.account.policy.PasswordPolicy;
import com.shop.core.annotations.AggregateRoot;
import com.shop.core.validator.Validator;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@Table(name = "account")
@Entity
@Builder
@ToString()
@Getter
@EqualsAndHashCode(of = "id")
@AggregateRoot
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Account {

    @Id
    @NotNull
    private UUID id;
    @Email
    @NotNull
    private String email;
    private String password;
    private String facebookId;
    private UUID resetPasswordCode;
    @Getter(AccessLevel.NONE)
    @ElementCollection(targetClass = Role.class)
    @CollectionTable(name = "role")
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;

    public Set<Role> getRoles() {
        return Sets.newHashSet(roles);
    }

    public void assignRole(Role role) {
        roles.add(role);
    }

    public void removeRole(Role role) {
        roles.remove(role);
    }

    public void changeEmail(String email, EmailPolicy policy) {
        // policy is excellent tool for 'feature toggle' usage on entity level
        policy.checkEmailUsed(id, email);
        this.email = email;
        Validator.validate(this);
    }

    public void changePassword(String password, PasswordPolicy policy) {
        policy.checkPasswordStrength(password);
        this.password = policy.encodePassword(password);
        this.resetPasswordCode = null;
    }

    public void generateResetPasswordCode() {
        if (resetPasswordCode == null) {
            this.resetPasswordCode = UUID.randomUUID();
        }
    }

    private Account(Account.AccountBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        resetPasswordCode = builder.resetPasswordCode;
        email = builder.email;
        facebookId = builder.facebookId;
        password = builder.password;
        roles = CollectionUtils.isEmpty(builder.roles) ? Sets.newHashSet() :  builder.roles;
        Validator.validate(this);
    }

    static class AccountBuilder {

        Account build() {
            return new Account(this);
        }

    }

}
