package com.shop.account.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Permission {

    ROLE_MANAGEMENT(Authority.ROLE_MANAGEMENT),

    PRODUCT_MANAGEMENT(Authority.PRODUCT_MANAGEMENT),
    PRODUCT_STOCK_MANAGEMENT(Authority.PRODUCT_STOCK_MANAGEMENT),

    SHOW_ACCOUNT(Authority.SHOW_ACCOUNT),
    DELETE_ACCOUNT(Authority.DELETE_ACCOUNT),

    CENSORSHIP_REVIEW(Authority.CENSORSHIP_REVIEW);

    private String authority;

    public static class Authority {
        public static final String ROLE_MANAGEMENT = "ROLE_MANAGEMENT";

        public static final String PRODUCT_MANAGEMENT = "PRODUCT_MANAGEMENT";
        public static final String PRODUCT_STOCK_MANAGEMENT = "PRODUCT_STOCK_MANAGEMENT";

        public static final String SHOW_ACCOUNT = "SHOW_ACCOUNT";
        public static final String DELETE_ACCOUNT = "DELETE_ACCOUNT";

        public static final String CENSORSHIP_REVIEW = "CENSORSHIP_REVIEW";
    }

}
