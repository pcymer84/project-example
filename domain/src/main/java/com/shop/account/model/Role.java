package com.shop.account.model;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.List;

@Getter
@AllArgsConstructor
public enum Role {

    ADMIN(
            Lists.newArrayList(Permission.values())
    ),
    SELLER(
            Lists.newArrayList(
                    Permission.PRODUCT_MANAGEMENT,
                    Permission.PRODUCT_STOCK_MANAGEMENT
            )
    ),
    MODERATOR(
            Lists.newArrayList(
                    Permission.SHOW_ACCOUNT,
                    Permission.DELETE_ACCOUNT,
                    Permission.CENSORSHIP_REVIEW
            )
    );

    private List<Permission> permissions;

}
