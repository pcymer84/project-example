package com.shop.account.policy;

import java.util.UUID;

public interface EmailPolicy {

    void checkEmailUsed(String email);

    void checkEmailUsed(UUID userId, String email);

}
