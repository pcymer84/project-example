package com.shop.account.policy;

public interface PasswordPolicy {

    void checkPasswordStrength(String rawPassword);

    String encodePassword(String rawPassword);

}
