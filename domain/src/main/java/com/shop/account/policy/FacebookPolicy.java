package com.shop.account.policy;

public interface FacebookPolicy {

    void checkFacebookIdUsed(String facebookId);

}
