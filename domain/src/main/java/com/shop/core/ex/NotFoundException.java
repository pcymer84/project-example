package com.shop.core.ex;

public class NotFoundException extends ClientSideException {

    public NotFoundException(String message) {
        super(message);
    }

}
