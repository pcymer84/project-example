package com.shop.core.ex;

public class UnprocessableException extends ClientSideException {

    public UnprocessableException(String message) {
        super(message);
    }

}
