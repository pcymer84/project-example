package com.shop.core.logger;

public enum LogLevel {

    DEBUG,
    INFO,
    WARN,
    ERROR

}
