package com.shop.core.validator.impl;

import com.shop.core.validator.Validator;
import com.shop.core.validator.annotations.Year;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class YearValidation implements ConstraintValidator<Year, Number> {

    private static final Integer MIN = 1970;
    private static final Integer MAX = 2300;

    @Override
    public boolean isValid(Number value, ConstraintValidatorContext context) {
        return Validator.validationResult(
                value == null || validate(value),
                String.format("year value should be between %s and %s", MIN, MAX),
                context
        );
    }

    private boolean validate(Number value) {
        int val = value.intValue();
        return val >= MIN && val <= MAX;
    }

}
