package com.shop.core.validator.impl;

import com.shop.core.validator.Validator;
import com.shop.core.validator.annotations.Phone;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneValidation implements ConstraintValidator<Phone, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return Validator.validationResult(
                value == null || value.matches("\\d+"),
                "phone number should contains only numbers",
                context
        );
    }

}
