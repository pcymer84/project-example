package com.shop.core.validator.impl;

import com.shop.core.validator.Validator;
import com.shop.core.validator.annotations.Month;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MonthValidation implements ConstraintValidator<Month, Number> {

    @Override
    public boolean isValid(Number value, ConstraintValidatorContext context) {
        return Validator.validationResult(
                value == null || validate(value),
                "month value should be between 1 and 12",
                context
        );
    }

    private boolean validate(Number value) {
        int val = value.intValue();
        return val >= 1 && val <= 12;
    }

}
