package com.shop.core.validator.annotations;

import com.shop.core.validator.impl.CurrencyValidation;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = CurrencyValidation.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Currency {

    String message() default "Invalid currency value";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
