package com.shop.core.validator.annotations;

import com.shop.core.validator.impl.MonthValidation;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = MonthValidation.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Month {

    String message() default "Invalid month value";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
