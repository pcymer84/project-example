package com.shop.core.validator;

import com.shop.core.validator.ex.ValidationException;
import lombok.experimental.UtilityClass;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@UtilityClass
public final class Validator {

    private static final javax.validation.Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public boolean validationResult(Boolean result, String message, ConstraintValidatorContext context) {
        if (!result && context != null) {
            context.buildConstraintViolationWithTemplate(message)
                    .addConstraintViolation()
                    .disableDefaultConstraintViolation();
        }
        return result;
    }

    public void validate(Object obj) throws ValidationException {
        Set<ConstraintViolation<Object>> violations = validator.validate(obj);
        if (violations.size() > 0) {
            throw createValidatorException(obj, violations);
        }
    }

    private ValidationException createValidatorException(Object obj, Set<ConstraintViolation<Object>> violations) {
        return new ValidationException(createValidationErrorMessage(obj, violations), obj, violations);
    }

    private String createValidationErrorMessage(Object obj, Set<ConstraintViolation<Object>> violations) {
        return String.format("validation exception when creating object '%s' with violations : %s", obj.getClass().getSimpleName(), convertViolations(violations));
    }

    private List<String> convertViolations(Set<ConstraintViolation<Object>> violations) {
        return violations.stream().map(Validator::convertViolation).collect(Collectors.toList());
    }

    private String convertViolation(ConstraintViolation<Object> violation) {
        return String.format("{property='%s', violation='%s', value='%s'}",
                violation.getPropertyPath(),
                violation.getMessage(),
                violation.getInvalidValue()
        );
    }

}
