package com.shop.core.validator.impl;

import com.shop.core.validator.Validator;
import com.shop.core.validator.annotations.URL;
import org.apache.commons.validator.routines.UrlValidator;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UrlValidation implements ConstraintValidator<URL, String> {

    private static final UrlValidator urlValidator = new UrlValidator();

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return Validator.validationResult(
                value == null || urlValidator.isValid(value),
                "invalid url format",
                context
        );
    }

}
