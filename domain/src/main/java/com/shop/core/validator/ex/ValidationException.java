package com.shop.core.validator.ex;

import com.shop.core.ex.ClientSideException;
import lombok.Getter;
import javax.validation.ConstraintViolation;
import java.util.Set;

@Getter
public class ValidationException extends ClientSideException {

    private Object invalidObject;
    private Set<ConstraintViolation<Object>> violations;

    public ValidationException(String message, Object invalidObject, Set<ConstraintViolation<Object>> violations) {
        super(message);
        this.invalidObject = invalidObject;
        this.violations = violations;
    }

}