package com.shop.core.validator.impl;

import com.shop.core.validator.Validator;
import com.shop.core.validator.annotations.Currency;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

public class CurrencyValidation implements ConstraintValidator<Currency, Double> {

    @Override
    public boolean isValid(Double value, ConstraintValidatorContext context) {
        return Validator.validationResult(
                value == null || validate(value),
                "currency value should be positive decimal with maximum 2 places after dot",
                context);
    }

    private boolean validate(Double value) {
        BigDecimal noZero = BigDecimal.valueOf(value).stripTrailingZeros();
        int scale = noZero.scale() < 0 ? 0 : noZero.scale();
        return scale <= 2 && value.intValue() >= 0;
    }

}
