package com.shop.product.ex;

import com.shop.core.ex.UnprocessableException;

public class ProductNotAvailableException extends UnprocessableException {

    public ProductNotAvailableException(String message) {
        super(message);
    }

}
