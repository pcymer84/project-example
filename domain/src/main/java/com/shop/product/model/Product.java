package com.shop.product.model;

import com.google.common.collect.Sets;
import com.shop.core.annotations.AggregateRoot;
import com.shop.core.validator.Validator;
import com.shop.core.validator.annotations.Currency;
import com.shop.core.validator.annotations.URL;
import com.shop.product.ex.ProductNotAvailableException;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@Table(name="product")
@Entity
@Builder
@ToString
@Getter
@EqualsAndHashCode(of = "id")
@AggregateRoot
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Product {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private UUID sellerId;
    @NotNull
    private String name;
    @NotNull
    @Currency
    // to calculate financial transactions always use BigDecimal
    private Double price;
    @URL
    private String imageUrl;
    private String description;
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "product_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProductTransfer> productTransfers;

    public Integer getStockNumber() {
        return productTransfers.stream()
                .mapToInt(ProductTransfer::calculate)
                .sum();
    }

    public void checkAvailability() throws ProductNotAvailableException {
        if (getStockNumber() <= 0) {
            throw new ProductNotAvailableException(String.format("product '%s' is not available", id));
        }
    }

    public void addProductTransfer(ProductTransfer productTransfer) {
        productTransfers.add(productTransfer);
    }

    private Product(Product.ProductBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        sellerId = builder.sellerId;
        name = builder.name;
        price = builder.price;
        description = builder.description;
        imageUrl = builder.imageUrl;
        productTransfers = CollectionUtils.isEmpty(builder.productTransfers) ? Sets.newHashSet() : Sets.newHashSet(builder.productTransfers);
        Validator.validate(this);
    }

    public static class ProductBuilder {

        public Product build() {
            return new Product(this);
        }

    }

}
