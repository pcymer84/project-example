package com.shop.product.model;

public interface ProductTransferFactory {

    ProductTransfer createAddProductTransfer(Integer amount);

    ProductTransfer createRemoveProductTransfer(Integer amount);

}
