package com.shop.product.model;

import com.shop.core.annotations.AggregateRoot;
import com.shop.core.validator.Validator;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Table(name="product_transfer")
@Entity
@ToString
@Getter
@Builder
@EqualsAndHashCode(of = "id")
@AggregateRoot
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductTransfer {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    @Enumerated(EnumType.STRING)
    private Operation operation;
    @NotNull
    @Min(1)
    private Integer amount;
    @NotNull
    private Date transferDate;

    public int calculate() {
        return operation.getFunction().apply(amount);
    }

    private ProductTransfer(ProductTransfer.ProductTransferBuilder builder) {
        this.id = builder.id == null ? UUID.randomUUID() : builder.id;
        this.operation = builder.operation;
        this.amount = builder.amount;
        this.transferDate = builder.transferDate;
        Validator.validate(this);
    }

    static class ProductTransferBuilder {

        ProductTransfer build() {
            return new ProductTransfer(this);
        }

    }

}
