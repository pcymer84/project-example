package com.shop.product.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.function.Function;

@Getter
@AllArgsConstructor
public enum Operation {

    ADD(x -> x),
    REMOVE(x -> Math.abs(x) * -1);

    Function<Integer, Integer> function;

}
