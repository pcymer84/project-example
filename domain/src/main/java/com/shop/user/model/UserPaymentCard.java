package com.shop.user.model;

import com.shop.core.validator.Validator;
import com.shop.core.validator.annotations.Month;
import com.shop.core.validator.annotations.Year;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Table(name="user_payment_card")
@Builder
@Entity
@ToString
@Getter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserPaymentCard {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private String cardNumber;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    @Month
    private Integer createdMonth;
    @NotNull
    @Year
    private Integer createdYear;
    @NotNull
    @Month
    private Integer expiredMonth;
    @NotNull
    @Year
    private Integer expiredYear;

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        Validator.validate(this);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        Validator.validate(this);
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        Validator.validate(this);
    }

    public void setCreatedMonth(Integer createdMonth) {
        this.createdMonth = createdMonth;
        Validator.validate(this);
    }

    public void setCreatedYear(Integer createdYear) {
        this.createdYear = createdYear;
        Validator.validate(this);
    }

    public void setExpiredMonth(Integer expiredMonth) {
        this.expiredMonth = expiredMonth;
        Validator.validate(this);
    }

    public void setExpiredYear(Integer expiredYear) {
        this.expiredYear = expiredYear;
        Validator.validate(this);
    }

    private UserPaymentCard(UserPaymentCard.UserPaymentCardBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        this.cardNumber = builder.cardNumber;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.createdMonth = builder.createdMonth;
        this.createdYear = builder.createdYear;
        this.expiredMonth = builder.expiredMonth;
        this.expiredYear = builder.expiredYear;
        Validator.validate(this);
    }

    public static class UserPaymentCardBuilder {

        public UserPaymentCard build() {
            return new UserPaymentCard(this);
        }

    }

}
