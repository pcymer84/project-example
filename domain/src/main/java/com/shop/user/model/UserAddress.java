package com.shop.user.model;

import com.shop.core.validator.Validator;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Table(name="user_address")
@Builder
@Entity
@ToString
@Getter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserAddress {

    @Id
    @NotNull
    private UUID id;
    @Setter
    private String postcode;
    @Setter
    private String city;
    @Setter
    private String street;
    @Setter
    private String description;

    private UserAddress(UserAddress.UserAddressBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        this.postcode = builder.postcode;
        this.city = builder.city;
        this.street = builder.street;
        this.description = builder.description;
        Validator.validate(this);
    }

    public static class UserAddressBuilder {

        public UserAddress build() {
            return new UserAddress(this);
        }

    }

}
