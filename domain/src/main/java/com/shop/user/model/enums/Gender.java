package com.shop.user.model.enums;

public enum Gender {
    FEMALE,
    MALE
}
