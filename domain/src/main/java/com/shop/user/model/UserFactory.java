package com.shop.user.model;

import com.shop.account.model.Account;

public interface UserFactory {

    User createUserProfileForAccount(Account account);

}
