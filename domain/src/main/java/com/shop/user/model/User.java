package com.shop.user.model;

import com.google.common.collect.Sets;
import com.shop.core.annotations.AggregateRoot;
import com.shop.core.validator.Validator;
import com.shop.core.validator.annotations.Phone;
import com.shop.user.ex.UserAddressNotFoundException;
import com.shop.user.ex.UserPaymentCardNotFoundException;
import com.shop.user.model.enums.Gender;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;

@Table(name = "user")
@Entity
@Builder
@ToString
@Getter
@EqualsAndHashCode(of = "id")
@AggregateRoot
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class User {

    @Id
    private UUID id;
    @Setter
    private String firstName;
    @Setter
    private String lastName;
    @Setter
    private LocalDate birthDate;
    @Setter
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @Phone
    @Setter
    private String phone;
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "user_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<UserAddress> addresses;
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "user_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<UserPaymentCard> paymentCards;

    public Optional<UserAddress> findAddress(UUID addressId) {
        return addresses.stream().filter(address -> address.getId().equals(addressId)).findFirst();
    }

    public UserAddress getAddress(UUID addressId) {
        return findAddress(addressId).orElseThrow(() -> new UserAddressNotFoundException(String.format("address with id '%s' not found", addressId)));
    }

    public Set<UserAddress> getAddresses() {
        return Sets.newHashSet(addresses);
    }

    public void addAddress(UserAddress userAddress) {
        Objects.requireNonNull(userAddress);
        addresses.add(userAddress);
    }

    public void removeAddress(UUID addressId) {
        findAddress(addressId).ifPresent(address -> addresses.remove(address));
    }

    public Optional<UserPaymentCard> findPaymentCard(UUID paymentCardId) {
        return paymentCards.stream().filter(paymentCard -> paymentCard.getId().equals(paymentCardId)).findFirst();
    }

    public UserPaymentCard getPaymentCard(UUID paymentCardId) {
        return findPaymentCard(paymentCardId).orElseThrow(() -> new UserPaymentCardNotFoundException(String.format("paymentCard with id '%s' not found", paymentCardId)));
    }

    public Set<UserPaymentCard> getPaymentCards() {
        return Sets.newHashSet(paymentCards);
    }

    public void addPaymentCard(UserPaymentCard paymentCard) {
        Objects.requireNonNull(paymentCard);
        paymentCards.add(paymentCard);
    }

    public void removePaymentCard(UUID paymentCardId) {
        findPaymentCard(paymentCardId).ifPresent(paymentCard -> paymentCards.remove(paymentCard));
    }

    private User(UserBuilder builder) {
        id = builder.id;
        phone = builder.phone;
        firstName = builder.firstName;
        lastName = builder.lastName;
        birthDate = builder.birthDate;
        gender = builder.gender;
        addresses = CollectionUtils.isEmpty(builder.addresses) ? Sets.newHashSet() : Sets.newHashSet(builder.addresses);
        paymentCards = CollectionUtils.isEmpty(builder.paymentCards) ? Sets.newHashSet() : Sets.newHashSet(builder.paymentCards);
        Validator.validate(this);
    }

    static class UserBuilder {

        User.UserBuilder with(Consumer<UserBuilder> builderFunction) {
            builderFunction.accept(this);
            return this;
        }

        User build() {
            return new User(this);
        }

    }

}
