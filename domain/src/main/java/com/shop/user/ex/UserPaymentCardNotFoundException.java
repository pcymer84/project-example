package com.shop.user.ex;

import com.shop.core.ex.NotFoundException;

public class UserPaymentCardNotFoundException extends NotFoundException {

    public UserPaymentCardNotFoundException(String message) {
        super(message);
    }

}
