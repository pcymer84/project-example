package com.shop.user.ex;

import com.shop.core.ex.NotFoundException;

public class UserAddressNotFoundException extends NotFoundException {

    public UserAddressNotFoundException(String message) {
        super(message);
    }

}
